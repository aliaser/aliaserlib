[![build status](https://gitlab.com/aliaser/aliaserlib/badges/master/build.svg)](https://gitlab.com/aliaser/aliaserlib/commits/master)
[![coverage report](https://gitlab.com/aliaser/aliaserlib/badges/master/coverage.svg)](https://gitlab.com/aliaser/aliaserlib/commits/master)

Aliaser is a rust library that allows for interpreting defined workflows and implementation.

Note: aliaserlib is still under heavy development and is not recommended for use yet!

# Scenario

Ever been confused by differences between programs that do the same job.

Eg to 'uncommit' something in a VCS, to simply undo the 'act' of commiting. In bzr it is `bzr uncommit` however it git it is `git reset --soft HEAD^` and in hg it is `hg rollback` or `hg strip -r -1 -k`.

What if you could simply do `a vcs:git uncommit` or `a vcs:hg uncommit` or as a VCS can be determined by the presence of files `a vcs uncommit` and the command is automatically converted and run for you!

This library aims to provide a simple interface which applications can use to perform this abstraction.

# Building

```
cargo build
```

# Testing

```
cargo test
```
