
# Implementation

An implementation is a specific implementation of a workflow which may use some backends.

## Definition

```
impl:
  WORKFLOW:
    IMPLEMENTATION:
      COMMAND:
        exec: LIST<CMD>
        expect: LIST<int>
        filter: LIST<FILTER>
```

Or a list of exec's if you need different filter or expect on some exec's

```
impl:
  WORKFLOW:
    IMPLEMENTATION:
      COMMAND:
        LIST<
          exec: LIST<CMD>
          expect: LIST<int>
          filter: LIST<FILTER>
        >
```

## Example

```
impl:
  vcs:
    bzr:
      clone:
        exec:
          - ${EXEC} clone ${branch} ${target}
        filter:
          - bzr-progress-fix
```

Or an example with a list of exec's

```
impl:
  vcs:
    bzr:
      clone:
        - exec:
            - ${EXEC} first-cmd
          expect:  # valid return codes
            - 1
            - 5
          filter:
            - filter-a
        - exec:
            - ${EXEC} second-cmd
          filter:
            - filter-b
```

## Options

### Name

The name of the implementation should match the [Name spec](/docs/spec/naming.md).

### Exec

This is the actual command that will be run, `${EXEC}` will find a use the exec of a backend with the same name as this implementation or `${EXEC:git}` can be used to specify a backend.

TODO: describe how attributes are used ${target} and how variables ared used ${BRANCH_NAME} and how to embed attributes --foo="${bar}-test"
if variable is not whole of arg it must have quotes?
Define escape rules

### Expect

This is a list of integers specifying the expected return codes of the exec line, if this is not specified zero is assumed as the expected and a non-zero will result in an error.

TODO: this is not implemented yet!

### Filter

This is a list of filters to run on the output of the exec's.

## Future

  * Define how changing directory works ? either `cd foo` then next commands are in dir or `directory: foo` on each that is not the CWD?
  * Have optional which allows for an optional argument to only be used when at least one attribute is not equal to it's default eg ```optional: - --fixes=${fixes}```
  * Flags might be able to use optional, --my-flag could then set it's name to the value. Then `optional: - --${flag}` gets replaced with --my-flag as it is not the default - this requires flag to be special
