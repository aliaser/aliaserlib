
# Filter

Filter contains a key-value store of the names of the filters and then details.

The order the filters are run when defined in defaults and implementation commands is first to last.
Starting with the command filters, then the default filters.

TODO: we need system level filters to be able to apply color formatting to different OSes?
Eg how in avcs we have formatters for ansi, html, powershell, None.

These will need a common colour input that is understood and then can be changed to the platform one.
Also somewhere we need to declare which one to use, eg below?

TODO: maybe have workflow filters eventually, if implementation can provide the same output?

## Definition

```
filter
  NAME:
    buffer: BUFFERSIZE
    replace:
      SEARCHER: REPLACER
    match:
      SEARCHER: REPLACER
```

## Example

```
filter:
  bzr-progress-fix:
    buffer: char
    replace:
      "\r": "\x1b[K\r"  # \033[K but in unicode
  bzr-diff-color:
    buffer: line
    match:
      "/^R(?<data>.*)$/": "[color=red]$data[/color]"
```

## Options

### Name

The name of the filter, this should match the [Name spec](/docs/spec/naming.md).

### Buffer

This is the minimum amount of data that this filter requires. Currently there are two options:

  * `char` - this filter only requires, at minimum a single character to function
  * `line` - a buffer up until the first \r or \n will be made before passing to the filter

TODO: line mode is not implemented yet.

### Replace

This is a simple search and replace. If the following was used on the sting `abcd` it would result in `1bc4`.

```
replace:
  "a": "1"
  "d": "4"
```

### Match

This is a regex searcher which are each tried against the input (usually a line), if a match is found then the replacer is used.

TODO: this is not implemented yet

## Future

  * Line mode is not implemented yet
  * Matching is not implemented yet
  * Have escape key-value that is run before match, eg to allow for escaping out things that match adds such as colours - `escape: "\\": "\\\\"`
