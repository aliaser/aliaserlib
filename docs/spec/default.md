
# Defaults

Defaults declare the environment and implementations that should be used by default.

Defaults can be stacked (or override), so if `vcs:git` is set as the default in the user configuration. The project configuration can then define `vcs:bzr` to override the user configuration.

Note: Logging cannot be a default, because we want to set the logging value *before* loading the configuration files.

## Definition

```
default:
  executer: EXECUTER
  env:
    KEY: VALUE
  workflow:
    WORKFLOW: IMPLEMENTATION
```

## Example

```
default:
  executer: cmd
  env:
    ALIAS_DEBUG: true
    PATH: /home/my/bin/:$PATH
  workflow:
    vcs: git
```

Workflows support an auto mode which allows for automatic probing of the current working directory and then picking of a implementation or a fallback.

The example below would probe for an implementation to use, if one is found then use it otherwise fallback to trying with git.

```
default:
  workflow:
    vcs: auto/git
```

## Options

### Executer

This is the executer to use, from the list below

  * `cmd` - this runs the command within the normal shell environment on the existing host
  * `mock` - this is used for test and debug situations and simply outputs to the console the command which would be run

### Env

This is a mapping of environment variables to use for all commands.

Note: The defaults environment is loaded first, so the backend environment can override any environment variables set.

TODO: check and give example that this is suitable place for adding to PATH

### Workflow

This is a mapping defining which implementation to use as default for each workflow.

For example if `vcs: git` is defined as a default, then when the user runs `a vcs clone repository` or `a vcs status` it will use `git` as the implementation.

There is also the option for `auto` this means that the probe commands in the backend are used to determine which implementation to use.

However using `auto` does not work for standalone commands, such as `a vcs clone repository` as there is no folder to probe, so here a fallback should be given such as `auto/git`.

The table below shows what would happen for standalone and normal commands if they were run under the different modes, auto, auto with fallback and specifying a implementation.

|                | auto | auto/git | git |
|:--------------:|:----:|:--------:|:---:|
| **standalone** | unknown | git | git |
| **normal**     | probe | probe | git |

## Future

  * Default filters ? (eg ansi-color, html-color, strip-color)
  * Should there be default variables? (eg PROJECT: pwd)
