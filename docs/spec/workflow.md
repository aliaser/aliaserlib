
# Workflow

Workflow contains a key-value store of the names of the workflows and then details.

## Definition

```
workflow
  NAME:
    COMMAND:
      args:
        ARGUMENT:
          default: DEFAULT
          optional: OPTIONAL
          type: TYPE
      description: DESCRIPTION
      order: LIST<ARGUMENTS>
      standalone: bool
```

## Example

```
workflow:
  vcs:
    clone:
      args:
        remote:
          type: repository
        target:
          type: dir
      description: |
        Clone a repository into a directory
      order:
        - remote
        - target
      standalone: true
```

TODO: do we need order? (can we use order from args?)
TODO: should we split positional and optional args? eg
TODO: what about args that accept multiple? eg FILES... before

```
args:
  positional:
    first:
      type: str
  optional:
    second:
      default: "my-default"
      type: str
  order:
    - first
description: |
  My test command!
standalone: true
```

## Options

### Name

This is the name of the workflow, see the [Name spec](/docs/spec/naming.md) for restrictions.

### Command

This is a key-value store of the commands, the value is a key-value store of the arguments.

The name of the command should match the [Name spec](/docs/spec/naming.md).

#### Arguments

This is a key-value store of the arguments.

The name of the argument should match the [Name spec](/docs/spec/naming.md).

##### Default

This is the value that is used if an optional argument is not given a value from the user.

This field is mandatory if optional is true.

##### Optional

This is a bool defining if the argument is optional.

##### Type

This is a string defining the type of the argument.

#### Description

This is a description of the command, useful for docs for the user at the command-line. eg ```$ aliaser vcs help```

#### Order

This is a list defining the order of the non-optional arguments.

FIXME: is this required? Can we use the list order?

#### Standalone

Standalone refers to if the command can be run without a pre-existing valid backend found in the directory. This means that probe (from the backend) is not used in the directory, and there must be a implementation or at least a fallback default defined.

Eg for Git VCS most commands require a .git folder to exist before they can be run.

## Future

  * A workflow may have no arguments? `a vcs status` so do we need `null: true` for that?
  * Could we inherit one workflow into another? `inherit: vcs:clone` ? `inherit: vcs/clone`
  * Should we split args into optional and positional ?
  * If above ^^, do we need order?
  * What about args that accept multiple? eg FILES... before
  * What about args that are like flags? --flag or no flag
  * Argument name should also only be lowercase? (to not conflict with variables)
  * default is not a good name for the ide, maybe fallback is better?
