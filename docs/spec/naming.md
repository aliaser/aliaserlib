
# Naming specification

This defines a naming specification used across various components.

## Regex

The general form that can be used is `[A-z][A-z0-9-_]*` which means:

  * Must start with a letter
  * Must be at least one character long
  * May contain upper or lower case letters
  * May contain numbers (but not start with)
  * May contain a dash or underscore (but not start with)

## Blacklist

In addition to the regex, there is a blacklist of names that cannot be used

  * `all`
  * `auto`
  * `help`

## Reasons

This is a list of some of the reasons why certain characters cannot be used.

  * `:` - the workflow name cannot contain a colon as it is used to split the workflow from the implementation, eg `vcs:git` so something such as `my:vcs:git` would not work
  * `/` - in defaults the fallback system of workflows uses a slash after `auto` to state the fallback, eg `auto/git`
  * `all` - hooks use `all` as a way of saying that the hook is used in all workflows, implementations or commands
  * `auto` - the workflow name cannot be `auto` as defaults uses this to state that probing should be performed
  * `help` - commands and workflows cannot be called help as this is reserved for use by the client, eg `a help` or `a vcs help` would show help by the client not the implementation

## Future

  * `raw` - this could be reserved for commands so that direct commands can be passed through to the backend?

