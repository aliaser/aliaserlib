
# Hook

Hooks allow for running certain commands before or after a certain workflow or implementation command is executed. This is useful for ensuring that tests are run before you commit or push your work, or running a local backup after each commit of your work etc.

The order in which hooks will be executed is defined below (the more specific the hook the closer to the actual command).

  * before
    * all
      * workflow
      * implementation
      * command
    * specific
  * implementation command
  * after
    * specific
    * all
      * command
      * implementation
      * workflow

```
hook:
  vcs:
    bzr:
      all:
        after:
          - ${EXEC} check
      push:
        after:
          - rsync . backup-location
```

## Definition

```
hook:
  WORKFLOW:
    IMPLEMENTATION:
      COMMAND:
        before: LIST<BEFORE_CMD>
        after: LIST<AFTER_CMD>
```

## Example

```
hook:
  vcs:
    all:
      commit:
        before:
          - make test
    bzr:
      all:
        after:
          - ${EXEC} check
      commit:
        after:
          - rsync . my.backup.com/${PROJECT}/${BRANCH_NAME}
```

## Options

### Workflow

This is the workflow that will cause the hook to be run, or it can be `all` to run for any workflow.

### Implemention

This is the implementation that will cause the hook to be run, or it can be `all` to run for any implementation.

### Command

This is the command that will cause the hook to be run, or it can be `all` to run for any command.

### After

This is the list of commands to execute **after** the implementation command has been run.

### Before

This is the list of commands to execute **before** the implementation command has been run.

## Future

  * This is not implemented yet!
  * TODO: Work the same way as ImplementionCommand so have exec, filter
