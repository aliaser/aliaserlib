
# Type

This is how a type for a workflow argument is defined.

## Definition

```
type:
  NAME:
    primative: PRIMATIVE
    regex: REGEX
```

## Example

```
type:
  int:
    regex: [0-9]
  str:
    regex: [A-z0-9]
```

## Future

  * Not implemented yet!
  * We need to be able to have a way of declaring validators when using an implementation. Eg lp:~user/project/branch is valid for repository:bzr.
  * We need a way to manipulate an input into an output. Eg bzr uses 1 to revert by 1 commit but git may need to use -1 or even run a command to get a commit id


## Repository scenario

Valid examples for repository

  * `lp:~user/project/branch`
  * `https://github.com/project`

### Type

```
type:
  repository:
    primative: str
```

### Impl

```
impl:
  type:
    vcs:
      repository:
        validators:
          - regex
```
