
# Extend

This allows for extending a workflow, such as adding an extra command. This also requires partial implementation support.

## Definition

```
extend:
  WORKFLOW:
    COMMAND:
      ...
```

## Example

```
extend:
  vcs:
    mycommand:
      args:
        myarg: string

impl:
  vcs:
    IMPLEMENTATION:
      mycommand:
        exec: echo ${myarg}
```

## Future

  * Not implented yet!
  * Needs to define how partial implementations work, and the exact definition design.
