
# Backend

A backend is a definition of an environment to run commands from. It specifies a list of possible paths to the executable, the environment variables which should be used when executing, probe commands for detecting if the target location is able to use the backend and any variables that may need to be executed at runtime.

If the implementation and backend name match then

  * ${EXEC} inside the implementation will automatically use the exec from the backend
  * When automatically picking a implementation (eg there was no default or it was auto) the probe commands from the backend will be used

## Definition

```
backend:
  NAME:
    exec: list<path>
    env:
      KEY: VALUE
    probe:
      contains:
        FILE: VALUE
      exists: LIST<FILE>
      sha512:
        FILE: SUM
    variables:
      KEY: VALUE
```

## Example

```
backend:
  bzr:
    exec:
      - bzr
      - /usr/bin/bzr
    env:
      BZR_PROGRESS_BAR: text
    probe:
      exists:
        - .bzr
    variables:
      BRANCH_NAME: ${EXEC} nick
  git:
    exec:
      - git
      - /usr/bin/git
    probe:
      exists:
        - .git
    variables:
      BRANCH_NAME: ${EXEC} rev-parse --abbrev-ref HEAD
```

## Options

### Name

The name of the backend, note that if the implementation and backend name match then certain features exist - as noted above. Also this should match the [Name spec](/docs/spec/naming.md).

### Exec

The exec is a list of possible paths to the executable to use for this backend.

The exec selected will either be the first that passes a file exists check or if this fails the first in the list, this usually then means you put short names at the start and full absolute paths towards the end of the list.

```
backend:
  bzr:
    exec:
      - bzr
      - /usr/bin/bzr
```

### Env

The env is a key-value store of the environment variables to use with this backend.

### Probe

A command that is run when trying to autodetect which backend to use.

For example, say there is a workflow vcs and two backends bzr and git. The user may just run `a vcs status` in the folder, and the default vcs implementation maybe set to auto.

In this case aliaser will go through each implementation, picking the corresponding backend. It will then run each probe until one is valid, once this happens that backend and implementation are chosen.

The order in which probes will be run is as follows.

  * Exists
  * Sum
  * Contains


#### Contains

A key-value store of files and a string to search for inside the file, if the string exists then the probe is successful.

TODO: The path to the file is relative?
TODO: not implemented yet!

#### Exists

A simple check if the file or folder exists, can be relative or absolute.

#### Sum

The following sum types are supported

  * md5sum
  * sha256
  * sha512

These are a key-value store of files and the sum to match, if a file and sum match the probe is successful.


TODO: The path to the file is relative?
TODO: not implemented yet!

### Variables

The variables is a key-value store of possible values that can be in a command but need to be executed at run-time.

Eg. `BRANCH_NAME: ${EXEC} nick` could be used to give the name of the current branch and then used in the command as `${BRANCH_NAME}`.

Note: that the executable in the variables must be the backend as the parent, eg either `${EXEC}` or `${EXEC:backend}`.

## Future

  * Should be able to declare validated runner versions (eg git==2.11.*)
  * Exec doesn't know about PATH and should the PATH from the host be always copied to runner?
  * Contains needs to be implemented in probing
  * Sum needs to be implemented in probing
  * Variable names should only be uppercase (to not conflict with workflow arguments)
