
# Format

This is the general format for all configuration files.

## Definition

```
name: NAME
format: VERSION
```

## Example

```
name: vcs
format: 1
```

## Options

### Name

The name is a unique name across all configuration files that can be used to identify the configuration, this should match the [Name spec](/docs/spec/naming.md).

### Format

This defines the version of the format of this file, if the parser finds a version which isn't expected it may reject the configuration file.

## Future

  * Allow for excluding files by name - eg `a --exclude=vcs-bzr` would skip a file with `name: vcs-bzr`
