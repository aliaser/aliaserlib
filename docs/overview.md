
# Overview

This describes the general flow of how the engine interprets a command.

Input command (`vcs:git clone repo`) is split into workflow, implementation, command and arguments.

The command and arguments are then matched against the definition to check that they are valid.

If they are valid then the parser runs over the arguments converted them into the output commands.
Any pre and post hooks are also calculated and processed. Any variables that need to included are calculated by running them in the current directory.

Once the commands have been built they are run in turn, each starting with the environment specified by the backend and running from the current directory (unless otherwise specified).

The output of each of the command is then read and streamed to the output stream. Note that here filters may run on the output as the packets are passed upwards.

The stream is then given to the client which can display to the output buffer.


input -> workflow, implementation -> command, arguments -> parser, variables -> exec, env -> filter, output stream
