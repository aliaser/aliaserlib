
# Environment Variables

The following environment variables can be set to change properties of the library.

| Env var | Purpose |
|:-------:|:-------:|
| ALIASER_EXECUTER | Which executer to use: cmd, mock           |
| ALIASER_LOG      | Log level: trace, debug, info, warn, error |
| ALIASER_SYS_CONF | Location of system configuration files     |
| ALIASER_USR_CONF | Location of user configuration files       |
