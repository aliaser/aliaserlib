
# Configuration Locations

Aliaser uses three levels of configuration files.

  * System level
  * User level
  * Project level

It iterates from the system, to user, to the project level, loading each configuration file found. This means that you are able to stack changes in later configuration files upon the earlier (system) ones.

In a Linux environment this means that the configuration to be loaded will usually be

  * `/usr/share/aliaser/conf/`
  * `/home/$USER/.aliaser/`
  * `$PWD/.aliaser`

Note: if a folder is given as a path to scan, any subfolders will also be scanned.

## System level

Firstly the environment variable `ALIASER_SYS_CONF` is checked, if this is set then this is loaded as the path for system level configuration.

If the environment variable is not set then it will use the default install path, currently `/usr/share/aliaser/conf`.

## User level

Firstly the environment variables `ALIASER_USR_CONF` is checked, if this is set then this is loaded as the path for user level configuration.

If the environment variable is not set then it will look for the environment variable `HOME` and use the folder `.aliaser` inside this.

## Project level

This takes the current directory from the environment and uses the child folder `.aliaser`.

## Future

  * System level loop through XDG_CONFIG_DIRS / XDG_DATA_DIRS ?
  * System level have 'story' ? eg `/usr/lib/aliaser/story/<story>` then each story contains /bin and /defs and /bin is added to PATH (this allows use of flatpak extensions)
  * User level use XDG_CONFIG_HOME/.aliaser ?
  * Project level be a folder or only a file (eg .aliaser.yaml) or both ?
