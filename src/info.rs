/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::fmt;
use std::cmp::Ordering;

static VERSION_MAJOR: u64 = 1;
static VERSION_MINOR: u64 = 0;

#[derive(Eq)]
pub struct Version {
    major: u64,
    minor: u64,
}

impl fmt::Display for Version {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}.{}", self.major, self.minor)
    }
}

impl Ord for Version {
    fn cmp(&self, other: &Version) -> Ordering {
        match self.major.cmp(&other.major) {
            Ordering::Equal => {
                self.minor.cmp(&other.minor)
            }
            order => order
        }
    }
}

impl PartialOrd for Version {
    fn partial_cmp(&self, other: &Version) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl PartialEq for Version {
    fn eq(&self, other: &Version) -> bool {
        self.major == other.major && self.minor == other.minor
    }
}

pub fn version() -> Version {
    return Version {
        major: VERSION_MAJOR,
        minor: VERSION_MINOR,
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn fmt() {
        let version = Version { major: 1, minor: 2, };
        assert_eq!("1.2", format!("{}", version));
    }

    #[test]
    fn eq() {
        let a = Version { major: 2, minor: 2, };
        let b = Version { major: 2, minor: 2, };
        assert!(a == b);
    }

    #[test]
    fn gt_major() {
        let a = Version { major: 3, minor: 2, };
        let b = Version { major: 2, minor: 2, };
        assert!(a > b);
    }

    #[test]
    fn gt_minor() {
        let a = Version { major: 2, minor: 3, };
        let b = Version { major: 2, minor: 2, };
        assert!(a > b);
    }

    #[test]
    fn lt_major() {
        let a = Version { major: 1, minor: 2, };
        let b = Version { major: 2, minor: 2, };
        assert!(a < b);
    }

    #[test]
    fn lt_minor() {
        let a = Version { major: 2, minor: 1, };
        let b = Version { major: 2, minor: 2, };
        assert!(a < b);
    }

    #[test]
    fn major_minor() {
        assert_eq!(VERSION_MAJOR, version().major);
        assert_eq!(VERSION_MINOR, version().minor);
    }
}
