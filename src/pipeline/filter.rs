/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use common::Packet;

pub trait PipelineFilter {
    /// Process a given [`Packet`] of data
    ///
    /// This is used to run filters on the output of commands, such as simple
    /// find and replace.
    ///
    /// [`Packet`]: ../common/struct.Filter.html
    fn process_packet(&self, packet: Packet) -> Packet;
}


#[cfg(test)]
pub mod helper {
    use super::*;

    /// A mock of  PipelineFilter
    ///
    /// In stdout it replaces "a" with "b"
    /// In stderr it replaces "c" with "d"
    pub struct TestPipelineFilter {

    }

    impl TestPipelineFilter {
        pub fn new() -> Box<Self> {
            Box::new(Self {})
        }
    }

    impl PipelineFilter for TestPipelineFilter {
        fn process_packet(&self, mut packet: Packet) -> Packet {
            if let Some(stdout) = packet.stdout {
                packet.stdout = Some(stdout.replace("a", "b"));
            }

            if let Some(stderr) = packet.stderr {
                packet.stderr = Some(stderr.replace("c", "d"));
            }

            packet
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use pipeline::filter::helper::TestPipelineFilter;

    #[test]
    fn test_process_packet_no_change() {
        let filter = TestPipelineFilter::new();
        let mut packet = Packet::new();
        packet.stderr = Some("1234".to_owned());
        packet.stdout = Some("9876".to_owned());

        packet = filter.process_packet(packet);

        assert!(packet.stderr.is_some());
        assert!(packet.stdout.is_some());

        assert_eq!("1234", packet.stderr.unwrap());
        assert_eq!("9876", packet.stdout.unwrap());
    }

    #[test]
    fn test_process_packet_stderr() {
        let filter = TestPipelineFilter::new();
        let mut packet = Packet::new();
        packet.stderr = Some("1a3c".to_owned());

        packet = filter.process_packet(packet);

        assert!(packet.stderr.is_some());
        assert_eq!("1a3d", packet.stderr.unwrap());
    }

    #[test]
    fn test_process_packet_stdout() {
        let filter = TestPipelineFilter::new();
        let mut packet = Packet::new();
        packet.stdout = Some("9a7c".to_owned());

        packet = filter.process_packet(packet);
        assert!(packet.stdout.is_some());
        assert_eq!("9b7c", packet.stdout.unwrap());
    }
}
