/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};
use std::vec::Vec;

use config::Config;
use error::LibError;
use utils;


/// This is the original `Request` from the user, which is stored in the
/// [`Pipeline`]. This allows builders onto the [`Pipeline`] to make descisions
/// as to which chunks need adding or editing.
///
/// [`Pipeline`]: struct.Pipeline.html
pub struct Request {
    arguments: Vec<String>,
    attributes: BTreeMap<String, String>,
    command: String,
    directory: PathBuf,
    environment: BTreeMap<String, String>,
    implementation: String,
    workflow: String
}

impl Request {
    pub fn arguments(&self) -> &Vec<String> {
        &self.arguments
    }

    /// Attributes which are extracted from the given arguments
    pub fn attributes(&self) -> &BTreeMap<String, String> {
        &self.attributes
    }

    pub fn command(&self) -> &str {
        &self.command
    }

    pub fn directory(&self) -> &Path {
        &self.directory
    }

    pub fn environment(&self) -> &BTreeMap<String, String> {
        &self.environment
    }

    pub fn from(config: &Config, directory: &Path, environment: &BTreeMap<String, String>, parts: &Vec<String>) -> Result<Request, LibError> {
        // we need at least two parts, [workflow, command, ... ]
        match parts.len() {
            0 => return Err(lib_error!(LibError::Request, "Request expected workflow and command")),
            1 => return Err(lib_error!(LibError::Request, "Request expected a command")),
            _ => {}
        }

        // Split the workflow from :auto, :auto/git, :git
        // This also loads the default impl if none was given
        let (workflow, mut implementation) = utils::split_out_workflow(config, &parts[0])?;
        let ref command = parts[1];

        // This checks if the command is standalone and then using the given implementation
        // discovers which implementation should be used (potentially probing the directory)
        // FIXME: this should take a directory as well ? (to probe)
        // TODO: should this be in Utils or part of this Request ?
        implementation = utils::probe_or_fallback(config, &workflow, &implementation, &command)?;

        // Make the basic request
        let mut request = Request {
            arguments: parts[2..].to_vec(),
            attributes: BTreeMap::new(),
            command: command.to_owned(),
            directory: directory.to_path_buf(),
            environment: environment.to_owned(),
            implementation: implementation,
            workflow: workflow
        };

        // Extract the attributes from the arguments into a map
        request.extract_attributes(config)?;

        Ok(request)
    }

    pub fn implementation(&self) -> &str {
        &self.implementation
    }

    pub fn workflow(&self) -> &str {
        &self.workflow
    }
}

// FIXME: private parts from old code that need refactoring (move into Utils?)
impl Request {
    // TODO: should these be in Utils ?
    // console needs this as well, can we have generic function somewhere? or is priv fine? maybe Utils?
    fn arg_is_optional(arg: &str) -> bool {
        arg.starts_with("--")
    }

    // On an optional argument, remove the -- to leave just the name
    fn arg_optional_name(arg: &str) -> String {
        arg[2..].to_owned()
    }

    fn extract_attributes(&mut self, config: &Config) -> Result<&mut Request, LibError> {
        // TODO: this needs braking up
        // Find the workflow and command definition
        if let Some(workflow) = config.workflow(self.workflow.as_str()) {
            if let Some(command) = workflow.command(self.command.as_str()) {
                // Fill in defaults for optional arguments into attributes
                for (k, v) in command.args() {
                    if v.optional() {
                        self.attributes.insert(k.clone(), v.default_value().to_owned());
                    }
                }

                // Go through the positional arguments first, these should always come first for now
                let mut i: usize = 0;

                // TODO: should we use order? or just loop through commands finding !optional ones?
                for k in command.order() {
                    if i < self.arguments.len() {
                        self.attributes.insert(k.clone(), self.arguments[i].clone());
                    } else {
                        return Err(lib_error!(LibError::Request, format!("Expected {} positional arguments, found {}. Expected positional arguments: {:?}", command.order().len(), self.arguments.len(), command.order())));
                    }

                    i += 1;
                }

                // Read optional arguments left in self.arguments and override defaults written to map
                while i < self.arguments.len() {
                    if Request::arg_is_optional(self.arguments[i].as_str()) {
                        // Check optional argument name is an expected name
                        if !command.args().contains_key(Request::arg_optional_name(self.arguments[i].as_str()).as_str()) {
                            return Err(lib_error!(LibError::Request, format!("Unknown optional argument: {:?}", Request::arg_optional_name(self.arguments[i].as_str()))));
                        }

                        // Check if this is a flag or a key + value
                        if i + 1 < self.arguments.len() && !Request::arg_is_optional(self.arguments[i + 1].as_str()) {
                            self.attributes.insert(Request::arg_optional_name(self.arguments[i].as_str()), self.arguments[i + 1].to_owned());
                            i += 1;
                        } else {
                            self.attributes.insert(Request::arg_optional_name(&self.arguments[i].as_str()), "true".to_owned());
                        }
                    } else {
                        // Unexpected positional arg
                        return Err(lib_error!(LibError::Request, format!("Unexpected positional argument found: {:?}", self.arguments[i])));
                    }

                    i += 1;
                }

                Ok(self)
            } else {
                Err(lib_error!(LibError::Request, format!("Command not found: {:?} for workflow: {:?}", self.command, self.workflow)))
            }
        } else {
            Err(lib_error!(LibError::Request, format!("Workflow not found: {:?}", self.workflow)))
        }
    }
}

#[cfg(test)]
pub mod helper {
    use super::*;

    use config::helper::make_config;

    pub fn make_request() -> Request {
        let config = make_config();
        let directory = PathBuf::from("/test/path");
        let mut env = BTreeMap::new();
        env.insert("env-a".to_owned(), "env-b".to_owned());
        let args = vec!("wfl:a".to_owned(), "cmd".to_owned(), "1st".to_owned(), "2nd".to_owned());

        Request::from(&config, &directory, &env, &args).unwrap()
    }

    pub fn make_request_empty() -> Request {
        let config = make_config();
        let directory = PathBuf::from("/test/path");
        let env = BTreeMap::new();
        let args = vec!("wfl:a".to_owned(), "empty".to_owned());

        Request::from(&config, &directory, &env, &args).unwrap()
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use config::helper::make_config;
    use pipeline::helper::make_request;

    #[test]
    fn test_from() {
        let request = make_request();

        assert_eq!(2, request.arguments().len());
        assert_eq!("1st".to_owned(), request.arguments()[0]);
        assert_eq!("2nd".to_owned(), request.arguments()[1]);

        assert!(request.attributes().contains_key("first"));
        assert_eq!("1st", request.attributes().get("first").unwrap());

        assert!(request.attributes().contains_key("second"));
        assert_eq!("2nd", request.attributes().get("second").unwrap());

        assert_eq!("cmd", request.command());

        assert_eq!("/test/path", request.directory().to_str().unwrap());

        assert!(request.environment().contains_key("env-a"));
        assert_eq!("env-b", request.environment().get("env-a").unwrap());

        assert_eq!("a", request.implementation());

        assert_eq!("wfl", request.workflow());
    }

    #[test]
    fn test_from_no_command() {
        let config = make_config();
        let request = Request::from(&config, &PathBuf::from("/"), &BTreeMap::new(), &vec!("wfl".to_owned()));

        assert!(request.is_err());
    }

    #[test]
    fn test_from_no_workflow() {
        let config = make_config();
        let request = Request::from(&config, &PathBuf::from("/"), &BTreeMap::new(), &vec!());

        assert!(request.is_err());
    }

    #[test]
    fn test_from_split_wfl_impl_fail() {
        let config = make_config();
        // Using three parts to the workflow:impl will cause an error
        let request = Request::from(&config, &PathBuf::from("/"), &BTreeMap::new(), &vec!("wfl:impl:err".to_owned(), "cmd".to_owned()));

        assert!(request.is_err());
    }

    // TODO: test_from_probe
    // we need to have config to test probing (standalone cmds)

    // TODO: test_from_extract_attributes_fail
}
