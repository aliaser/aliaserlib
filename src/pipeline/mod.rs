/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Representation of the commands required to execute a given [`Request`] for a
//! given [`Config`].
//!
//! [`Config`]: ../config/struct.Config.html
//! [`Request`]: struct.Request.html
mod chunk;
mod chunkref;
mod filter;
mod request;

pub mod builders;

use std::slice::{Iter, IterMut};
use std::vec::Vec;

use config::Config;
use error::LibError;
use pipeline::builders::PipelineBuilder;

pub use pipeline::chunkref::PipelineChunkRef;
pub use pipeline::chunk::PipelineChunk;
pub use pipeline::filter::PipelineFilter;
pub use pipeline::request::Request;


/// A `Pipeline` holds the series of commands which will be executed by the
/// chosen executer.
///
/// The `Pipeline` is made up of [`PipelineChunk`], which holds:
///
///   * Pre-processors (eg variables used in the command which need to be
///     executed in the directory before the command runs to determine their
///     value)
///   * The command itself
///   * Post filters
///   * ID - so that other commands know where this chunk was generated from
///
/// [`PipelineChunk`]: struct.PipelineChunk.html
pub struct Pipeline<'c, 'r> {
    /// List of [`PipelineChunk`] that will be executed first to last.
    chunk: Vec<PipelineChunk>,
    config: &'c Config,
    request: &'r Request,
}

impl<'c, 'r> Pipeline<'c, 'r> {
    /// Adds the given [`PipelineChunk`] to the end of the [`Pipeline`]
    ///
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    pub fn append(&mut self, chunk: PipelineChunk) {
        trace!("Adding chunk to pipeline with ChunkRef: {}", chunk.reference());
        self.chunk.push(chunk);
    }

    /// Takes a list of [`PipelineBuilder`] and uses them to build the chunks.
    ///
    /// [`PipelineBuilder`]: trait.PipelineBuilder.html
    pub fn build(&mut self, builders: &Vec<Box<PipelineBuilder>>) -> Result<&mut Pipeline<'c, 'r>, LibError> {
        // Run each builder over the pipeline and error if they error
        for builder in builders.iter() {
            builder.build(self)?;
        }

        Ok(self)
    }

    /// Reference to the [`Config`] being used for this `Pipeline`
    ///
    /// [`Config`]: ../config/struct.Config.html
    pub fn config(&self) -> &'c Config {
        self.config
    }

    /// The number of chunks in this `Pipeline`
    pub fn chunks(&self) -> usize {
        self.chunk.len()
    }

    /// Returns the default builders that should be used to build the `Pipeline`
    pub fn default_builders() -> Vec<Box<PipelineBuilder>> {
        vec!(
            builders::CommandBuilder::new(),
            builders::BackendBuilder::new(),
            builders::FilterBuilder::new(),
            builders::RequestEnvBuilder::new(),
        )
    }

    /// Builds a `Pipeline` from a given [`Config`] and [`Request`]
    ///
    /// [`Config`]: ../config/struct.Config.html
    /// [`Request`]: struct.Request.html
    pub fn from(config: &'c Config, request: &'r Request) -> Pipeline<'c, 'r> {
        Pipeline {
            chunk: vec!(),
            config: config,
            request: request
        }
    }

    /// Provides an iterator over the chunks in the `Pipeline`.
    /// This is used when executers read the `Pipeline`.
    pub fn iter(&self) -> Iter<PipelineChunk> {
        self.chunk.iter()
    }

    /// Provides a mutable iterator over the chunks in the `Pipeline`.
    /// This is useful when editing the chunks.
    pub fn iter_mut(&mut self) -> IterMut<PipelineChunk> {
        self.chunk.iter_mut()
    }

    /// Adds the given [`PipelineChunk`] to the start of the `Pipeline`
    ///
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    // TODO: rename to pre_append ?
    pub fn preappend(&mut self, chunk: PipelineChunk) {
        self.chunk.insert(0, chunk);
    }

    /// The original [`Request`] of the user
    ///
    /// [`Request`]: struct.Request.html
    pub fn request(&self) -> &'r Request {
        self.request
    }
}

#[cfg(test)]
pub mod helper {
    pub use pipeline::chunk::helper::{
        make_pipeline_chunk,
        make_pipeline_chunk_from_task,
        make_pipeline_chunk_with_data
    };
    pub use pipeline::filter::helper::TestPipelineFilter;
    pub use pipeline::request::helper::{make_request, make_request_empty};
}

#[cfg(test)]
mod test {
    use super::*;

    use std::collections::BTreeMap;
    use std::path::PathBuf;

    use common::helper::make_command_task;
    use config::helper::make_config;
    use pipeline::helper::{make_request, make_pipeline_chunk, make_pipeline_chunk_with_data};

    #[test]
    fn test_append() {
        let config = make_config();
        let request = make_request();
        let mut pipeline = Pipeline::from(&config, &request);
        let chunk = make_pipeline_chunk_with_data();

        assert_eq!(0, pipeline.chunks());
        pipeline.append(chunk);
        assert_eq!(1, pipeline.chunks());
    }

    #[test]
    fn test_build() {
        // Test that we are able to set up pipeline builders
        let config = make_config();
        let directory = PathBuf::from("/target");
        let request = Request::from(&config, directory.as_path(), &BTreeMap::new(), &vec!("wfl:a".to_owned(), "cmd".to_owned(), "1st".to_owned(), "2nd".to_owned())).unwrap();

        trace!("Request args: {:?} attributes: {:?}", request.arguments(), request.attributes());
        let mut pipeline = Pipeline::from(&config, &request);

        assert!(pipeline.build(&Pipeline::default_builders()).is_ok());
    }

    #[test]
    fn test_build_error() {
        let config = Config::default();
        let request = make_request();
        let mut pipeline = Pipeline::from(&config, &request);

        let builders: Vec<Box<PipelineBuilder>> = vec!(builders::CommandBuilder::new(), builders::BackendBuilder::new(), builders::FilterBuilder::new(), builders::RequestEnvBuilder::new());

        // Builders expect some data to work, so this should fail
        assert!(pipeline.build(&builders).is_err());
    }

    #[test]
    fn test_default_builders() {
        let builders = Pipeline::default_builders();
        assert_eq!(false, builders.is_empty());
    }

    #[test]
    fn test_from() {
        let config = make_config();
        let request = make_request();
        let pipeline = Pipeline::from(&config, &request);

        assert!(pipeline.config().filter("filter-a").is_some());
        assert_eq!(0, pipeline.chunks());
        assert_eq!("/test/path", pipeline.request().directory().to_str().unwrap());
    }

    #[test]
    fn test_iter() {
        let config = make_config();
        let request = make_request();
        let mut pipeline = Pipeline::from(&config, &request);
        let chunk = make_pipeline_chunk_with_data();
        pipeline.append(chunk);

        let mut iter = pipeline.iter();
        let first = iter.next();

        assert!(first.is_some());
        assert!(first.unwrap().pre_processors().contains_key("variable-a"));

        assert!(iter.next().is_none());
    }

    #[test]
    fn test_iter_mut() {
        let config = make_config();
        let request = make_request();
        let mut pipeline = Pipeline::from(&config, &request);
        let chunk = make_pipeline_chunk_with_data();
        pipeline.append(chunk);

        {
            let mut iter = pipeline.iter_mut();
            let first = iter.next();

            assert!(first.is_some());
            let mut first_unwrap = first.unwrap();
            assert_eq!(true, first_unwrap.pre_processors().contains_key("variable-a"));
            assert_eq!(false, first_unwrap.pre_processors().contains_key("variable-iter-mut"));

            first_unwrap.add_pre_processor("variable-iter-mut", make_command_task());

            assert_eq!(true, first_unwrap.pre_processors().contains_key("variable-iter-mut"));

            assert!(iter.next().is_none());
        }

        {
            let mut iter = pipeline.iter();
            let first = iter.next();

            assert!(first.is_some());
            let first_unwrap = first.unwrap();
            assert!(first_unwrap.pre_processors().contains_key("variable-a"));
            assert!(first_unwrap.pre_processors().contains_key("variable-iter-mut"));

            assert!(iter.next().is_none());
        }
    }

    #[test]
    fn test_preappend() {
        let config = make_config();
        let request = make_request();
        let mut pipeline = Pipeline::from(&config, &request);
        let chunk = make_pipeline_chunk_with_data();
        pipeline.append(chunk);

        {
            let mut iter = pipeline.iter();
            let first = iter.next();

            assert!(first.is_some());
            assert_eq!(true, first.unwrap().pre_processors().contains_key("variable-a"));
            assert_eq!(false, first.unwrap().pre_processors().contains_key("pre-append"));

            assert!(iter.next().is_none());
        }

        let mut chunk_b = make_pipeline_chunk();
        chunk_b.add_pre_processor("pre-append", make_command_task());
        pipeline.preappend(chunk_b);

        {
            let mut iter = pipeline.iter();

            let first = iter.next();
            assert!(first.is_some());
            assert_eq!(false, first.unwrap().pre_processors().contains_key("variable-a"));
            assert_eq!(true, first.unwrap().pre_processors().contains_key("pre-append"));

            let second = iter.next();
            assert!(second.is_some());
            assert_eq!(true, second.unwrap().pre_processors().contains_key("variable-a"));
            assert_eq!(false, second.unwrap().pre_processors().contains_key("pre-append"));

            assert!(iter.next().is_none());
        }
    }
}
