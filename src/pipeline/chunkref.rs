/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::fmt;
use std::vec::Vec;

/// A store of details about the [`PipelineChunk`]
///
/// These are used as a reference for [`PipelineBuilder`] so that they can query
/// where this [`PipelineChunk`] was created from.
///
/// For example, the [`FilterBuilder`] needs to know which command or hook was
/// used to create the [`PipelineChunk`] so that it can find the relevant
/// post-filters to add to the [`PipelineChunk`].
///
/// [`FilterBuilder`]: builders/struct.FilterBuilder.html
/// [`PipelineBuilder`]: builders/trait.PipelineBuilder.html
/// [`PipelineChunk`]: struct.PipelineChunk.html
#[derive(Clone)]
pub struct PipelineChunkRef {
    details: BTreeMap<String, String>
}

impl PipelineChunkRef {
    /// Retrieve the value for the given key in the detail.
    pub fn get(&self, key: &str) -> Option<&str> {
        match self.details.get(key) {
            Some(value) => Some(value.as_ref()),
            None => None
        }
    }

    /// Insert a new key-value detail to the reference
    ///
    /// Note that you cannot overwrite a key that has already been added,
    /// in this case the function will return false.
    ///
    /// This is so that [`PipelineBuilder`] later in the [`Pipeline`] know that
    /// if an earlier [`PipelineChunk`] added a key-value detail it will still
    /// be there.
    ///
    /// [`Pipeline`]: struct.Pipeline.html
    /// [`PipelineBuilder`]: builders/trait.PipelineBuilder.html
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    // TODO: should this return Result ? and then error with ?
    pub fn insert(&mut self, key: &str, value: &str) -> bool {
        if !self.details.contains_key(key) {
            self.details.insert(key.to_owned(), value.to_owned());
            true
        } else {
            false
        }
    }

    /// Construct a new `PipelineChunkRef`
    pub fn new() -> Self {
        Self {
            details: BTreeMap::new()
        }
    }
}

impl fmt::Display for PipelineChunkRef {
    /// Formats the `PipelineChunkRef` into a human readable format
    ///
    /// This is useful as debug lines format the `PipelineChunkRef`
    // TODO: should it be like a dict? or just root/workflow/impl + { }
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut string: Vec<String> = vec!();

        //string.push(format!("{:?}/{:?}/{:?}", self.get("root"), self.get("workflow"), self.get("implementation")));

        for (key, value) in &self.details {
            string.push(format!("{:?}: {:?}", key, value));
        }

        write!(f, "PipelineChunkRef {{ {} }}", string.join(", "))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_fmt() {
        let mut chunk = PipelineChunkRef::new();
        chunk.insert("a", "b");
        chunk.insert("c", "d");

        let string = format!("{}", chunk);

        assert_eq!("PipelineChunkRef { \"a\": \"b\", \"c\": \"d\" }", string);
    }

    #[test]
    fn test_get_none() {
        let chunk = PipelineChunkRef::new();

        assert!(chunk.get("test").is_none());
    }

    #[test]
    fn test_insert_existing() {
        let mut chunk = PipelineChunkRef::new();
        assert_eq!(true, chunk.insert("a", "b"));
        assert_eq!(false, chunk.insert("a", "b"));
    }

    #[test]
    fn test_insert_get() {
        let mut chunk = PipelineChunkRef::new();
        assert_eq!(true, chunk.insert("test-a", "value-a"));

        assert!(chunk.get("test-a").is_some());
        assert_eq!("value-a", chunk.get("test-a").unwrap());

        assert!(chunk.get("test").is_none());
    }

    #[test]
    fn test_new() {
        let chunk = PipelineChunkRef::new();

        assert!(chunk.details.is_empty());
    }
}
