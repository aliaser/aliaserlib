/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

use common::CommandTask;
use error::LibError;
use pipeline::{Pipeline, PipelineBuilder};

/// Copies the environment specified into the Pipeline
///
/// Where an env var is already set the value will not be overwritten. Except
/// when it is defined as being extendable.
///
/// For example, if the request env has PATH set to `/tmp/bin` and the PATH var
/// is already set to `${PATH}:/my/bin`, this these will be joint to `/tmp/bin:/my/bin`.
// TODO: should this also do $PATH ?
pub struct RequestEnvBuilder {

}

impl PipelineBuilder for RequestEnvBuilder {
    fn build<'c, 'r, 'p>(&self, pipeline: &'p mut Pipeline<'c, 'r>) -> Result<&'p mut Pipeline<'c, 'r>, LibError> {
        let request_env = pipeline.request().environment();

        for chunk in pipeline.iter_mut() {
            // Inject environment into pre_processors
            for task in chunk.pre_processors_mut().values_mut() {
                RequestEnvBuilder::inject_env(task, request_env);
            }

            // Inject into the command
            RequestEnvBuilder::inject_env(chunk.command_mut(), request_env);
        }

        Ok(pipeline)
    }

    fn new() -> Box<RequestEnvBuilder> {
        Box::new(RequestEnvBuilder {})
    }
}

impl RequestEnvBuilder {
    /// Injects the given environment variables into the given task
    ///
    /// If there is an existing value merge_or_ignore is tried
    fn inject_env(task: &mut CommandTask, request_env: &BTreeMap<String, String>) {
        let mut task_env = task.env_mut();

        for (key, request_value) in request_env {
            // If there is an env var already existing then try to merge
            // Note: request is the base and config is the layer
            // if config has $KEY:value then they will be merged
            if let Some(config_value) = task_env.get_mut(key) {
                *config_value = RequestEnvBuilder::merge_or_ignore(key, request_value, config_value);
            }

            // If there was no env var then set the one from the request
            // TODO: should this be this way around?
            // if BZR_PROGRESS_BAR=text in yaml should env in request overwrite?
            task_env.entry(key.to_owned()).or_insert(request_value.to_owned());
        }
    }

    /// If the layer contains ${key} then replace the base into ${key}
    /// otherwise return just the layer
    ///
    /// BASE: PATH = /my/bin
    /// LAYER: PATH = ${PATH}:/tmp/bin
    ///
    /// /my/bin:/tmp/bin
    fn merge_or_ignore(key: &str, base: &str, layer: &str) -> String {
        layer.replace(format!("${{{}}}", key).as_str(), base)
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: test_build
    // TODO: test_inject_env

    #[test]
    fn test_merge_or_ignore() {
        // Merge of ${PATH}:/tmp/bin and /my/bin
        assert_eq!("/my/bin:/tmp/bin".to_owned(), RequestEnvBuilder::merge_or_ignore("PATH", "/my/bin", "${PATH}:/tmp/bin"));

        // Ignore as no match
        assert_eq!("${PATH}:/tmp/bin".to_owned(), RequestEnvBuilder::merge_or_ignore("TEST", "/my/bin", "${PATH}:/tmp/bin"));

        // Ignore as flipped
        assert_eq!("/my/bin".to_owned(), RequestEnvBuilder::merge_or_ignore("PATH", "${PATH}:/tmp/bin", "/my/bin"));
    }
}
