/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::path::PathBuf;

use common::CommandTask;
use config::Config;
use error::LibError;
use pipeline::{Pipeline, PipelineBuilder, PipelineChunk, PipelineChunkRef, Request};
use utils;

/// Builds implementation commands from the given [`Request`]
///
/// This [`PipelineBuilder`] uses the [`Request`] to pick which implementation
/// command to use. Once found this builder loops through each exec line adding
/// each one as a [`PipelineChunk`] to the [`Pipeline`].
///
/// [`Pipeline`]: ../struct.Pipeline.html
/// [`PipelineBuilder`]: ../trait.PipelineBuilder.html
/// [`Request`]: ../struct.Request.html
pub struct CommandBuilder {

}

impl PipelineBuilder for CommandBuilder {
    fn build<'c, 'r, 'p>(&self, pipeline: &'p mut Pipeline<'c, 'r>) -> Result<&'p mut Pipeline<'c, 'r>, LibError> {
        // Get the exec lines of the implementaton
        // This will be a list of lists - as the inner list have their own filters
        let execs = CommandBuilder::find_exec_request_from_config(pipeline.config(), pipeline.request())?;

        // ID needs to be workflow/implementation/command/n/m
        let mut base_ref = PipelineChunkRef::new();
        base_ref.insert("root", "command");
        base_ref.insert("workflow", pipeline.request().workflow());
        base_ref.insert("implementation", pipeline.request().implementation());
        base_ref.insert("command", pipeline.request().command());

        let mut n = 0;
        for exec_set in execs {
            // Loop through each exec in the exec set
            let mut m = 0;
            for exec in exec_set {
                // Add command_set and command_set_n
                let mut reference = base_ref.clone();
                reference.insert("command_set", format!("{}", n).as_str());
                reference.insert("command_set_n", format!("{}", m).as_str());

                if let Some((program, args)) = utils::string_to_args(exec).split_first() {
                    // Build the Command
                    // Note that program (${EXEC}) is converted in BackendBuilder
                    // and that env is loaded there as well
                    let command = CommandTask::new(args.to_vec(), pipeline.request().directory().to_path_buf(), BTreeMap::new(), PathBuf::from(program));

                    // Make a PipelineChunk from the Command and reference
                    pipeline.append(PipelineChunk::new(command, reference));
                } else {
                    return Err(lib_error!(format!("Could not find program and args in exec line: {:?}", exec)));
                }

                m += 1;
            }

            n += 1;
        }

        Ok(pipeline)
    }

    fn new() -> Box<CommandBuilder> {
        Box::new(CommandBuilder {})
    }
}

impl CommandBuilder {
    /// Exec lines for the request from the config as a list of lists
    /// The inner list have the same filter
    fn find_exec_request_from_config<'c, 'r>(config: &'c Config, request: &'r Request) -> Result<Vec<&'c Vec<String>>, LibError> {
        // TODO: split this into separate function ? (even maybe in Config itself?)
        // Filters uses same for find_command()
        if let Some(config_impl) = config.implementation(request.workflow(), request.implementation()) {
            if let Some(config_cmds) = config_impl.command(request.command()) {
                let mut execs: Vec<&'c Vec<String>> = vec!();

                for command in config_cmds {
                    execs.push(command.exec());
                }

                return Ok(execs);
            } else {
                Err(lib_error!(format!("Could not find command {:?} in the workflow and implementation {:?}:{:?}", request.command(), request.workflow(), request.implementation())))
            }
        } else {
            Err(lib_error!(format!("Could not find workflow or implementation {:?}:{:?}", request.workflow(), request.implementation())))
        }
    }
}

// TODO: tests!
