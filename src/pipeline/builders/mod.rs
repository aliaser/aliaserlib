/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */

//! Built-in builders which implement `PipelineBuilder`
//!
//! These builders implement core functionality, such as building commands,
//! finding executables, adding filters, adding hooks etc
//!
//! Below is a flow showing the order that builders should be in the `Pipeline`
//!
//! ```text
//! Command ->      -> Backend
//!            Hook ->
//!
//!                            -> Filter
//!                                        -> RequestEnv
//! ```
//!
//! For example the order could be
//! `Command`, `Hook`, `Backend`, `Filter`, `RequestEnv`
//! or a more minimal `Pipeline` could be
//! `Command`, `Backend`
//! this then has no support for hooks, filters or copying the environment from
//! the request.
mod command;
mod backend;
mod filter;
mod requestenv;

use pipeline::Pipeline;
use error::LibError;

pub use pipeline::builders::backend::BackendBuilder;
pub use pipeline::builders::command::CommandBuilder;
pub use pipeline::builders::filter::FilterBuilder;
pub use pipeline::builders::requestenv::RequestEnvBuilder;
// TODO: HookBuilder - later - from the request figure out which hooks


/// This is used for objects that want to add to the pipeline
///
/// For example, the builders for Commands, Filters etc implement this trait
/// and then are given to `Pipeline::build()`.
pub trait PipelineBuilder {
    fn build<'c, 'r, 'p>(&self, pipeline: &'p mut Pipeline<'c, 'r>) -> Result<&'p mut Pipeline<'c, 'r>, LibError>;
    /// Creates a new `PipelineBuilder` instance
    // TODO: remove new() from traits ? instead let that be impl specific ?
    fn new() -> Box<Self> where Self: Sized;
}

