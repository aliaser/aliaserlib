/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use common::Packet;
use config::{Config, Filter};
use error::LibError;
use pipeline::{PipelineChunkRef, Pipeline, PipelineBuilder, PipelineFilter};

/// Adds post-filters for each [`PipelineChunk`] in the [`Pipeline`]
///
/// Commands and hooks have post-filters, this [`PipelineBuilder`] uses the info
/// in the [`PipelineChunkRef`] to determine which command or hook the
/// [`PipelineChunk`] is and then add the relevant post-filters.
///
/// [`Pipeline`]: ../struct.Pipeline.html
/// [`PipelineBuilder`]: ../trait.PipelineBuilder.html
/// [`PipelineChunk`]: ../struct.PipelineChunk.html
/// [`PipelineChunkRef`]: ../struct.PipelineChunkRef.html
pub struct FilterBuilder {
    // TODO: could have blacklist: Vec<String> then have
    // FilterBuilder::blacklist_mut() and blacklist()
    // this allows client libraries to blacklist certain filters at runtime
}

impl PipelineBuilder for FilterBuilder {
    fn build<'c, 'r, 'p>(&self, pipeline: &'p mut Pipeline<'c, 'r>) -> Result<&'p mut Pipeline<'c, 'r>, LibError> {
        // get a ref to config, so we can have mut ref of pipeline below
        let ref config = pipeline.config();

        // Go through each chunk
        for chunk in pipeline.iter_mut() {
            // Try to find the original command of the chunk
            // This will either be an implementation or hook
            if let Some(filters) = FilterBuilder::command_for_chunk(config, chunk.reference()) {
                // Add the filters to the chunk
                for key in filters {
                    if let Some(filter) = config.filter(key) {
                        chunk.add_post_filter(key, Box::new(SimpleFilter::new(filter.clone())));
                    } else {
                        return Err(lib_error!(format!("Could not find filter: {:?}", key)));
                    }
                }
            } else {
                return Err(lib_error!(format!("Could not find command to add filter to, for the chunk: {}", chunk.reference())));
            }
        }

        Ok(pipeline)
    }

    fn new() -> Box<FilterBuilder> {
        Box::new(FilterBuilder {})
    }
}

impl FilterBuilder {
    fn command_for_chunk<'c>(config: &'c Config, reference: &PipelineChunkRef) -> Option<&'c Vec<String>> {
        // From the given refernce determine what was the type we need to find
        match reference.get("root") {
            Some("hook") => FilterBuilder::find_hook(config, reference),
            Some("command") => FilterBuilder::find_command(config, reference),
            _ => {
                warn!("Could not determine if a filter is required for chunk: {}", reference);
                None
            }
        }
    }

    /// Expects PipelineChunkRef to have workflow, implementation, command, command_set
    fn find_command<'c>(config: &'c Config, reference: &PipelineChunkRef) -> Option<&'c Vec<String>> {
        // Check and retrieve workflow, implementation, command, command_set
        // FIXME: doesn't fit any styling
        if let (
            Some(workflow),
            Some(implementation),
            Some(command),
            Some(command_set)
        ) = (
            reference.get("workflow"),
            reference.get("implementation"),
            reference.get("command"),
            reference.get("command_set")
        ) {
            // Extract usize part
            if let Ok(n) = command_set.parse::<usize>() {
                // Find the ImplementationCommand::filter() for the given parts
                if let Some(config_impl) = config.implementation(workflow, implementation) {
                    if let Some(config_cmds) = config_impl.command(command) {
                        if let Some(config_cmd) = config_cmds.get(n) {
                            return Some(config_cmd.filter());
                        }
                    }
                }
            } else {
                warn!("Expected number as the command_set in the PipelineChunkRef got: {:?}", command_set);
            }
        }

        None
    }

    /// Expects PipelineChunkRef to have workflow, implementation, command, command_set
    ///
    /// TODO: Hooks aren't implemented yet in Config, so do this later
    #[allow(unused_variables)]
    fn find_hook<'c>(config: &'c Config, parts: &PipelineChunkRef) -> Option<&'c Vec<String>> {
        None
    }
}

/// Simple implementation of Filters
///
/// Which uses a given Config Filter type as the definition.
struct SimpleFilter {
    filter: Filter,
}

impl SimpleFilter {
    /// Create a PipelineFilter from the given Config Type Filter
    fn new(filter: Filter) -> Self {
        Self {
            filter: filter,
        }
    }
}

impl PipelineFilter for SimpleFilter {
    /// Process the given packet using the defined Filter
    fn process_packet(&self, mut packet: Packet) -> Packet {
        // FIXME: for now only filter Char type
        if self.filter.is_char() {
            packet.stderr = self.filter.replace(packet.stderr);
            packet.stdout = self.filter.replace(packet.stdout);
        }

        packet
    }
}

// TODO: tests!
