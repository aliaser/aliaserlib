/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use regex::Regex;
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};

use common::CommandTask;
use error::LibError;
use pipeline::{Pipeline, PipelineBuilder, PipelineChunk};
use utils;


lazy_static! {
    /// Regex searcher for ${EXEC:backend}, to extract the backend
    static ref EXEC_BACKEND_RE: Regex = Regex::new(r"^\$\{EXEC:(?P<backend>[[:alnum:]-_]+)\}$").unwrap();
}


/// Finds the backend for commands and hooks
///
/// For example in commands `${EXEC}` or `${EXEC:backend}` are used to say that
/// the executable file path should be the one defined in the backend.
///
/// This [`PipelineBuilder`] does the following to each [`PipelineChunk`]
///
///  * Finds if the command is using `${EXEC}` and then picks the relevant backend
///  * Loads the variables defined in the backend into the pre-processors for
///    the [`PipelineChunk`]
///  * Loads the environment vars from the backend into the command, note the
///    same vars as set to the commands generated for any variables
///  * Replaces `${EXEC}` with path to the executable, note that this also
///    occurs for variables
///
/// Therefore this [`PipelineBuilder`] should be after [`CommandBuilder`] and
/// `HookBuilder`. As it requires the commands and hooks to exist.
///
/// Note: that this sets the found backend into the [`PipelineChunkRef`].
///
/// [`CommandBuilder`]: struct.CommandBuilder.html
/// [`PipelineBuilder`]: trait.PipelineBuilder.html
/// [`PipelineChunk`]: ../struct.PipelineChunk.html
/// [`PipelineChunkRef`]: ../struct.PipelineChunkRef.html
// [`HookBuilder`]: struct.HookBuilder.html  // FIXME: doesn't exist yet :-)
pub struct BackendBuilder {

}

impl PipelineBuilder for BackendBuilder {
    fn build<'c, 'r, 'p>(&self, pipeline: &'p mut Pipeline<'c, 'r>) -> Result<&'p mut Pipeline<'c, 'r>, LibError> {
        // get a ref to config, so we can have mut ref of pipeline below
        let ref config = pipeline.config();
        let ref request = pipeline.request();

        // Go through each chunk
        for chunk in pipeline.iter_mut() {
            // If the program exists then this is a direct command so skip
            if BackendBuilder::program_path_exists(chunk.command().program()) {
                trace!("Not searching for backend as program exists: {:?}", chunk.command().program());
                continue;
            }

            // Pick a backend that should be used
            if let Some(backend) = BackendBuilder::pick_backend(chunk.command().program().to_str().unwrap(), request.implementation()) {
                // Add the chosen backend to the the chunkref
                trace!("Chosen backend {:?} for program {:?}", backend, chunk.command().program());
                chunk.reference_mut().insert("backend", backend.as_str());

                // Find the backend in the config
                if let Some(config_backend) = config.backend(backend.as_str()) {
                    // Copy environment to chunk command
                    chunk.command_mut().env_mut().extend(config_backend.env().clone().into_iter());

                    // Replace the program in the command
                    if let Some(exec) = config_backend.executable() {
                        chunk.command_mut().program_replace(Path::new(exec));
                    } else {
                        return Err(lib_error!(format!("Could not find executable for backend: {:?}", backend)));
                    }

                    // Add pre-processor commands for the backend
                    // Note that this needs to be after setting up the chunk as
                    // it copies executable and environment variables from it
                    BackendBuilder::inject_variables(backend.as_str(), chunk, config_backend.variables())?;
                } else {
                    return Err(lib_error!(format!("Could not find chosen backend {:?} in config.", backend)));
                }
            } else {
                return Err(lib_error!(format!("No backend found for chunk command program: {:?}", chunk.command().program())));
            }
        }

        Ok(pipeline)
    }

    fn new() -> Box<BackendBuilder> {
        Box::new(BackendBuilder {})
    }
}

impl BackendBuilder {
    /// Return whether the given [`CommandTask`] requires the given variable
    fn command_contains_variable(command: &CommandTask, variable: &str) -> bool {
        let needle = format!("${{{}}}", variable);

        command.args().iter().any(|arg| arg.contains(&needle))
    }

    /// Return whether the given program exists
    ///
    /// TODO: this should search PATH ?
    /// TODO: should be in Utils::parser ?
    // Utils::parser::program_exists(&Request, &Path) -> bool
    fn program_path_exists(program: &Path) -> bool {
        program.exists()
    }

    fn inject_variables<'c>(backend: &str, chunk: &'c mut PipelineChunk, variables: &BTreeMap<String, String>) -> Result<&'c mut PipelineChunk, LibError> {
        for (key, value) in variables {
            // Check that the command for this command contains this variable
            if !BackendBuilder::command_contains_variable(chunk.command(), key) {
                continue;
            }

            if let Some((program, args)) = utils::string_to_args(value).split_first() {
                let mut exec = PathBuf::from(program);
                let mut env = BTreeMap::new();

                // Find the backend of the variable
                // if they match copy the environment and executable
                // TODO: variables might not have a backend? just straight bash ?
                match BackendBuilder::pick_backend(program, backend) {
                    Some(ref var_backend) if var_backend == backend => {
                        exec = PathBuf::from(chunk.command().program());
                        env = chunk.command().env().clone();
                    }
                    Some(var_backend) => {
                        return Err(lib_error!(format!("Expected variable backend {:?} to match chunk command backend {:?}", var_backend, backend)));
                    }
                    None => {}
                }

                // Add the pre-processor to the chunk
                // and copy the directory from the chunk command
                let directory = chunk.command().directory().to_path_buf();
                chunk.add_pre_processor(key, CommandTask::new(args.to_vec(), directory, env, exec));
            } else {
                return Err(lib_error!(format!("Could not find program and args in exec line: {:?}", value)));
            }
        }

        Ok(chunk)
    }

    /// Given a program and an implementation pick a backend
    ///
    /// The program is expected to be in the form `${EXEC}` or `${EXEC:backend}`
    /// TODO: should be in Utils::parser ?
    fn pick_backend(program: &str, implementation: &str) -> Option<String> {
        if program == "${EXEC}" {
            Some(implementation.to_owned())
        } else {
            if let Some(captures) = EXEC_BACKEND_RE.captures(program) {
                if let Some(backend) = captures.name("backend") {
                    Some(backend.as_str().to_owned())
                } else {
                    None
                }
            } else {
                None
            }
        }
    }
}

// TODO: tests!
