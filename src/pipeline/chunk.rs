/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::vec::Vec;

use common::CommandTask;
use pipeline::filter::PipelineFilter;
use pipeline::chunkref::PipelineChunkRef;

/// The is a command with it's pre-processors and post-filters combined in a
/// chunk that are then stored in the `Pipeline`
pub struct PipelineChunk {
    preprocessors: BTreeMap<String, CommandTask>,  // variables
    command: CommandTask,
    postfilters: Vec<Box<PipelineFilter>>,
    reference: PipelineChunkRef,
}

impl PipelineChunk {
    /// Add the given filter name to the post-filters
    pub fn add_post_filter(&mut self, key: &str, filter: Box<PipelineFilter>) {
        trace!("Adding filter {:?} to chunk {}", key, self.reference());
        self.postfilters.push(filter)
    }

    /// Add the given command for the key to the pre-processors
    ///
    /// Pre-processors are commands that are run before the command within the
    /// chunk and are stored to the given key for use as an argument in the
    /// command.
    ///
    /// For example, when using version control systems you might have a command
    /// that needs to use the branch name - so you need to run a sub-command
    /// to find out the name of the current branch.
    ///
    /// For git you could have the key `BRANCH_NAME` and the command
    /// `git rev-parse --abbrev-ref HEAD`. This will then be executed before
    /// the command in this chunk and available to use in the command as
    ///  `${BRANCH_NAME}`
    pub fn add_pre_processor(&mut self, key: &str, command: CommandTask) {
        trace!("Adding pre-processor {:?} to chunk {}", key, self.reference());
        self.preprocessors.insert(key.to_owned(), command);
    }

    /// Retrieve the [`CommandTask`] of the `PipelineChunk`
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    pub fn command(&self) -> &CommandTask {
        &self.command
    }

    /// Retrieve a mutable reference to the [`CommandTask`[ in the chunk
    ///
    /// This is so that new details can be added to the [`CommandTask`]
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    pub fn command_mut(&mut self) -> &mut CommandTask {
        &mut self.command
    }

    /// Creates a new [`PipelineChunk`] from a given `CommandTask`
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    pub fn new(command: CommandTask, reference: PipelineChunkRef) -> Self {
        Self {
            preprocessors: BTreeMap::new(),
            command: command,
            postfilters: vec!(),
            reference: reference,
        }
    }

    /// Retrieve a map of pre-processors for this `PipelineChunk`
    // TODO: pre_processor vs variables
    pub fn pre_processors(&self) -> &BTreeMap<String, CommandTask> {
        &self.preprocessors
    }

    /// Mutable reference to the map of pre-processors
    ///
    /// This is useful for the `RequestEnvBuilder` as it changes the environment
    /// of pre-processors
    pub fn pre_processors_mut(&mut self) -> &mut BTreeMap<String, CommandTask> {
        &mut self.preprocessors
    }

    /// Retrieve a list of post-filters for this `PipelineChunk`
    // TODO: post_filters vs filters
    pub fn post_filters(&self) -> &Vec<Box<PipelineFilter>> {
        &self.postfilters
    }

    /// Retrieve the reference for this `PipelineChunk`
    ///
    /// This allows a later [`PipelineBuilder`] to determine where a chunk was
    /// generated from. Which is useful for example for the Filter builder to
    /// know which post filters to add to a chunk.
    ///
    /// Some examples of information that maybe stored in the reference
    ///
    ///  * `root` - the type that created `PipelineChunk` (eg command or hook)
    ///  * `backend`
    ///  * `workflow`
    ///  * `implementation`
    ///  * `command`
    ///  * `command_set` - which command set (eg 0, 1, 2...)
    ///  * `command_set_n` - which exec line in the set (eg 0, 1, 2...)
    ///
    /// [`PipelineBuilder`]: builders/trait.PipelineBuilder.html
    pub fn reference(&self) -> &PipelineChunkRef {
        &self.reference
    }

    /// Retrieve a mutable reference for this [`PipelineChunk`]
    ///
    /// So that new details can be added to the reference.
    pub fn reference_mut(&mut self) -> &mut PipelineChunkRef {
        &mut self.reference
    }
}

#[cfg(test)]
pub mod helper {
    use super::*;

    use std::path::PathBuf;

    use common::helper::make_command_task;
    use pipeline::helper::TestPipelineFilter;

    /// Helper for making a simple [`PipelineChunk`]
    ///
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    pub fn make_pipeline_chunk() -> PipelineChunk {
        let command = make_command_task();
        let mut reference = PipelineChunkRef::new();
        reference.insert("a", "b");

        PipelineChunk::new(command, reference)
    }

    /// Helper for making a [`PipelineChunk`] from a given [`CommandTask`]
    ///
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    pub fn make_pipeline_chunk_from_task(task: CommandTask) -> PipelineChunk {
        let mut reference = PipelineChunkRef::new();
        reference.insert("c", "d");

        PipelineChunk::new(task, reference)
    }

    // Helper for making a [`PipelineChunk`] with test data
    ///
    /// [`PipelineChunk`]: struct.PipelineChunk.html
    pub fn make_pipeline_chunk_with_data() -> PipelineChunk {
        let mut chunk = make_pipeline_chunk();

        let mut variable_a = make_command_task();
        variable_a.program_replace(&PathBuf::from("/test/variable-a"));
        chunk.add_pre_processor("variable-a", variable_a);

        let mut variable_b = make_command_task();
        variable_b.program_replace(&PathBuf::from("/test/variable-b"));
        chunk.add_pre_processor("variable-b", variable_b);

        chunk.add_post_filter("filter-a", TestPipelineFilter::new());
        chunk.add_post_filter("filter-b", TestPipelineFilter::new());

        chunk
    }
}

#[cfg(test)]
mod tests {
    use std::path::PathBuf;

    use common::helper::make_command_task;
    use pipeline::helper::{make_pipeline_chunk, TestPipelineFilter};

    #[test]
    fn test_add_post_filters() {
        let mut chunk = make_pipeline_chunk();
        assert!(chunk.post_filters().is_empty());

        chunk.add_post_filter("filter-a", TestPipelineFilter::new());
        chunk.add_post_filter("filter-b", TestPipelineFilter::new());

        assert_eq!(2, chunk.post_filters().len());
    }

    #[test]
    fn test_add_pre_processor() {
        let mut chunk = make_pipeline_chunk();
        assert!(chunk.pre_processors().is_empty());

        let mut variable_a = make_command_task();
        variable_a.program_replace(&PathBuf::from("/test/variable-a"));
        chunk.add_pre_processor("variable-a", variable_a);

        let mut variable_b = make_command_task();
        variable_b.program_replace(&PathBuf::from("/test/variable-b"));
        chunk.add_pre_processor("variable-b", variable_b);

        assert!(chunk.pre_processors().contains_key("variable-a"));
        assert_eq!("/test/variable-a", chunk.pre_processors().get("variable-a").unwrap().program().to_str().unwrap());

        assert!(chunk.pre_processors().contains_key("variable-b"));
        assert_eq!("/test/variable-b", chunk.pre_processors().get("variable-b").unwrap().program().to_str().unwrap());
    }

    #[test]
    fn test_command_mut() {
        let mut chunk = make_pipeline_chunk();
        assert_eq!("/test/program", chunk.command().program().to_str().unwrap());

        chunk.command_mut().program_replace(&PathBuf::from("/test/a"));

        assert_eq!("/test/a", chunk.command().program().to_str().unwrap());
    }

    #[test]
    fn test_new() {
        let chunk = make_pipeline_chunk();

        assert_eq!("/test/program", chunk.command().program().to_str().unwrap());
        assert!(chunk.pre_processors().is_empty());
        assert!(chunk.post_filters().is_empty());
        assert!(chunk.reference().get("a").is_some());
    }

    #[test]
    fn test_pre_processors_mut() {
        let mut chunk = make_pipeline_chunk();
        assert!(chunk.pre_processors().is_empty());

        let mut variable_a = make_command_task();
        variable_a.program_replace(&PathBuf::from("/test/variable-a"));
        chunk.add_pre_processor("variable-a", variable_a);

        assert!(chunk.pre_processors().contains_key("variable-a"));
        assert_eq!("/test/variable-a", chunk.pre_processors().get("variable-a").unwrap().program().to_str().unwrap());

        chunk.pre_processors_mut().get_mut("variable-a").unwrap().program_replace(&PathBuf::from("/test/a"));

        assert_eq!("/test/a", chunk.pre_processors().get("variable-a").unwrap().program().to_str().unwrap());
    }

    #[test]
    fn test_reference_mut() {
        let mut chunk = make_pipeline_chunk();
        assert!(chunk.reference().get("c").is_none());

        chunk.reference_mut().insert("c", "d");

        assert!(chunk.reference().get("c").is_some());
    }
}
