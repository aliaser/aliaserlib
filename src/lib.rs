/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */

//! This is the documentation for the `aliaserlib` crate.
//!
//! `aliaserlib` is a library for abstracting common command-line interfaces
//! into a single common interface.
//!
//! This library is made up of three main parts
//!
//!  * [`Config`] which reads files to create a representation of the
//!    configuration
//!  * [`Pipeline`] which uses [`PipelineBuilder`] to create a chain of commands
//!    from a given [`Request`]
//!  * [`Runner`] uses a [`Pipeline`] and [`ExecuterType`] to run each of the
//!    chain of commands on the host. Then filtering any output and providing
//!   [`Packet`] as a non-blocking output.
//!
//! As an example the use of this library may look like the following.
//!
//! ```text
//! let config = Config::auto()?;
//! let request = Request::from(&config, directory, parts)?;
//!
//! let mut pipeline = Pipeline::from(&config, &request);
//! pipeline.build(&Pipeline::default_builders())?;
//!
//! let mut runner = Runner::new(&pipeline, Runner::default_executer());
//!
//! let mut output = OutputBlocking::new(&mut runner);
//! for packet in output {
//!   if let Some(data) = packet.stdout {
//!     println!("{}", data);
//!   }
//! }
//! ```
//!
//! [`Config`]: config/struct.Config.html
//! [`ExecuterType`]: runner/executer/trait.ExecuterType.html
//! [`Packet`]: common/struct.Packet.html
//! [`Pipeline`]: pipeline/struct.Pipeline.html
//! [`PipelineBuilder`]: pipeline/trait.PipelineBuilder.html
//! [`Request`]: pipeline/struct.Request.html
//! [`Runner`]: runner/struct.Runner.html

// TODO: example as actual code

#[macro_use] extern crate lazy_static;

// When native-logging feature is enabled we use the macros from the log crate
#[cfg(feature = "native-logging")]
#[macro_use] extern crate log;

extern crate regex;
extern crate serde;
#[macro_use] extern crate serde_derive;
extern crate serde_yaml;

// Logging is first as other modules may require logging macros
// When native-logging feature is enable we do not use our custom logging
#[cfg(not(feature = "native-logging"))]
#[macro_use] pub mod logging;

// Error is second as other modules may require error structs
#[macro_use] pub mod error;

// FIXME: define pub modules correctly as part of API
pub mod config;
pub mod common;
pub mod info;
pub mod pipeline;
pub mod runner;
mod utils;

pub use config::{Config, ConfigBuilder};
pub use error::LibError;
pub use pipeline::{Pipeline, Request};
pub use runner::{Runner, OutputBlocking};
