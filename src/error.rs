/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::error::Error;
use std::fmt;
use std::io;
use std::string::String;

use serde_yaml;

#[derive(Debug)]
pub enum ConfigError {
    InvalidFormatVersion(String),
    InvalidName(String),
    InvalidPath(String),
    Unknown(String),
}

impl fmt::Display for ConfigError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            ConfigError::InvalidFormatVersion(ref msg) => fmt::write(f, format_args!("Invalid format version: {}", msg)),
            ConfigError::InvalidName(ref msg) => fmt::write(f, format_args!("Invalid name: {}", msg)),
            ConfigError::InvalidPath(ref path) => fmt::write(f, format_args!("Invalid path: {}", path)),
            ConfigError::Unknown(ref msg) => fmt::write(f, format_args!("Unknown error: {}", msg)),
        }
    }
}

impl Error for ConfigError {
    fn description(&self) -> &str {
        match *self {
            ConfigError::InvalidFormatVersion(_) => "Invalid format version",
            ConfigError::InvalidName(_) => "Invalid name",
            ConfigError::InvalidPath(_) => "Invalid path",
            ConfigError::Unknown(_) => "Unknown error",
        }
    }
}

#[macro_export]
macro_rules! config_error {
    ($error:expr, $string:expr) => (
        $crate::error::LibError::Config($error($string.to_owned()))
    );
    ($string:expr) => (
        $crate::error::LibError::Config($crate::error::ConfigError::Unknown($string.to_owned()))
    );
}


#[derive(Debug)]
pub enum LibError {
    Config(ConfigError),
    IoError(io::Error),
    Request(String),
    SerdeYamlError(serde_yaml::Error),
    Unknown(String),
}

impl fmt::Display for LibError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match *self {
            LibError::Config(ref err) => fmt::write(f, format_args!("ConfigError: {}", err)),
            LibError::IoError(ref err) => fmt::write(f, format_args!("IoError: {}", err)),
            LibError::Request(ref msg) => fmt::write(f, format_args!("Request error occurred: {}", msg)),
            LibError::SerdeYamlError(ref err) => fmt::write(f, format_args!("SerdeYamlError: {}", err)),
            LibError::Unknown(ref msg) => fmt::write(f, format_args!("Unknown error occurred: {}", msg)),
        }
    }
}

impl Error for LibError {
    fn cause(&self) -> Option<&Error> {
        match *self {
            LibError::Config(ref inner) => Some(inner as &Error),
            LibError::IoError(ref inner) => Some(inner as &Error),
            LibError::Request(_) => None,
            LibError::SerdeYamlError(ref inner) => Some(inner as &Error),
            LibError::Unknown(_) => None,
        }
    }

    fn description(&self) -> &str {
        match *self {
            LibError::Config(_) => "Config error occurred",
            LibError::IoError(_) => "I/O error occurred",
            LibError::Request(_) => "Request error occurred",
            LibError::SerdeYamlError(_) => "SerdeYaml error occurred",
            LibError::Unknown(_) => "Unknown error occurred",
        }
    }
}

impl From<io::Error> for LibError {
    fn from(err: io::Error) -> Self {
        LibError::IoError(err)
    }
}

impl From<serde_yaml::Error> for LibError {
    fn from(err: serde_yaml::Error) -> Self {
        LibError::SerdeYamlError(err)
    }
}

#[macro_export]
macro_rules! lib_error {
    ($string:expr) => (
        $crate::error::LibError::Unknown($string.to_owned())
    );
    ($error:expr, $string:expr) => (
        $error($string.to_owned())
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_config_description() {
        {
            let config = ConfigError::InvalidFormatVersion("msg".to_owned());
            assert_eq!("Invalid format version", config.description());
        }

        {
            let config = ConfigError::InvalidName("msg".to_owned());
            assert_eq!("Invalid name", config.description());
        }

        {
            let config = ConfigError::InvalidPath("msg".to_owned());
            assert_eq!("Invalid path", config.description());
        }

        {
            let config = ConfigError::Unknown("msg".to_owned());
            assert_eq!("Unknown error", config.description());
        }
    }

    #[test]
    fn test_config_display() {
        {
            let config = ConfigError::InvalidFormatVersion("msg".to_owned());
            assert_eq!("Invalid format version: msg", &format!("{}", config));
        }

        {
            let config = ConfigError::InvalidName("msg".to_owned());
            assert_eq!("Invalid name: msg", &format!("{}", config));
        }

        {
            let config = ConfigError::InvalidPath("msg".to_owned());
            assert_eq!("Invalid path: msg", &format!("{}", config));
        }

        {
            let config = ConfigError::Unknown("msg".to_owned());
            assert_eq!("Unknown error: msg", &format!("{}", config));
        }
    }

    #[test]
    fn test_from_io_error() {
        let err: LibError = io::Error::new(io::ErrorKind::Other, "msg").into();

        assert!(match err {
            LibError::IoError(_) => true,
            _ => false,
        });
    }

    #[test]
    fn test_from_serde_yaml_error() {
        let serde: Result<bool, serde_yaml::Error> = serde_yaml::from_str("");
        assert!(serde.is_err());
        let err: LibError = serde.err().unwrap().into();

        assert!(match err {
            LibError::SerdeYamlError(_) => true,
            _ => false,
        });
    }

    #[test]
    fn test_lib_cause() {
        assert!(LibError::Config(ConfigError::Unknown("msg".to_owned())).cause().is_some());
        assert!(LibError::IoError(io::Error::new(io::ErrorKind::Other, "msg")).cause().is_some());
        assert!(LibError::Request("msg".to_owned()).cause().is_none());

        {
            let serde: Result<bool, serde_yaml::Error> = serde_yaml::from_str("");
            assert!(serde.is_err());
            let err: LibError = serde.err().unwrap().into();
            assert!(err.cause().is_some());
        }

        assert!(LibError::Unknown("msg".to_owned()).cause().is_none());
    }

    #[test]
    fn test_lib_description() {
        {
            let lib = LibError::Config(ConfigError::Unknown("msg".to_owned()));
            assert_eq!("Config error occurred", lib.description());
        }

        {
            let lib = LibError::IoError(io::Error::new(io::ErrorKind::Other, "msg"));
            assert_eq!("I/O error occurred", lib.description());
        }

        {
            let lib = LibError::Request("msg".to_owned());
            assert_eq!("Request error occurred", lib.description());
        }

        {
            let serde: Result<bool, serde_yaml::Error> = serde_yaml::from_str("");
            assert!(serde.is_err());
            let err: LibError = serde.err().unwrap().into();
            assert_eq!("SerdeYaml error occurred", err.description());
        }

        {
            let lib = LibError::Unknown("msg".to_owned());
            assert_eq!("Unknown error occurred", lib.description());
        }
    }

    #[test]
    fn test_lib_display() {
        {
            let lib = LibError::Config(ConfigError::Unknown("msg".to_owned()));
            assert_eq!("ConfigError: Unknown error: msg", &format!("{}", lib));
        }

        {
            let lib = LibError::IoError(io::Error::new(io::ErrorKind::Other, "msg"));
            assert_eq!("IoError: msg", &format!("{}", lib));
        }

        {
            let lib = LibError::Request("msg".to_owned());
            assert_eq!("Request error occurred: msg", &format!("{}", lib));
        }

        {
            let serde: Result<bool, serde_yaml::Error> = serde_yaml::from_str("");
            assert!(serde.is_err());
            let err: LibError = serde.err().unwrap().into();
            assert_eq!("SerdeYamlError: EOF while parsing a value", &format!("{}", err));
        }

        {
            let lib = LibError::Unknown("msg".to_owned());
            assert_eq!("Unknown error occurred: msg", &format!("{}", lib));
        }
    }

    #[test]
    fn test_macro_config_error() {
        // Test unknown state
        assert!(match config_error!("Impl but no workflow") {
            LibError::Config(ConfigError::Unknown(_)) => true,
            _ => false,
        });

        // Test with inner type
        assert!(match config_error!(ConfigError::InvalidName, "Name cannot be 'all'") {
            LibError::Config(ConfigError::InvalidName(_)) => true,
            _ => false,
        });
    }

    #[test]
    fn test_macro_lib_error() {
        // Test unknown state
        assert!(match lib_error!("Library error!") {
            LibError::Unknown(_) => true,
            _ => false,
        });

        // Test with inner type
        assert!(match lib_error!(LibError::Request, "Request error!") {
            LibError::Request(_) => true,
            _ => false,
        });
    }
}
