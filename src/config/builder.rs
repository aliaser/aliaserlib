/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::ffi::OsStr;
use std::fs::File;
use std::path::{Path, PathBuf};
use std::io::{BufReader, Read};
use std::vec::Vec;

use serde_yaml;

use config::Config;
use error::{ConfigError, LibError};

enum ConfigFormat {
    YAML,
}

/// A [`ConfigBuilder`] is used to create a [`Config`] from given paths.
///
/// [`Config`]: struct.Config.html
/// [`ConfigBuilder`]: struct.ConfigBuilder.html
pub struct ConfigBuilder {
    // don't use Set here as order is important
    // otherwise when configs are merged, override order is lost
    paths: Vec<PathBuf>,
}

impl ConfigBuilder {
    /// Takes a single path, could be a directory of files or a single file
    ///
    /// Note that the path must be absolute otherwise it will be rejected.
    /// Note that paths that are added later may override earlier ones.
    /// Note that a path of `*.yaml` will only load files with the `yaml`
    /// extension in the given folder and subfolders
    pub fn add_path(&mut self, path: &Path) -> Result<&mut Self, LibError> {
        // Check that the path we have been given is absolute
        if !path.is_absolute() {
            return Err(config_error!(ConfigError::InvalidPath, format!("Path is not absolute: {:?}", path)));
        }

        // Detect if we are being asked to filter for a specific extension
        if let Some("*") = path.file_stem().map(|s| s.to_str().unwrap_or_default()) {
            if let Some(ext) = path.extension() {
                if let Some(parent) = path.parent() {
                    if !parent.exists() {
                        Err(config_error!(ConfigError::InvalidPath, format!("Could not find path: {:?} in given extension filtered path: {:?}", parent, path)))
                    } else if !parent.is_dir() {
                        Err(config_error!(ConfigError::InvalidPath, format!("Path is not a folder: {:?} in given extension filtered path: {:?}", parent, path)))
                    } else {
                        self.add_directory(&parent, ext.to_str())
                    }
                } else {
                    Err(config_error!(ConfigError::InvalidPath, format!("No parent was found to search for files by extension in the path: {:?}", path)))
                }
            } else {
                Err(config_error!(ConfigError::InvalidPath, format!("* was used in file name to filter by extension, but no extension was given: {:?}", path)))
            }
        } else {
            // Check path exists
            if path.exists() {
                if path.is_file() {
                    self.add_file(&path)
                } else if path.is_dir() {
                    self.add_directory(&path, None)
                } else {
                    Err(config_error!(ConfigError::InvalidPath, format!("Unknown type of path, not directory or file: {:?}", path)))
                }
            } else {
                Err(config_error!(ConfigError::InvalidPath, format!("Could not find path: {:?}", path)))
            }
        }
    }

    /// Create a new [`ConfigBuilder`]
    ///
    /// [`ConfigBuilder`]: struct.ConfigBuilder.html
    pub fn new() -> Self {
        Self {
            paths: Vec::new(),
        }
    }

    /// Convert into the resulting [`Config`]
    ///
    /// [`Config`]: struct.Config.html
    pub fn to_config(&self) -> Result<Config, LibError> {
        let mut config = Config::default();

        // Load paths into the config
        for path in &self.paths {
            self.load_file(&mut config, &path)?;
        }

        // Validate the resultant config
        config.validate()?;

        Ok(config)
    }
}

impl ConfigBuilder {
    /// Add a file to the internal cache
    fn add_file(&mut self, path: &Path) -> Result<&mut Self, LibError> {
        let path_buf = path.to_path_buf();

        // Insert this path into cache, if it already exists then skip and warn
        if !self.paths.contains(&path_buf) {
            // Canonicalize to ensure that sym links are resolved
            // this helps prevent duplicates
            self.paths.push(path_buf.canonicalize()?);
        } else {
            warn!("Attempted to load config path more than once: {:?}", path);
        }

        Ok(self)
    }

    /// Loads a directory of files into the internal cache using add_path
    /// This can be filtered by a given extension
    fn add_directory(&mut self, path: &Path, extension: Option<&str>) -> Result<&mut Self, LibError> {
        let mut dirs: Vec<PathBuf> = vec!();
        let mut files: Vec<PathBuf> = vec!();

        // Add path in the directory
        for entry in path.read_dir()? {
            // TODO: should we error if entry is error?
            if let Ok(entry) = entry {
                if entry.path().is_file() {
                    files.push(entry.path());
                } else if entry.path().is_dir() {
                    dirs.push(entry.path());
                } else {
                    warn!("Entry in folder is not path or directory: {:?}", entry.path());
                }
            }
        }

        // Ensure dirs and files are sorted
        dirs.sort();
        files.sort();

        // Load the files
        for file in files {
            // If we are in extensions mode then check the path is valid
            // otherwise add it anyway
            if extension.is_none() || extension.unwrap_or_default() == file.extension().and_then(OsStr::to_str).unwrap_or_default() {
                self.add_file(&file)?;
            }
        }

        // Load the folders
        for dir in dirs {
            // Don't recurse into dot directories as this was probably not
            // intended by the user
            // Eg If the config repo was under Git, we would try to load .git
            // TODO: as there is extension filtering is this required now?
            // if dir.file_name().and_then(OsStr::to_str).unwrap_or_default().starts_with(".") {
            //    warn!("Not recusing into dot directory: {:?}", dir);
            //    continue;
            // }

            // TODO: do we need to protect against loops ?
            self.add_directory(&dir, extension)?;
        }

        Ok(self)
    }

    /// Loads a file using load_string
    fn load_file(&self, config: &mut Config, path: &Path) -> Result<&Self, LibError> {
        info!("Going to load config path: {:?}", path);

        // Read file into string
        let file = File::open(path)?;
        let mut buf_reader = BufReader::new(file);
        let mut contents = "".to_owned();

        buf_reader.read_to_string(&mut contents)?;

        // Use the extension of the file to match the format
        match path.extension() {
            Some(ext) if ext == "yaml" => self.load_string(config, contents, ConfigFormat::YAML),
            Some(ext) => Err(config_error!(ConfigError::InvalidPath, format!("Could not load path {:?}, unknown extension {:?}", path, ext))),
            None => Err(config_error!(ConfigError::InvalidPath, format!("Could not load path {:?}, no extension found.", path)))
        }
    }

    /// Loads a string - might need format type (enum YAML), stream or String ?
    fn load_string(&self, config: &mut Config, data: String, format: ConfigFormat) -> Result<&Self, LibError> {
        // build Config from string
        match format {
            ConfigFormat::YAML => {
                // TODO: maybe use Result<Config, serde_yaml::Error> so we can capture error?
                let loaded_config: Config = serde_yaml::from_str(&data)?;
                config.merge(loaded_config)?;

                Ok(self)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    // TODO: tests, need fixture
    // test_add_directory
    // test_add_file
    // test_add_path
    // test_load_file

    #[test]
    fn test_load_string() {
        let builder = ConfigBuilder::new();
        let mut config = Config::default();

        {
            let data = "
    format: 1
    backend:
      a:
        exec:
          - /test/path/a".to_owned();
            assert!(builder.load_string(&mut config, data, ConfigFormat::YAML).is_ok());
            assert!(config.backends().contains(&"a".to_owned()));
        }

        {
            let data = "invalid: config".to_owned();
            assert!(builder.load_string(&mut config, data, ConfigFormat::YAML).is_err());
        }
    }

    #[test]
    fn test_new() {
        let config = ConfigBuilder::new().to_config();
        assert!(config.is_ok());
    }

}

