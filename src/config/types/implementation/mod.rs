/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

mod command;

use self::command::ImplementationCommand;

pub type Implementation = BTreeMap<String, Vec<ImplementationCommand>>;

pub struct ImplementationWrapper<'c> {
    commands: &'c BTreeMap<String, Vec<ImplementationCommand>>,
}

impl<'c> ImplementationWrapper<'c> {
    pub fn command(&self, key: &str) -> Option<&'c Vec<ImplementationCommand>> {
        self.commands.get(key)
    }

    // FIXME: should be Vec<String> or references
    pub fn commands(&self) -> &'c Implementation {
        &self.commands
    }

    pub fn from(commands: &'c Implementation) -> ImplementationWrapper<'c> {
        Self {
            commands: commands
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
cmda:
  - exec:
      - testa
    filter:
      - filter-a
cmdb:
  - exec:
      - testb
    filter:
      - filter-b");
        let implementation: Implementation = serde_yaml::from_str(&serialized).unwrap();
        let implementation = ImplementationWrapper::from(&implementation);

        assert!(implementation.commands().contains_key("cmda"));
        assert_eq!(1, implementation.command("cmda").unwrap().len());
        let ref cmda = implementation.command("cmda").unwrap()[0];
        assert_eq!(1, cmda.exec().len());
        assert_eq!("testa", cmda.exec()[0]);
        assert_eq!(1, cmda.filter().len());
        assert_eq!("filter-a", cmda.filter()[0]);

        assert!(implementation.commands().contains_key("cmdb"));
        assert_eq!(1, implementation.command("cmdb").unwrap().len());
        let ref cmdb = implementation.command("cmdb").unwrap()[0];
        assert_eq!(1, cmdb.exec().len());
        assert_eq!("testb", cmdb.exec()[0]);
        assert_eq!(1, cmdb.filter().len());
        assert_eq!("filter-b", cmdb.filter()[0]);
    }

    #[test]
    fn test_deserialize_commands_invalid() {
        let serialized = "command: invalid";
        let deserialized: Result<Implementation, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
