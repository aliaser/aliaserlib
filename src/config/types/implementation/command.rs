/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::vec::Vec;

#[derive(Serialize, Deserialize)]
pub struct ImplementationCommand {
    exec: Vec<String>,

    #[serde(default)]
    filter: Vec<String>,
}

impl ImplementationCommand {
    pub fn exec(&self) -> &Vec<String> {
        &self.exec
    }

    pub fn filter(&self) -> &Vec<String> {
        &self.filter
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
exec:
  - /test/command/a
  - /test/command/b
filter:
  - filter-a
  - filter-b");
        let command: ImplementationCommand = serde_yaml::from_str(&serialized).unwrap();

        assert_eq!(2, command.exec().len());
        assert_eq!("/test/command/a", command.exec()[0]);
        assert_eq!("/test/command/b", command.exec()[1]);
        assert_eq!(2, command.filter().len());
        assert_eq!("filter-a", command.filter()[0]);
        assert_eq!("filter-b", command.filter()[1]);
    }

    #[test]
    fn test_deserialize_exec_invalid() {
        let serialized = "
exec: invalid";
        let deserialized: Result<ImplementationCommand, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_exec_missing() {
        let serialized = "
filter:
  - test";
        let deserialized: Result<ImplementationCommand, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_filter_default() {
        let serialized = String::from("
exec:
  - /test/command/a");
        let command: ImplementationCommand = serde_yaml::from_str(&serialized).unwrap();
        assert!(command.filter().is_empty());
    }

    #[test]
    fn test_deserialize_filter_invalid() {
        let serialized = "
exec:
  - /test/path
filter: invalid";
        let deserialized: Result<ImplementationCommand, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
