/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

/// The possible buffer modes for [`Filter`]
#[derive(Serialize, Deserialize, Clone)]
pub enum FilterBuffer {
    /// Buffer only one character
    #[serde(rename = "char")]
    Char,

    /// Buffer until the first `\n` or `r`
    #[serde(rename = "line")]
    Line,
}

// FIXME: do we need Clone on others? or should we have Filter::from(other: Filter) ?
#[derive(Serialize, Deserialize, Clone)]
pub struct Filter {
    /// How much data this [`Filter`] requires
    buffer: FilterBuffer,

    // TODO: match and tests

    /// Map of searcher and replacer for the [`Filter`] to run on the data
    #[serde(default)]
    replace: BTreeMap<String, String>,
}

impl Filter {
    pub fn is_char(&self) -> bool {
        match self.buffer {
            FilterBuffer::Char => true,
            _ => false,
        }
    }

    pub fn is_line(&self) -> bool {
        match self.buffer {
            FilterBuffer::Line => true,
            _ => false
        }
    }

    pub fn replace(&self, packet: Option<String>) -> Option<String> {
        match packet {
            Some(mut string) => {
                for (searcher, replacer) in &self.replace {
                    string = string.replace(searcher, replacer);
                }

                Some(string)
            }
            None => None
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
buffer: char
replace:
  a: b");

        let deserialized: Filter = serde_yaml::from_str(&serialized).unwrap();

        assert!(deserialized.is_char());
        assert!(deserialized.replace.contains_key("a"));
        assert_eq!("b", deserialized.replace.get("a").unwrap());
    }

    #[test]
    fn test_deserialize_buffer_char() {
        let serialized = "
buffer: char";
        let deserialized: Filter = serde_yaml::from_str(serialized).unwrap();

        assert!(deserialized.is_char());
    }


    #[test]
    fn test_deserialize_buffer_line() {
        let serialized = "
buffer: line";
        let deserialized: Filter = serde_yaml::from_str(serialized).unwrap();

        assert!(deserialized.is_line());
    }

    #[test]
    fn test_deserialize_buffer_invalid() {
        let serialized = "
buffer: invalid";
        let deserialized: Result<Filter, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_buffer_missing() {
        let serialized = "
replace:
  A: B";
        let deserialized: Result<Filter, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_replace_invalid() {
        let serialized = "
buffer: char
replace: invalid";
        let deserialized: Result<Filter, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_replace() {
        let serialized = "
buffer: char
replace:
  a: 1
  d: 4";
        let filter: Filter = serde_yaml::from_str(serialized).unwrap();

        let input = "abcd".to_owned();
        let output = filter.replace(Some(input));

        assert!(output.is_some());
        assert_eq!(output.unwrap(), "1bc4");
    }

    #[test]
    fn test_replace_none() {
        let serialized = "
buffer: char";
        let filter: Filter = serde_yaml::from_str(serialized).unwrap();

        assert!(filter.replace(None).is_none());
    }
}
