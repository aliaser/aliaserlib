/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::path::{Path, PathBuf};
use std::vec::Vec;

mod probe;

use self::probe::BackendProbe;

#[derive(Serialize, Deserialize)]
pub struct Backend {
    #[serde(default)]
    env: BTreeMap<String, String>,

    exec: Vec<PathBuf>,

    #[serde(default)]
    probe: BackendProbe,

    #[serde(default)]
    variables: BTreeMap<String, String>,
}

impl Backend {
    pub fn env(&self) -> &BTreeMap<String, String> {
        &self.env
    }

    // Returns any executable that is found
    pub fn executable(&self) -> Option<&Path> {
        for path in &self.exec {
            // TODO: this doesn't search the PATH for eg bash
            // Maybe this conversion needs to be done later
            // or in a separate method like
            // executable(&self, path: Vec<Path>)
            if path.exists() {
                return Some(path.as_path());
            }
        }

        // Fallback to returning the first if none are found
        self.exec.first().map(|path| path.as_path())
    }

    pub fn probe(&self) -> &BackendProbe {
        &self.probe
    }

    pub fn variables(&self) -> &BTreeMap<String, String> {
        &self.variables
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
env:
  A: B
exec:
  - /test/path/a
probe:
  exists:
    - /test/path/exists/a
    - /test/path/exists/b
variables:
  C: D");
        let backend: Backend = serde_yaml::from_str(&serialized).unwrap();

        assert!(backend.env().contains_key("A"));
        assert_eq!("B", backend.env().get("A").unwrap());
        assert!(backend.executable().is_some());
        assert_eq!("/test/path/a", backend.executable().unwrap().to_str().unwrap());
        assert_eq!(2, backend.probe().exists().len());
        assert_eq!("/test/path/exists/a", backend.probe().exists()[0]);
        assert_eq!("/test/path/exists/b", backend.probe().exists()[1]);
        assert!(backend.variables().contains_key("C"));
        assert_eq!("D", backend.variables().get("C").unwrap());
    }

    #[test]
    fn test_deserialize_env_invalid() {
        let serialized = "
env: invalid
exec:
  - /test/path";
        let deserialized: Result<Backend, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_exec_invalid() {
        let serialized = "
exec: invalid";
        let deserialized: Result<Backend, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_exec_missing() {
        let serialized = "
env:
  a: b";
        let deserialized: Result<Backend, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_probe_invalid() {
        let serialized = "
exec:
  - /test/path
probe: invalid";
        let deserialized: Result<Backend, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_variables_invalid() {
        let serialized = "
exec:
  - /test/path
variables: invalid";
        let deserialized: Result<Backend, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    // TODO: test_exec_exists  // need to figure out fixture environments still

    #[test]
    fn test_exec_missing_fallback_first() {
        let serialized = "
exec:
  - /test/path/a
  - /test/path/b";
        let backend: Backend = serde_yaml::from_str(&serialized).unwrap();

        assert!(backend.executable().is_some());
        assert_eq!("/test/path/a", backend.executable().unwrap().to_str().unwrap());
    }
}
