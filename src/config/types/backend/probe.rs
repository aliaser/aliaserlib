/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::vec::Vec;

#[derive(Serialize, Deserialize, Default)]
pub struct BackendProbe {
    exists: Vec<String>,  // TODO: should be PathBuf ?
}

impl BackendProbe {
    pub fn exists(&self) -> &Vec<String> {
        &self.exists
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_default() {
        let probe = BackendProbe::default();
        assert_eq!(0, probe.exists().len());
    }

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
exists:
  - /path/a
  - /path/b");
        let deserialized: BackendProbe = serde_yaml::from_str(&serialized).unwrap();

        assert_eq!(2, deserialized.exists().len());
        assert_eq!("/path/a", deserialized.exists()[0]);
        assert_eq!("/path/b", deserialized.exists()[1]);
    }

    #[test]
    fn test_deserialize_invalid() {
        let serialized = "exists: test";
        let deserialized: Result<BackendProbe, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
