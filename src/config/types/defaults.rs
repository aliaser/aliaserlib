/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

#[derive(Serialize, Deserialize, Default)]
pub struct Defaults {
    #[serde(default)]
    env: BTreeMap<String, String>,

    #[serde(default)]
    executer: Option<String>,  // TODO: should we limit to cmd, mock ?

    #[serde(default)]
    workflow: BTreeMap<String, String>,
}

impl Defaults {
    /// Moves the elements from `other` into `Self`, leaving `other` empty
    pub fn append(&mut self, other: &mut Self) {
        self.env.append(&mut other.env);

        // Replace other.executer with None and move to self if there is a value
        // but if there is None in other do no overwrite as some may exist
        if let Some(exec) = other.executer.take() {
            self.executer = Some(exec);
        }

        self.workflow.append(&mut other.workflow);
    }

    pub fn env(&self) -> &BTreeMap<String, String> {
        &self.env
    }

    pub fn executer(&self) -> Option<&String> {
        self.executer.as_ref()
    }

    pub fn workflow(&self, key: &str) -> Option<&String> {
        self.workflow.get(key)
    }

    pub fn workflows(&self) -> Vec<String> {
        // FIXME: might need Utils::use_fallback ? to turn auto/git into git ?
        // but this should be interpreted higher?
        self.workflow.keys().cloned().collect()
    }
}

#[cfg(test)]
impl Defaults {
    pub fn mock_env(&mut self, key: &str, value: &str) {
        self.env.insert(key.to_owned(), value.to_owned());
    }

    pub fn mock_executer(&mut self, executer: &str) {
        self.executer = Some(executer.to_owned());
    }

    pub fn mock_workflow(&mut self, workflow: &str, implementation: &str) {
        self.workflow.insert(workflow.to_owned(), implementation.to_owned());
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_append_exec_none() {
        // Test when exec is None that existing stays

        let mut existing: Defaults = serde_yaml::from_str("executer: mock").unwrap();
        assert!(existing.executer().is_some());

        let mut overwrite: Defaults = serde_yaml::from_str("fake: value").unwrap();
        assert!(overwrite.executer().is_none());

        existing.append(&mut overwrite);
        assert!(existing.executer().is_some());
    }

    #[test]
    fn test_append_existing_overwrite() {
        // Test that existing entries are overwritten

        let serialized_existing = String::from("
env:
  A: B
workflow:
  a: b");
        let mut existing: Defaults = serde_yaml::from_str(&serialized_existing).unwrap();
        assert!(existing.env().contains_key("A"));
        assert_eq!("B", existing.env().get("A").unwrap());
        assert!(existing.workflows().contains(&"a".to_owned()));
        assert_eq!("b", existing.workflow("a").unwrap());


        let serialized_overwrite = String::from("
env:
  A: C
workflow:
  a: c");
        let mut overwrite: Defaults = serde_yaml::from_str(&serialized_overwrite).unwrap();
        assert!(overwrite.env().contains_key("A"));
        assert_eq!("C", overwrite.env().get("A").unwrap());
        assert!(overwrite.workflows().contains(&"a".to_owned()));
        assert_eq!("c", overwrite.workflow("a").unwrap());

        existing.append(&mut overwrite);
        assert!(existing.env().contains_key("A"));
        assert_eq!("C", existing.env().get("A").unwrap());
        assert!(existing.workflows().contains(&"a".to_owned()));
        assert_eq!("c", existing.workflow("a").unwrap());
    }

    #[test]
    fn test_append_insert() {
        // Test that new entries are merged alongside existing

        let serialized_existing = String::from("
env:
  A: B
workflow:
  a: b");
        let mut existing: Defaults = serde_yaml::from_str(&serialized_existing).unwrap();
        assert!(existing.env().contains_key("A"));
        assert!(existing.workflows().contains(&"a".to_owned()));


        let serialized_insert = String::from("
env:
  C: D
workflow:
  c: d");
        let mut insert: Defaults = serde_yaml::from_str(&serialized_insert).unwrap();
        assert!(insert.env().contains_key("C"));
        assert!(insert.workflows().contains(&"c".to_owned()));

        existing.append(&mut insert);
        assert!(existing.env().contains_key("A"));
        assert!(existing.workflows().contains(&"a".to_owned()));
        assert!(existing.env().contains_key("C"));
        assert!(existing.workflows().contains(&"c".to_owned()));
    }

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
env:
  A: B
executer: mock
workflow:
  wfl: a");
        let deserialized: Defaults = serde_yaml::from_str(&serialized).unwrap();

        assert!(deserialized.env().contains_key("A"));
        assert_eq!("B", deserialized.env().get("A").unwrap());
        assert!(deserialized.executer().is_some());
        assert_eq!("mock", deserialized.executer().unwrap());
        assert!(deserialized.workflows().contains(&"wfl".to_owned()));
        assert_eq!("a", deserialized.workflow("wfl").unwrap());
    }

    #[test]
    fn test_deserialize_empty() {
        let serialized = String::from("fake: value");
        let deserialized: Defaults = serde_yaml::from_str(&serialized).unwrap();

        assert!(deserialized.env().is_empty());
        assert!(deserialized.executer().is_none());
        assert!(deserialized.workflows().is_empty());
    }

    #[test]
    fn test_deserialize_env_invalid() {
        let serialized = String::from("
env:
  - value");
        let deserialized: Result<Defaults, serde_yaml::Error> = serde_yaml::from_str(&serialized);

        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_exec_invalid() {
        let serialized = String::from("
executer:
  - value");
        let deserialized: Result<Defaults, serde_yaml::Error> = serde_yaml::from_str(&serialized);

        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_exec_none() {
        let serialized = String::from("
env:
  A: B");
        let deserialized: Defaults = serde_yaml::from_str(&serialized).unwrap();

        assert!(deserialized.executer().is_none());
    }

    #[test]
    fn test_deserialize_workflow_invalid() {
        let serialized = String::from("
workflow:
  - value");
        let deserialized: Result<Defaults, serde_yaml::Error> = serde_yaml::from_str(&serialized);

        assert!(deserialized.is_err());
    }
}
