/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::default::Default;

fn default_arg_type() -> String {
    "str".to_owned()
}

fn default_optional() -> bool {
    false
}

#[derive(Serialize, Deserialize)]
pub struct WorkflowArg {
    #[serde(default="default_arg_type", rename="type")]
    arg_type: String,

    #[serde(default)]
    default: String,

    #[serde(default="default_optional")]
    optional: bool,
}

impl WorkflowArg {
    pub fn arg_type(&self) -> &str {
        &self.arg_type
    }

    pub fn default_value(&self) -> &str {
        &self.default
    }

    pub fn optional(&self) -> bool {
        self.optional
    }
}

impl Default for WorkflowArg {
    fn default() -> Self {
        Self {
            arg_type: default_arg_type(),
            default: "".to_owned(),
            optional: default_optional(),
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_default() {
        let arg = WorkflowArg::default();

        assert_eq!("str", arg.arg_type());
        assert_eq!("", arg.default_value());
        assert_eq!(false, arg.optional());
    }

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
default: value
optional: true
type: int");
        let arg: WorkflowArg = serde_yaml::from_str(&serialized).unwrap();

        assert_eq!("int", arg.arg_type());
        assert_eq!("value", arg.default_value());
        assert_eq!(true, arg.optional());
    }

    #[test]
    fn test_deserialize_arg_type_default() {
        let serialized = String::from("
default: value
optional: true");
        let arg: WorkflowArg = serde_yaml::from_str(&serialized).unwrap();
        assert_eq!("str", arg.arg_type());
    }

    #[test]
    fn test_deserialize_arg_type_invalid() {
        let serialized = "
type:
  - test";
        let deserialized: Result<WorkflowArg, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_default_default() {
        let serialized = String::from("
optional: true
type: int");
        let arg: WorkflowArg = serde_yaml::from_str(&serialized).unwrap();
        assert_eq!("", arg.default_value());
    }

    #[test]
    fn test_deserialize_default_invalid() {
        let serialized = "
default:
  - test";
        let deserialized: Result<WorkflowArg, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_optional_default() {
        let serialized = String::from("
default: value
type: int");
        let arg: WorkflowArg = serde_yaml::from_str(&serialized).unwrap();
        assert_eq!(false, arg.optional());
    }

    #[test]
    fn test_deserialize_optional_invalid() {
        let serialized = "
optional:
  - test";
        let deserialized: Result<WorkflowArg, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
