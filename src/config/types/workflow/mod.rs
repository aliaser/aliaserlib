/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

mod arg;
mod command;

pub use self::command::WorkflowCmd;

pub type Workflow = BTreeMap<String, WorkflowCmd>;

pub struct WorkflowWrapper<'c> {
    commands: &'c BTreeMap<String, WorkflowCmd>,
}

impl<'c> WorkflowWrapper<'c> {
    pub fn command(&self, key: &str) -> Option<&'c WorkflowCmd> {
        self.commands.get(key)
    }

    // FIXME: should be Vec<String> or references
    pub fn commands(&self) -> &'c Workflow {
        self.commands
    }

    pub fn from(commands: &'c Workflow) -> WorkflowWrapper<'c> {
        Self {
            commands: commands
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
cmda:
  args:
    a:
      type: int
  description: Command A
  order:
    - a
  standalone: true
cmdb:
  description: Command B");
        let workflow: Workflow = serde_yaml::from_str(&serialized).unwrap();
        let workflow = WorkflowWrapper::from(&workflow);

        assert!(workflow.commands().contains_key("cmda"));
        let cmda = workflow.command("cmda").unwrap();
        assert!(cmda.args().contains_key("a"));
        assert_eq!("int", cmda.args().get("a").unwrap().arg_type());
        assert_eq!("Command A", cmda.description());
        assert_eq!(1, cmda.order().len());
        assert_eq!("a", cmda.order()[0]);
        assert_eq!(true, cmda.standalone());

        assert!(workflow.commands().contains_key("cmdb"));
        let cmdb = workflow.command("cmdb").unwrap();
        assert_eq!("Command B", cmdb.description());
    }

    #[test]
    fn test_deserialize_commands_invalid() {
        let serialized = "command: invalid";
        let deserialized: Result<Workflow, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
