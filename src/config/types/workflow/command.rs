/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

use config::types::workflow::arg::WorkflowArg;

fn default_standalone() -> bool {
    false
}

#[derive(Serialize, Deserialize)]
pub struct WorkflowCmd {
    #[serde(default)]
    args: BTreeMap<String, WorkflowArg>,

    #[serde(default)]
    description: String,

    #[serde(default)]
    order: Vec<String>,

    #[serde(default="default_standalone")]
    standalone: bool,
}

impl WorkflowCmd {
    pub fn args(&self) -> &BTreeMap<String, WorkflowArg> {
        &self.args
    }

    pub fn description(&self) -> &str {
        &self.description
    }

    pub fn order(&self) -> &Vec<String> {
        &self.order
    }

    pub fn standalone(&self) -> bool {
        self.standalone
    }
}

#[cfg(test)]
impl WorkflowCmd {
    pub fn new() -> Self {
        Self {
            args: BTreeMap::new(),
            description: "".to_owned(),
            order: vec!(),
            standalone: default_standalone(),
        }
    }

    // TODO: are these required?
    pub fn mock_arg(&mut self, key: &str, value: WorkflowArg) {
        self.args.insert(key.to_owned(), value);
    }

    pub fn mock_description(&mut self, description: &str) {
        self.description = description.to_owned()
    }

    pub fn mock_order(&mut self, value: &str) {
        self.order.push(value.to_owned())
    }

    pub fn mock_standalone(&mut self, value: bool) {
        self.standalone = value
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use serde_yaml;

    #[test]
    fn test_deserialize() {
        let serialized = String::from("
args:
  a:
    default: a-default
    optional: true
    type: int
description: Test description
order:
  - a
standalone: true");
        let command: WorkflowCmd = serde_yaml::from_str(&serialized).unwrap();
        assert!(command.args().contains_key("a"));

        let arg = command.args().get("a").unwrap();
        assert_eq!("int", arg.arg_type());
        assert_eq!("a-default", arg.default_value());
        assert_eq!(true, arg.optional());

        assert_eq!("Test description", command.description());
        assert_eq!(1, command.order().len());
        assert_eq!("a", command.order()[0]);
        assert_eq!(true, command.standalone());
    }

    #[test]
    fn test_deserialize_args_default() {
        let serialized = String::from("
description: Test description
order:
  - a
standalone: true");
        let command: WorkflowCmd = serde_yaml::from_str(&serialized).unwrap();
        assert!(command.args().is_empty());
    }

    #[test]
    fn test_deserialize_args_invalid() {
        let serialized = "
args:
  - test";
        let deserialized: Result<WorkflowCmd, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_description_default() {
        let serialized = String::from("
args:
  a:
    default: a-default
    optional: true
    type: int
order:
  - a
standalone: true");
        let command: WorkflowCmd = serde_yaml::from_str(&serialized).unwrap();
        assert!(command.description().is_empty());
    }

    #[test]
    fn test_deserialize_description_invalid() {
        let serialized = "
description:
  - invalid";
        let deserialized: Result<WorkflowCmd, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_order_default() {
        let serialized = String::from("
args:
  a:
    default: a-default
    optional: true
    type: int
description: Test description
standalone: true");
        let command: WorkflowCmd = serde_yaml::from_str(&serialized).unwrap();
        assert!(command.order().is_empty());
    }

    #[test]
    fn test_deserialize_order_invalid() {
        let serialized = "
order: invalid";
        let deserialized: Result<WorkflowCmd, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }

    #[test]
    fn test_deserialize_standalone_default() {
        let serialized = String::from("
args:
  a:
    default: a-default
    optional: true
    type: int
order:
  - a");
        let command: WorkflowCmd = serde_yaml::from_str(&serialized).unwrap();
        assert_eq!(false, command.standalone());
    }

    #[test]
    fn test_deserialize_standalone_invalid() {
        let serialized = "
standalone:
  - invalid";
        let deserialized: Result<WorkflowCmd, serde_yaml::Error> = serde_yaml::from_str(serialized);
        assert!(deserialized.is_err());
    }
}
