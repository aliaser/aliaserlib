/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::{BTreeMap, HashSet};
use std::default::Default;
use std::error::Error;
use std::fmt::Debug;
use std::hash::Hash;

use error::{ConfigError, LibError};
use utils::check_name;

mod builder;
mod types;

pub use self::builder::ConfigBuilder;
pub use self::types::{Backend, Defaults, Filter, Implementation, ImplementationWrapper, Workflow, WorkflowWrapper};

#[derive(Serialize, Deserialize)]
pub struct Config {
    // {bash: Backend, bzr: Backend, git: Backend}
    #[serde(default)]
    backend: BTreeMap<String, Backend>,

    // {env: {ALIAS_DEBUG: true}, workflow: {vcs: bzr}}
    // TODO: should this be singular "default" ?
    #[serde(default)]
    defaults: Defaults,

    // {bzr-progress-fix: Filter}
    #[serde(default)]
    filter: BTreeMap<String, Filter>,

    /// The version of this config file
    format: usize,

    // {vcs: {bzr: Implementation, git: Implementation}}
    #[serde(default, rename="impl")]
    implementation: BTreeMap<String, BTreeMap<String, Implementation>>,

    // {vcs: Workflow}
    #[serde(default)]
    workflow: BTreeMap<String, Workflow>
}

impl Config {
    /// The [`Backend`] for the given key
    pub fn backend(&self, key: &str) -> Option<&Backend> {
        self.backend.get(key)
    }

    /// The [`Backend`] names in this [`Config`]
    pub fn backends(&self) -> Vec<String> {
        self.backend.keys().cloned().collect()
    }

    /// The [`Defaults`] for this configuration
    pub fn defaults(&self) -> &Defaults {
        &self.defaults
    }

    /// The [`Filter`] for the given key
    pub fn filter(&self, key: &str) -> Option<&Filter> {
        self.filter.get(key)
    }

    /// The [`Filter`] names in this [`Config`]
    pub fn filters(&self) -> Vec<String> {
        self.filter.keys().cloned().collect()
    }

    /// The format version of this [`Config`]
    pub fn format(&self) -> usize {
        self.format
    }

    /// The [`Implementation`] for the given workflow and key
    pub fn implementation(&self, workflow: &str, key: &str) -> Option<ImplementationWrapper> {
        if let Some(workflow) = self.implementation.get(workflow) {
            workflow.get(key).map(|commands| ImplementationWrapper::from(commands))
        } else {
            None
        }
    }

    /// List all [`Implementation`] for a given [`Workflow`]
    pub fn implementations(&self, workflow: &str) -> Option<&BTreeMap<String, Implementation>> {
        self.implementation.get(workflow)
    }

    /// Merge the given [`Config`] into this [`Config`]
    pub fn merge(&mut self, mut other: Self) -> Result<&mut Self, LibError> {
        // TODO: check that format versions match ? and is 1

        // Merge filters - duplicate fail
        Config::check_for_duplicates(self.filter.keys(), other.filter.keys())?;
        self.filter.append(&mut other.filter);

        // Merge workflows - duplicate fail
        Config::check_for_duplicates(self.workflow.keys(), other.workflow.keys())?;
        self.workflow.append(&mut other.workflow);

        // Merge backends - duplicate fail
        Config::check_for_duplicates(self.backend.keys(), other.backend.keys())?;
        self.backend.append(&mut other.backend);

        // Merge implementations - duplicate fail
        // Note this is { "vcs": { "bzr": { ... }, "git": { ... } } }
        for (workflow, mut other_implementation) in other.implementation {
            let mut this_implementation = self.implementation.entry(workflow).or_insert(BTreeMap::new());

            Config::check_for_duplicates(this_implementation.keys(), other_implementation.keys())?;
            this_implementation.append(&mut other_implementation);
        }

        // Merge defaults - duplicate overwride
        self.defaults.append(&mut other.defaults);

        Ok(self)
    }

    /// The [`Workflow`] for the given key
    pub fn workflow(&self, key: &str) -> Option<WorkflowWrapper> {
        self.workflow.get(key).map(|commands| WorkflowWrapper::from(commands))
    }

    /// The [`Workflow`] names in this [`Config`]
    pub fn workflows(&self) -> Vec<String> {
        self.workflow.keys().cloned().collect()
    }
}

impl Config {
    /// Given two Iterator's error if there are duplicate entries between them
    fn check_for_duplicates<'a, A: 'a + Debug + Eq + Hash, T: Iterator<Item = &'a A> + Sized>(a: T, b: T) -> Result<(), LibError> {
        let mut uniq = HashSet::new();

        for item in a.chain(b) {
            if !uniq.insert(item) {
                return Err(lib_error!(format!("Duplicate key was found when merging: {:?}", item)));
            }
        }

        Ok(())
    }

    fn validate(&self) -> Result<&Self, LibError> {
        // TODO: maybe split up child parts into Type::validate(&self, conf: &Config, key: &str) -> Result<Self, LibError>

        // Check format version
        if self.format != 1 {
            return Err(config_error!(ConfigError::InvalidFormatVersion, format!("Format version was expected to be 1 but {:?} was found", self.format)));
        }

        // Check filters - values don't need to be validated
        for (key, _) in &self.filter {
            if let Err(error) = check_name(key) {
                return Err(config_error!(ConfigError::InvalidName, format!("Filter name {:?} is not valid: {:?}", key, error.description())));
            }
        }

        // Check workflows
        for (key, workflow) in &self.workflow {
            if let Err(error) = check_name(key) {
                return Err(config_error!(ConfigError::InvalidName, format!("Workflow name {:?} is not valid: {:?}", key, error.description())));
            }

            // Check the command names are valid
            let workflow = WorkflowWrapper::from(workflow);

            for (key, cmd) in workflow.commands() {
                if let Err(error) = check_name(key) {
                    return Err(config_error!(ConfigError::InvalidName, format!("Command name {:?} is not valid: {:?}", key, error.description())));
                }

                // Check the argument names are valid
                for (key, _) in cmd.args() {
                    if let Err(error) = check_name(key) {
                        return Err(config_error!(ConfigError::InvalidName, format!("Argument name {:?} is not valid: {:?}", key, error.description())));
                    }
                }
            }
        }

        // Check backends
        for (key, _) in &self.backend {
            if let Err(error) = check_name(key) {
                return Err(config_error!(ConfigError::InvalidName, format!("Backend name {:?} is not valid: {:?}", key, error.description())));
            }
        }

        // Check implementations
        for (wfl_key, implementations) in &self.implementation {
            if !self.workflow.contains_key(wfl_key) {
                return Err(config_error!(format!("Implementation given for a workflow that doesn't exist: {:?}", wfl_key)));
            }

            for (key, _) in implementations {
                if let Err(error) = check_name(key) {
                    return Err(config_error!(ConfigError::InvalidName, format!("Implementation name {:?} is not valid: {:?}", key, error.description())));
                }

                // TODO: check that implementation has all workflow cmds
                // this then checks that implementation cmd names are correct

                // TODO: do we need to check other way? if we have partial implementations?

                // TODO: check that all filters have been defined
            }
        }

        // Check defaults
        // TODO: should we check exec ?

        for workflow in self.defaults.workflows() {
            if !self.workflow.contains_key(&workflow) {
                return Err(config_error!(format!("Default workflow:implementation given for a workflow that doesn't exist: {:?}", workflow)));
            }

            // TODO: check that wfl:impl is correct
            // auto - fail default always needs to be given
            // auto/git - extract git
            // git - extract git
            /*
            if self.implementation(workflow, implementation).is_none() {
                return Err(config_error!(format!("Default workflow:implementation given for an implementation that doesn't exist: {:?}", implementation)));
            }
            */
        }

        Ok(self)
    }
}

impl Default for Config {
    fn default() -> Self {
        Self {
            backend: BTreeMap::new(),
            defaults: Defaults::default(),
            filter: BTreeMap::new(),
            format: 1,
            implementation: BTreeMap::new(),
            workflow: BTreeMap::new(),
        }
    }
}

#[cfg(test)]
impl Config {
    pub fn mock_workflow(&mut self, key: String, value: Workflow) {
        self.workflow.insert(key, value);
    }

    pub fn mock_workflow_command(&mut self, workflow: &str, mut value: Workflow) {
        if let Some(mut workflow) = self.workflow.get_mut(workflow) {
            workflow.append(&mut value);
        }
    }
}

#[cfg(test)]
pub mod helper {
    use super::*;

    use serde_yaml;

    pub fn make_config_string() -> String {
        "
format: 1

backend:
  a:
    exec:
      - /test/path/a
      - /test/path/aa
    variables:
      A: ${EXEC} test
      B: ${EXEC:a} test
      C: test

defaults:
  env:
    DEFAULT_ENV_KEY_A: default_env_value_a
    DEFAULT_ENV_KEY_B: default_env_value_b
  executer: mock
  workflow:
    wfl: auto/a

workflow:
  wfl:
    cmd:
      args:
        first:
          type: str
        second:
          type: str
      description: A test command
      order:
        - first
        - second
    empty:
      description: Empty test command

filter:
  filter-a:
    buffer: char
    replace:
      a: b
  filter-b:
    buffer: char
    replace:
      c: d

impl:
  wfl:
    a:  # TODO: rename to implA ?
      cmd:
        - exec:
            - ${EXEC} first-cmd
            - ${EXEC} second-cmd
          filter:
            - filter-a
        - exec:
            - ${EXEC} third-cmd
            - ${EXEC} fourth-cmd
          filter:
            - filter-b
      empty:
        - exec:
            - ${EXEC} empty".to_owned()
    }

    pub fn make_config() -> Config {
        let config: Config = serde_yaml::from_str(&make_config_string()).unwrap();

        config
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use config::helper::make_config_string;

    use serde_yaml;

    #[test]
    fn test_check_duplicates_duplicate() {
        // Last
        {
            let a = vec!(1, 2, 3);
            let b = vec!(3, 4, 5);
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_err());
        }

        // First
        {
            let a = vec!(3, 2, 1);
            let b = vec!(5, 4, 3);
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_err());
        }

        // Middle
        {
            let a = vec!(1, 3, 2);
            let b = vec!(4, 3, 5);
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_err());
        }

        // All
        {
            let a = vec!(1, 2, 3);
            let b = vec!(1, 2, 3);
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_err());
        }
    }

    #[test]
    fn test_check_duplicates_no_duplicate() {
        // Linear chain
        {
            let a = vec!(1, 2, 3);
            let b = vec!(4, 5, 6);
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_ok());
        }

        // Empty
        {
            let a: Vec<u8> = vec!();
            let b: Vec<u8> = vec!();
            assert!(Config::check_for_duplicates(a.iter(), b.iter()).is_ok());
        }
    }

    #[test]
    fn test_default() {
        let config = Config::default();

        assert!(config.backends().is_empty());
        assert!(config.defaults().env().is_empty());
        assert!(config.filters().is_empty());
        assert_eq!(1, config.format());
        assert!(config.implementation.is_empty());  // no way to get list from outside
        assert!(config.workflows().is_empty());
    }

    #[test]
    fn test_deserialize() {
        let serialized = make_config_string();
        let config: Config = serde_yaml::from_str(&serialized).unwrap();

        // format
        assert_eq!(1, config.format());

        // backend
        assert_eq!(1, config.backends().len());
        assert!(config.backends().contains(&"a".to_owned()));

        // defaults
        assert!(config.defaults().env().contains_key("DEFAULT_ENV_KEY_A"));
        assert!(config.defaults().env().contains_key("DEFAULT_ENV_KEY_B"));
        assert!(config.defaults().executer().is_some());
        assert!(config.defaults().workflows().contains(&"wfl".to_owned()));

        // workflow
        assert_eq!(1, config.workflows().len());
        assert!(config.workflows().contains(&"wfl".to_owned()));

        // filter
        assert_eq!(2, config.filters().len());
        assert!(config.filters().contains(&"filter-a".to_owned()));
        assert!(config.filters().contains(&"filter-b".to_owned()));

        // impl
        assert!(config.implementations("wfl").is_some());
        assert!(config.implementation("wfl", "a").is_some());
    }

    #[test]
    fn test_deserialize_default() {
        let serialized = String::from("format: 1");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();

        // backend default
        assert!(config.backends().is_empty());

        // defaults default
        assert!(config.defaults().env().is_empty());
        assert!(config.defaults().executer().is_none());
        assert!(config.defaults().workflows().is_empty());

        // filter default
        assert!(config.filters().is_empty());

        // implementation default
        // note uses private struct value
        assert!(config.implementation.is_empty());

        // workflow default
        assert!(config.workflows().is_empty());
    }

    #[test]
    fn test_deserialize_backend_invalid() {
        let serialized = String::from("
format: 1
backend: invalid");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_defaults_invalid() {
        let serialized = String::from("
format: 1
defaults: invalid");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_filter_invalid() {
        let serialized = String::from("
format: 1
filter: invalid");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_format_invalid() {
        let serialized = String::from("format: test");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_format_missing() {
        let serialized = String::from("
backend:
  a:
    exec:
      - /test/path/a");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_implementation_invalid() {
        let serialized = String::from("
format: 1
impl: invalid");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_deserialize_workflow_invalid() {
        let serialized = String::from("
format: 1
workflow: invalid");
        let config: Result<Config, serde_yaml::Error> = serde_yaml::from_str(&serialized);
        assert!(config.is_err());
    }

    #[test]
    fn test_validate() {
        let serialized = make_config_string();
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_ok());
    }

    #[test]
    fn test_validate_backend_name_invalid() {
        let serialized = String::from("
format: 1
backend:
  bad!name:
    exec:
      - /test/path/a");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_defaults_workflow_missing() {
        let serialized = String::from("
format: 1
defaults:
  workflow:
    wfl: auto/a");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_filter_invalid() {
        let serialized = String::from("
format: 1
filter:
  bad!name:
    buffer: char
    replace:
      a: b");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_format_invalid() {
        let serialized = String::from("format: 2");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_implementation_name_invalid() {
        let serialized = String::from("
format: 1
impl:
  wfl:
    bad!name:
      cmd:
        - exec:
          - /test/exec/a
workflow:
  wfl:
    cmd:
      description: test");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_implementation_workflow_missing() {
        let serialized = String::from("
format: 1
impl:
  wfl:
    a:
      cmd:
        - exec:
          - /test/exec/a");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_workflow_name_invalid() {
        let serialized = String::from("
format: 1
workflow:
  bad!name:
    cmd:
      description: test");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_workflow_command_arg_name_invalid() {
        let serialized = String::from("
format: 1
workflow:
  wfl:
    cmd:
      args:
        bad!name:
          type: str
      description: test");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }

    #[test]
    fn test_validate_workflow_command_name_invalid() {
        let serialized = String::from("
format: 1
workflow:
  wfl:
    bad!name:
      description: test");
        let config: Config = serde_yaml::from_str(&serialized).unwrap();
        assert!(config.validate().is_err());
    }
}

