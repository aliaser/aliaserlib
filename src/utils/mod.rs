/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use regex::Regex;
use std::iter::FromIterator;
use std::path::PathBuf;
use std::{thread, time};
use std::vec::Vec;

use config::Config;
use error::{ConfigError, LibError};

lazy_static! {
    static ref RE: Regex = Regex::new(r"^([[:alpha:]]{1})([[:alnum:]-_])*$").unwrap();
}

pub fn check_name(name: &str) -> Result<(), LibError> {
    // Note for error messages, name is show in parent error

    // Check the size of the name
    if name.len() == 0 {
        return Err(config_error!(ConfigError::InvalidName, "Name cannot be empty"))
    }

    // Check the blacklist of names
    match name {
        "all" | "auto" | "help" => {
            return Err(config_error!(ConfigError::InvalidName, format!("Name cannot be: {:?}", name)));
        }
        _ => {}
    }

    // Check the regex pattern
    if RE.is_match(name) {
        Ok(())
    } else {
        Err(config_error!(ConfigError::InvalidName, "Regex check failed of name"))
    }
}

// Find the default impl for the given workflow - fallback to "auto" if there isn't one
fn get_default_impl(config: &Config, workflow: &str) -> String {
    match config.defaults().workflow(workflow) {
        Some(default_impl) => default_impl.to_owned(),
        None => "auto".to_owned()
    }
}

// Find out if the workflow+command is standalone - fails if workflow or command not found
pub fn is_standalone_command(config: &Config, workflow: &str, command: &str) -> Result<bool, LibError> {
    match config.workflow(workflow) {
        Some(workflow_impl) => {
            match workflow_impl.commands().get(command) {
                Some(command_impl) => Ok(command_impl.standalone()),
                None => Err(config_error!(format!("Command not found: {:?} for workflow: {:?}", command, workflow)))
            }
        }
        None => Err(config_error!(format!("Workflow not found: {:?}", workflow)))
    }
}

// needs to check implementation is auto ? if it is not auto then skip ?
pub fn probe_for_implementation(config: &Config, workflow: &str, implementation: &str) -> Option<String> {
    if implementation == "auto" || implementation.starts_with("auto/") {
        // Find the list of possible implementations
        match config.implementations(workflow) {
            Some(workflow_impl) => {
                for key in workflow_impl.keys() {
                    // For each implementation see if there is a matching backend
                    match config.backend(key) {
                        Some(backend) => {
                            debug!("Probing for implementation: {:?}", key);

                            // TODO: should this be moved into Backend::probe() ? or Backend::run_probe()
                            // TODO: is there anyway we can mock this?

                            // CHeck the exists probes
                            for path in backend.probe().exists() {
                                if PathBuf::from(path).exists() {
                                    return Some(key.to_owned())
                                }
                            }

                            // TODO: check the sum probes

                            // TODO: check the contains probes
                        }
                        None => continue
                    }
                }

                None
            }
            None => None
        }
    } else {
        None
    }
}

// If the command is standalone then any fallback is used (eg auto/git or git => git)
// If the command is not standalone then the folder is probed if auto or auto/git otherwise fallback is used
pub fn probe_or_fallback(config: &Config, workflow: &str, implementation: &str, command: &str) -> Result<String, LibError> {
    // Check if this command is standalone or not
    if is_standalone_command(config, &workflow, &command)? {
        use_fallback(&implementation)
    } else {
        // Probe for implementation - if not auto or auto/git or probe fails it will be None
        if let Some(found) = probe_for_implementation(config, &workflow, &implementation) {
            Ok(found)
        } else {
            use_fallback(&implementation)
        }
    }
}

// Split vcs:auto, vcs:auto/git vcs:git into (vcs, auto) (vcs, auto/git) (vcs, git)
// If no impl is given it asks get_default_impl what the default is
pub fn split_out_workflow(config: &Config, string: &str) -> Result<(String, String), LibError> {
    let split_string: Vec<&str> = string.split(":").collect();

    match split_string.len() {
        2 => Ok((split_string[0].to_owned(), split_string[1].to_owned())),
        1 => Ok((split_string[0].to_owned(), get_default_impl(config, split_string[0]))),
        _ => Err(lib_error!(format!("Unknown amount of split parts: {:?}", string)))
    }
}

// Takes a string of args (eg --flag --arg="test") and convert into a list of args
pub fn string_to_args(string: &str) -> Vec<String> {
    if string.is_empty() {
        Vec::new()
    } else {
        // read until next space
        let mut in_quote = false;
        let mut slash_count = 0;
        let mut buf = Vec::new();
        let mut i = 0;

        for c in string.chars() {
            i += 1;

            match c {
                '"' if slash_count % 2 == 0 => {
                    in_quote = !in_quote;
                    slash_count = 0;

                    if !string.starts_with("\"") {  // string doesn't start with quote so we leave it
                        buf.push(c);
                    }
                },
                '\\' => {
                    slash_count += 1;

                    if slash_count % 2 == 0 {  // two slashes in a row so inject one
                        buf.push(c);
                    }
                },
                ' ' if !in_quote => break,  // we have reached a space that is not inside a quote
                _ => {
                    // A trailing slash was on it's own (not attached to a " or \)
                    if c != '"' && slash_count % 2 == 1 {
                        buf.push('\\');
                    }

                    slash_count = 0;
                    buf.push(c);
                }
            }
        }

        let mut result = vec![String::from_iter(buf)];
        result.extend(string_to_args(&string[i..]));

        result
    }
}

// A simple use of CPU time to not kill the CPU usage when running a loop
pub fn tick() {
    thread::sleep(time::Duration::from_millis(10));
}

// If there is an auto mode then use the fallback if there is one
// auto - no fallback so fail
// auto/git - fallback so use git
// git - not in auto so use git
// auto/b/c - unknown situation
pub fn use_fallback(implementation: &str) -> Result<String, LibError> {
    let split_implementation: Vec<&str> = implementation.split("/").collect();

    match split_implementation.len() {
        2 => {  // could be auto/git
            if split_implementation[0] == "auto" {
                Ok(split_implementation[1].to_owned())
            } else {
                Err(lib_error!(format!("Unknown implementation, was expecting auto/impl: {:?}", implementation)))
            }
        }
        1 => {  // could be auto or git
            if split_implementation[0] == "auto" {
                Err(lib_error!(format!("No implementation specified (in auto mode with no fallback) and command is standalone, please specify an implementation.")))
            } else {
                Ok(split_implementation[0].to_owned())
            }
        }
        _ => Err(lib_error!(format!("Unknown amount of split parts for implementation: {:?}", implementation)))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::BTreeMap;
    use std::time::Instant;

    use serde_yaml;

    use config::Workflow;
    use config::helper::make_config;

    fn make_standalone_config() -> Config {
        let mut config = make_config();
        let data = "
false:
  standalone: false
true:
  standalone: true";
        let workflow: Workflow = serde_yaml::from_str(&data).unwrap();
        config.mock_workflow("test".to_owned(), workflow);

        config
    }

    #[test]
    fn test_check_name() {
        // Confirm some real names that should be valid
        assert!(check_name("vcs").is_ok());
        assert!(check_name("git").is_ok());
    }

    #[test]
    fn test_check_name_blacklist() {
        assert!(check_name("all").is_err());
        assert!(check_name("auto").is_err());
        assert!(check_name("help").is_err());
    }

    #[test]
    fn test_check_name_length() {
        // Name cannot be empty
        assert!(check_name("").is_err());
        assert!(check_name("a").is_ok());
    }

    #[test]
    fn test_check_name_regex_invalid() {
        // Test some specific invalid chars from spec
        assert!(check_name("bad:name").is_err());
        assert!(check_name("bad/name").is_err());
        assert!(check_name("1badname").is_err());
        assert!(check_name("-badname").is_err());
        assert!(check_name("_badname").is_err());

        // Check some other invalid chars
        assert!(check_name("bad!name").is_err());
        assert!(check_name("bad@name").is_err());
        assert!(check_name("bad name").is_err());
    }

    #[test]
    fn test_check_name_valid() {
        // Test the cases that should be allowed

        // Upper case letters
        assert!(check_name("NAME").is_ok());

        // Lower case
        assert!(check_name("name").is_ok());

        // Mixed case
        assert!(check_name("MiXeDcAsE").is_ok());

        // Numbers
        assert!(check_name("numbered123name").is_ok());

        // Dash
        assert!(check_name("dashed-name").is_ok());

        // Underscore
        assert!(check_name("underscore_name").is_ok());
    }

    #[test]
    fn test_default_impl() {
                let data = "
format: 1
defaults:
  workflow:
    vcs: git";
        let config: Config = serde_yaml::from_str(&data).unwrap();
        assert_eq!(get_default_impl(&config, "vcs"), "git".to_owned());
    }

    #[test]
    fn test_default_impl_none() {
        let config = make_config();
        assert_eq!(get_default_impl(&config, "invalid"), "auto".to_owned());
    }

    #[test]
    fn test_is_standalone_command() {
        let config = make_standalone_config();

        assert!(is_standalone_command(&config, "test", "false").is_ok());
        assert_eq!(is_standalone_command(&config, "test", "false").unwrap(), false);

        assert!(is_standalone_command(&config, "test", "true").is_ok());
        assert_eq!(is_standalone_command(&config, "test", "true").unwrap(), true);
    }

    #[test]
    fn test_is_standalone_command_missing_command() {
        let mut config = Config::default();
        let workflow = BTreeMap::new();

        config.mock_workflow("vcs".to_owned(), workflow);

        assert!(is_standalone_command(&config, "vcs", "cmd").is_err());
    }

    #[test]
    fn test_is_standalone_command_missing_workflow() {
        let config = Config::default();

        assert!(is_standalone_command(&config, "vcs", "cmd").is_err());
    }


    /*
    // TODO: write tests for probe_for_implementation
    #[test]
    fn test_probe_for_implementation_contains() {

    }

    #[test]
    fn test_probe_for_implementation_exists() {

    }

    #[test]
    fn test_probe_for_implementation_no_valid() {

    }
    */

    #[test]
    fn test_probe_for_implementation_not_auto() {
        let config = Config::default();
        assert!(probe_for_implementation(&config, "vcs", "git").is_none());
    }

    /*
    // TODO: write test when ready
    #[test]
    fn test_probe_for_implementation_sha512() {

    }
    */

    #[test]
    fn test_probe_or_fallback_is_standalone_auto_fallback() {
        // Standalone and has fallback so should use that
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "auto/git", "true").is_ok());
        assert_eq!(probe_or_fallback(&config, "test", "auto/git", "true").unwrap(), "git".to_owned());
    }

    #[test]
    fn test_probe_or_fallback_is_standalone_auto_no_fallback() {
        // Standalone and no fallback so should fail
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "auto", "true").is_err());
    }

    #[test]
    fn test_probe_or_fallback_is_standalone_specific() {
        // Standalone and specific impl so should use that
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "git", "true").is_ok());
        assert_eq!(probe_or_fallback(&config, "test", "git", "true").unwrap(), "git".to_owned());
    }


    #[test]
    fn test_probe_or_fallback_not_standalone_auto_fallback() {
        // Not standalone, auto and has fallback should use fallback - probe fails
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "auto/git", "false").is_ok());
        assert_eq!(probe_or_fallback(&config, "test", "auto/git", "false").unwrap(), "git".to_owned());

    }

    #[test]
    fn test_probe_or_fallback_not_standalone_auto_no_fallback() {
        // Not standalone, auto and no fallback so should fail - probe fails
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "auto", "false").is_err());
    }

    #[test]
    fn test_probe_or_fallback_not_standalone_specific() {
        // Not standalone and specific impl so should use that
        let config = make_standalone_config();

        assert!(probe_or_fallback(&config, "test", "git", "false").is_ok());
        assert_eq!(probe_or_fallback(&config, "test", "git", "false").unwrap(), "git".to_owned());
    }

    /*
    #[test]
    fn test_probe_or_fallback_not_standalone_probe_success() {
        // TODO: write this test
    }
    */

    #[test]
    fn test_split_out_workflow_one_default() {
        let data = "
format: 1
defaults:
  workflow:
    vcs: git";
        let config: Config = serde_yaml::from_str(&data).unwrap();

        match split_out_workflow(&config, "vcs") {
            Ok((workflow, implementation)) => {
                assert_eq!("vcs".to_owned(), workflow);
                assert_eq!("git".to_owned(), implementation);
            },
            Err(_) => panic!("Unexpected error"),
        }
    }

    #[test]
    fn test_split_out_workflow_one_no_default() {
        let config = Config::default();

        match split_out_workflow(&config, "vcs") {
            Ok((workflow, implementation)) => {
                assert_eq!("vcs".to_owned(), workflow);
                assert_eq!("auto".to_owned(), implementation);
            },
            Err(_) => panic!("Unexpected error"),
        }
    }

    #[test]
    fn test_split_out_workflow_two() {
        let config = Config::default();

        match split_out_workflow(&config, "vcs:git") {
            Ok((workflow, implementation)) => {
                assert_eq!("vcs".to_owned(), workflow);
                assert_eq!("git".to_owned(), implementation);
            },
            Err(_) => panic!("Unexpected error"),
        }
    }

    #[test]
    fn test_split_out_workflow_two_auto() {
        let config = Config::default();

        match split_out_workflow(&config, "vcs:auto/git") {
            Ok((workflow, implementation)) => {
                assert_eq!("vcs".to_owned(), workflow);
                assert_eq!("auto/git".to_owned(), implementation);
            },
            Err(_) => panic!("Unexpected error"),
        }
    }

    #[test]
    fn test_split_out_workflow_three() {
        let config = Config::default();
        assert!(split_out_workflow(&config, "vcs:git:err").is_err());
    }

    #[test]
    fn test_string_to_args_empty() {
        let vec: Vec<String> = Vec::new();
        assert_eq!(string_to_args(""), vec);
    }

    #[test]
    fn test_string_to_args_flag() {
        assert_eq!(string_to_args("--flag"), vec!["--flag"]);
    }

    #[test]
    fn test_string_to_args_quoted_text() {
        assert_eq!(string_to_args("--msg \"test\""), vec!["--msg", "test"]);
    }

    #[test]
    fn test_string_to_args_quoted_end_text() {
        assert_eq!(string_to_args("--flag --msg=\"test\""), vec!["--flag", "--msg=\"test\""]);
    }

    #[test]
    fn test_string_to_args_quoted_text_quote() {
        assert_eq!(string_to_args("--msg \"test\\\"s\""), vec!["--msg", "test\"s"]);
    }

    #[test]
    fn test_string_to_args_quoted_text_slash() {
        assert_eq!(string_to_args("--msg \"a\\b\""), vec!["--msg", "a\\b"]);
    }

    #[test]
    fn test_string_to_args_quoted_text_space() {
        assert_eq!(string_to_args("--msg \"test text\""), vec!["--msg", "test text"]);
    }

    #[test]
    fn test_string_to_args_slash() {
        assert_eq!(string_to_args("--msg a\\b"), vec!["--msg", "a\\b"]);
    }

    #[test]
    fn test_string_to_args_space() {
        assert_eq!(string_to_args("--msg test"), vec!["--msg", "test"]);
    }

    #[test]
    fn test_tick() {
        let amount = 10;
        let mut diff: i64 = 0;  // amount of ns diff of time between tick and non-tick

        // Measure how long a loop with no tick takes
        {
            let mut i = 0;
            let no_tick = Instant::now();

            while i < amount {
                i += 1;
            }

            let duration = no_tick.elapsed();

            diff -= (duration.as_secs() * (1000000000)) as i64 + duration.subsec_nanos() as i64;
        }

        // Measure how long a loop with ticks takes
        {
            let mut i = 0;
            let no_tick = Instant::now();

            while i < amount {
                tick();
                i += 1;
            }

            let duration = no_tick.elapsed();
            diff += (duration.as_secs() * (1000000000)) as i64 + duration.subsec_nanos() as i64;
        }

        // Extra time taken for tick loop should be between amount - 2 and amount + 2
        // amount * 10ms (time is stored in ns so ms is 10 ^ (9 - 3) )
        assert!(diff > ((amount - 2) * (10 * 1000000)));
        assert!(diff < ((amount + 2) * (10 * 1000000)));
    }

    #[test]
    fn test_use_fallback_one_auto() {
        assert!(use_fallback("auto").is_err());
    }

    #[test]
    fn test_use_fallback_one_git() {
        assert!(use_fallback("git").is_ok());
    }

    #[test]
    fn test_use_fallback_two_auto_git() {
        assert!(use_fallback("auto/git").is_ok());
    }

    #[test]
    fn test_use_fallback_two_git_bzr() {
        assert!(use_fallback("git/bzr").is_err());
    }

    #[test]
    fn test_use_fallback_three_auto_git_bzr() {
        assert!(use_fallback("auto/git/bzr").is_err());
    }
}
