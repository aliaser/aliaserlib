/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::vec::Vec;

use common::CommandTask;
use pipeline::{PipelineChunk, PipelineFilter};

/// Information about the current [`PipelineChunk`] in the [`Runner`]
///
/// This is used to store which pre-processors (variables) have been completed,
/// the [`next_command_task`] method builds each [`CommandTask`] in turn until
/// the final command.
///
/// The output from the pre-processors are stored in the [`variables`], this
/// then allows the [`Runner`] to build a [`CommandTaskArgs`] for the command -
/// with the relevant attributes and variables.
///
/// [`CommandTask`]: ../common/struct.CommandTask.html
/// [`CommandTaskArgs`]: ../common/struct.CommandTaskArgs.html
/// [`PipelineChunk`]: ../pipeline/struct.PipelineChunk.html
/// [`Runner`]: struct.Runner.html
/// [`next_command_task`]: #method.next_command_task
/// [`variables`]: #method.variables
pub struct RunnerChunk<'c> {
    /// The `PipelineChunk` that will be executed
    chunk: &'c PipelineChunk,
    /// Whether `next` has returned the `CommandTask` of the `PipelineChunk`
    complete: bool,
    /// List of pending processors (variables)
    pending: Vec<String>,
    /// The target for the output, None is pass up, Some(key) is the variable
    target: Option<String>,
    /// The output from any variables that have been run
    variables: BTreeMap<String, String>,
}

impl<'c> RunnerChunk<'c> {
    /// The filters for this `RunnerChunk`
    pub fn filters(&self) -> &'c Vec<Box<PipelineFilter>> {
        &self.chunk.post_filters()
    }

    /// Create a new `RunnerChunk` from the given [`PipelineChunk`]
    ///
    /// [`PipelineChunk`]: ../pipeline/struct.PipelineChunk.html
    pub fn new(chunk: &'c PipelineChunk) -> Self {
        let mut pending: Vec<String> = chunk.pre_processors().keys().cloned().collect();
        pending.reverse();  // store backwards as we pop

        Self {
            chunk: chunk,
            complete: false,
            pending: pending,
            target: None,
            variables: BTreeMap::new(),
        }
    }

    /// The next `CommandTask` to run from this `RunnerChunk`
    pub fn next_command_task(&mut self) -> Option<&'c CommandTask> {
        // If there are pending variables return last
        if let Some(ref key) = self.pending.pop() {
            self.target = Some(key.to_owned());
            self.chunk.pre_processors().get(key)
        } else if !self.complete {
            self.target = None;
            self.complete = true;

            Some(self.chunk.command())
        } else {
            self.target = None;
            None
        }
    }

    /// Where the output of the current `CommandTask` should go
    ///
    /// Either
    ///
    ///  * Target has a key - which is the variable to store the data as
    ///  * Target is None - which means the output should be pass 'up'
    pub fn target(&self) -> Option<&String> {
        self.target.as_ref()
    }

    /// Variables that have been stored in this chunk
    pub fn variables(&self) -> &BTreeMap<String, String> {
        &self.variables
    }

    /// Mutable reference to the variables within the `RunnerChunk`
    ///
    /// This is used when the `Runner` has received a `Packet` from an
    /// `Executer` which had a target of a variable.
    pub fn variables_mut(&mut self) -> &mut BTreeMap<String, String> {
        &mut self.variables
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use pipeline::helper::{make_pipeline_chunk, make_pipeline_chunk_with_data};

    #[test]
    fn test_filters() {
        let pipeline_chunk = make_pipeline_chunk_with_data();
        let chunk = RunnerChunk::new(&pipeline_chunk);

        assert_eq!(2, chunk.filters().len());
    }

    #[test]
    fn test_new() {
        let pipeline_chunk = make_pipeline_chunk();
        let chunk = RunnerChunk::new(&pipeline_chunk);

        assert!(chunk.filters().is_empty());
        assert!(chunk.target().is_none());
        assert!(chunk.variables().is_empty());

        assert_eq!(0, chunk.pending.len());
    }

    #[test]
    fn test_new_copies_pre_processors() {
        let pipeline_chunk = make_pipeline_chunk_with_data();
        let chunk = RunnerChunk::new(&pipeline_chunk);

        assert_eq!(2, chunk.pending.len());
    }

    #[test]
    fn test_next_command_task() {
        let pipeline_chunk = make_pipeline_chunk_with_data();
        let mut chunk = RunnerChunk::new(&pipeline_chunk);

        // We should get the first pre_processor
        let task_a = chunk.next_command_task();
        assert!(task_a.is_some());
        assert_eq!("/test/variable-a", task_a.unwrap().program().to_str().unwrap());

        assert!(chunk.target().is_some());
        assert_eq!("variable-a", chunk.target().unwrap());

        // We should get the second pre_processor
        let task_b = chunk.next_command_task();
        assert!(task_b.is_some());
        assert_eq!("/test/variable-b", task_b.unwrap().program().to_str().unwrap());

        assert!(chunk.target().is_some());
        assert_eq!("variable-b", chunk.target().unwrap());

        // We should get the Command
        let task_c = chunk.next_command_task();
        assert!(task_c.is_some());
        assert_eq!("/test/program", task_c.unwrap().program().to_str().unwrap());

        assert!(chunk.target().is_none());

        // We should get None next
        assert!(chunk.next_command_task().is_none());
    }

    #[test]
    fn test_next_command_task_empty() {
        let pipeline_chunk = make_pipeline_chunk();
        let mut chunk = RunnerChunk::new(&pipeline_chunk);

        // We should get the Command first
        let task = chunk.next_command_task();
        assert!(task.is_some());
        assert_eq!("/test/program", task.unwrap().program().to_str().unwrap());
        assert!(chunk.target().is_none());

        // We should get None next
        assert!(chunk.next_command_task().is_none());
    }

    #[test]
    fn test_target() {
        let pipeline_chunk = make_pipeline_chunk_with_data();
        let mut chunk = RunnerChunk::new(&pipeline_chunk);

        // Will start with None
        assert!(chunk.target().is_none());

        // Will change to Some(key) (for each variable)
        chunk.next_command_task();
        assert!(chunk.target().is_some());
        assert_eq!("variable-a", chunk.target().unwrap());

        chunk.next_command_task();
        assert!(chunk.target().is_some());
        assert_eq!("variable-b", chunk.target().unwrap());

        // Will change back to None (at end of variables)
        chunk.next_command_task();
        assert!(chunk.target().is_none());
    }

    #[test]
    fn test_variables() {
        let pipeline_chunk = make_pipeline_chunk();
        let mut chunk = RunnerChunk::new(&pipeline_chunk);

        assert!(chunk.variables().is_empty());

        chunk.variables_mut().insert("key".to_owned(), "value".to_owned());

        assert!(chunk.variables().contains_key("key"));
        assert_eq!("value", chunk.variables().get("key").unwrap());
    }
}
