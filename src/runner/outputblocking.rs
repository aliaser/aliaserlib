/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use common::Packet;
use runner::Runner;
use utils::tick;

/// Reads [`Packet`] from a given [`Runner`] blocking when there are none.
///
/// This is useful for streaming the output of the [`Runner`] to a target.
/// `OutputBlocking` implements a `Iterator` so could be used in the following
/// way.
///
/// ```text
/// let mut output = OutputBlocking::new(&mut runner);
///
/// for packet in output {
///   if let Some(data) = packet.stdout {
///     println!("{}", data);
///   }
/// }
/// ```
///
/// [`Packet`]: ../common/struct.Packet.html
/// [`Runner`]: struct.Runner.html
// TODO: real code ?
pub struct OutputBlocking<'r, 'p: 'r> {
    runner: &'r mut Runner<'p, 'p, 'p>,
}

impl<'r, 'p> OutputBlocking<'r, 'p> {
    /// Create a new `OutputBlocking` from a [`Runner`].
    ///
    /// [`Runner`]: struct.Runner.html
    pub fn new(runner: &'r mut Runner<'p, 'p, 'p>) -> OutputBlocking<'r, 'p> {
        OutputBlocking {
            runner: runner,
        }
    }
}

impl<'r, 'p> Iterator for OutputBlocking<'r, 'p> {
    type Item = Packet;

    fn next(&mut self) -> Option<Packet> {
        // See if there is a packet
        match self.runner.next_packet() {
            Some(packet) => Some(packet),
            None => {
                // See if we are still running
                match self.runner.active() {
                    true => {
                        tick();  // Don't kill CPU

                        // Recursively loop
                        // FIXME: should we use while loop ?
                        // are there limits on recursion ?
                        self.next()
                    },
                    false => None
                }
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::env;

    use pipeline::Pipeline;
    use runner::executer::{ExecuterMock, ExecuterType};

    use common::helper::make_command_task_with_args;
    use config::helper::make_config;
    use pipeline::helper::{make_request_empty, make_pipeline_chunk_from_task};

    #[test]
    fn test_iterator() {
        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, ExecuterMock::new());
        let output = OutputBlocking::new(&mut runner);

        let mut string = "".to_owned();

        for packet in output {
            if let Some(stdout) = packet.stdout {
                string.push_str(&stdout);
            }
        }

        let mut expected = "".to_owned();
        expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
        expected.push_str("Program: \"echo\" Args: [\"Hello World!\"]\n");

        assert_eq!(expected, string);
    }

    #[test]
    fn test_iterator_multi_chunk() {
        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Task A!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        {
            let task = make_command_task_with_args("echo", vec!("Task B!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, ExecuterMock::new());
        let output = OutputBlocking::new(&mut runner);

        let mut string = "".to_owned();

        for packet in output {
            if let Some(stdout) = packet.stdout {
                string.push_str(&stdout);
            }
        }

        let mut expected = "".to_owned();
        expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
        expected.push_str("Program: \"echo\" Args: [\"Task A!\"]\n");
        expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
        expected.push_str("Program: \"echo\" Args: [\"Task B!\"]\n");

        assert_eq!(expected, string);
    }

    #[test]
    fn test_iterator_none() {
        let config = make_config();
        let request = make_request_empty();
        let pipeline = Pipeline::from(&config, &request);

        let mut runner = Runner::new(&pipeline, ExecuterMock::new());
        let output = OutputBlocking::new(&mut runner);

        let mut string = "".to_owned();

        for packet in output {
            if let Some(stdout) = packet.stdout {
                string.push_str(&stdout);
            }
        }

        assert_eq!("".to_owned(), string);
    }
}
