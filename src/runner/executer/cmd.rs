/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::io::Read;
use std::process::{Child, Command, Stdio};
use std::sync::mpsc;
use std::thread;

use common::{CommandTask, CommandTaskArgs, Packet};
use error::LibError;
use runner::executer::ExecuterType;


/// Executes the [`CommandTask`] and [`CommandTaskArgs`] to the shell.
///
/// This is the normal [`ExecuterType`] to use when running commands on a host
///
/// [`CommandTask`]: ../../common/struct.CommandTask.html
/// [`CommandTaskArgs`]: ../../common/struct.CommandTaskArgs.html
/// [`ExecuterType`]: trait.ExecuterType.html
pub struct ExecuterCmd {
    command: Option<Command>,
    status: Option<i32>,

    status_rx: mpsc::Receiver<i32>,
    status_tx: mpsc::Sender<i32>,

    stderr_rx: mpsc::Receiver<Option<String>>,
    stderr_tx: mpsc::Sender<Option<String>>,

    stdout_rx: mpsc::Receiver<Option<String>>,
    stdout_tx: mpsc::Sender<Option<String>>,
}


impl ExecuterType for ExecuterCmd {
    /// Whether this ExecuterCmd is still running and there may be more packets
    fn active(&self) -> bool {
        // If there is no status then the command is still running
        self.status.is_none() && self.command.is_some()
    }

    /// Create a new `ExecuterCmd`
    fn new() -> Box<Self> {
        let (status_tx, status_rx) = mpsc::channel();
        let (stderr_tx, stderr_rx) = mpsc::channel();
        let (stdout_tx, stdout_rx) = mpsc::channel();

        Box::new(
            Self {
                command: None,
                status: None,

                status_rx: status_rx,
                status_tx: status_tx,

                stderr_rx: stderr_rx,
                stderr_tx: stderr_tx,

                stdout_rx: stdout_rx,
                stdout_tx: stdout_tx,
            }
        )
    }

    /// Non-blocking method to get the next [`Packet`] or None
    ///
    /// [`Packet`]: ../../common/struct.Packet.html
    fn next_packet(&mut self) -> Option<Packet> {
        // Join the two stderr and stdout streams together into a packet
        let stderr = match self.stderr_rx.try_recv() {
            Ok(data) => data,
            Err(_)   => None,
        };
        let stdout = match self.stdout_rx.try_recv() {
            Ok(data) => data,
            Err(_)   => None,
        };

        match (stderr, stdout) {
            (None, None) => {
                // There are no packets so we might have reached the end
                // check if there is a status
                if let Ok(status) = self.status_rx.try_recv() {
                    self.status = Some(status);
                }

                None
            },
            // TODO: actually do filtering on the Executer so that mock can show filters
            (data_stderr, data_stdout) => Some(Packet {
                stderr: data_stderr,
                stdout: data_stdout,
            }),
        }
    }

    //// Spawn the given [`CommandTask`] or error
    ///
    /// [`CommandTask`]: ../../common/struct.CommandTask.html
    fn spawn_command<'e>(&'e mut self, task: &CommandTask, args: CommandTaskArgs) -> Result<&'e mut ExecuterType, LibError> {
        // Check we aren't already active
        if self.active() {
            return Err(lib_error!(format!("Cannot spawn command: {} as ExecuterCmd is already active", task)));
        }

        // Build a Command from the task
        let mut command = ExecuterCmd::command_from_task(task, args);

        // Try to spawn the Command
        match command.spawn() {
            Ok(process) => {
                self.command = Some(command);
                self.status = None;

                // Start a thread to wait for the stderr, stdout and exit code
                self.start_monitor_thread(process);

                Ok(self)
            },
            // NOTE: we used to use err.raw_os_error()
            Err(err) => {
                self.command = Some(command);
                self.status = Some(-1);

                Err(lib_error!(format!("Failed to spawn command: {} - {}", task, err)))
            }
        }
    }

    /// The exit code of the [`CommandTask`] if it has ended
    ///
    /// [`CommandTask`]: ../../common/struct.CommandTask.html
    fn status(&self) -> Option<i32> {
        self.status
    }

    #[cfg(test)]
    /// The name of this Executer that tests can refer to
    fn name(&self) -> &str {
        "ExecuterCmd"
    }
}

impl ExecuterCmd {
    /// Create a `Command` from a `CommandTask`
    fn command_from_task(task: &CommandTask, args: CommandTaskArgs) -> Command {
        let mut command = Command::new(task.program().to_str().unwrap_or(""));
        command
            .current_dir(task.directory())
            .stderr(Stdio::piped())
            .stdout(Stdio::piped());

        // Ensure we have a clear environment
        command.env_clear();

        // Copy the environment from the task
        for (key, value) in task.env() {
            command.env(key, value);
        }

        // Copy the variables to the environment
        for (key, value) in args.variables() {
            // TODO: check for clashes ?
            command.env(key, value);
        }

        // Rename and replace the attributes
        let mut task_args = task.args().clone();

        for (key, value) in args.attributes() {
            // Create the unique name
            // TODO: this will have a random hash
            let new_key = format!("aliaser_RANDHASH{}", key);

            for arg in task_args.iter_mut() {
                // Replace the attr key with the new key if there is one
                // FIXME: using new_key doesn't seem to work
                // for now just put value there, this means quoting will fail
                *arg = arg.replace(&format!("${{{}}}", key), value.as_str());
            }

            // insert the value into the environment
            command.env(new_key, value);
        }

        // Add the changed args
        command.args(task_args);

        // TODO: expected status

        return command;
    }

    // TODO: move to Utils or something? are these useful?
    fn stdio_buffered_flush(tx: &mut mpsc::Sender<Option<String>>, buffer: &mut Vec<u8>) {
        tx.send(Some(String::from_utf8_lossy(buffer).into_owned())).unwrap();
        buffer.clear();
    }

    // TODO: move to Utils and allow buffer max and match byte (closure) to be configurable ?
    fn stdio_buffered_watcher<T: Read>(reader: T, mut tx: mpsc::Sender<Option<String>>) {
        let mut buffer = Vec::new();  // build a buffer to the first \r or \n

        for byte in reader.bytes() {
            match byte {
                Ok(byte) => {
                    buffer.push(byte);

                    match byte {
                        b'\r' | b'\n' => ExecuterCmd::stdio_buffered_flush(&mut tx, &mut buffer),
                        _ => {},
                    }
                }
                Err(_) => break
            }

            // Flush the buffer when it is larger than 1024, to prevent flooding
            if buffer.len() > 1024 {
                ExecuterCmd::stdio_buffered_flush(&mut tx, &mut buffer);
            }
        }

        // Ensure the buffer is flushed (eg if it didn't end with \r or \n)
        if !buffer.is_empty() {
            ExecuterCmd::stdio_buffered_flush(&mut tx, &mut buffer);
        }
    }

    fn start_monitor_thread(&mut self, mut process: Child) {
        // Clone the tx of the channels so we can use in the thread
        let status_tx = self.status_tx.clone();
        let stderr_tx = self.stderr_tx.clone();
        let stdout_tx = self.stdout_tx.clone();

        // Start a watcher thread
        thread::spawn(move || {
            // FIXME: unwrap ?
            let stderr = process.stderr.take().unwrap();
            let stdout = process.stdout.take().unwrap();

            // Start a thread which reads the stderr and stdout
            let stderr_thread = thread::spawn(move || {
                ExecuterCmd::stdio_buffered_watcher(stderr, stderr_tx);
            });
            let stdout_thread = thread::spawn(move || {
                ExecuterCmd::stdio_buffered_watcher(stdout, stdout_tx);
            });

            // Once stderr and stdout have finished wait for a status
            // FIXME: catch error case, this happens when Exec goes out of scope
            // while the thread is still running. Maybe kill() helps here ?
            stderr_thread.join().unwrap();
            stdout_thread.join().unwrap();

            // TODO: should we wait or listen for kill()
            match process.wait() {
                Ok(status) => {
                    status_tx.send(
                        match status.code() {
                            Some(code) => code,
                            None       => {
                                match status.success() {
                                    true => 0,
                                    false => 1,
                                }
                            }
                        }
                    ).unwrap();
                },
                Err(_) => status_tx.send(1).unwrap()
            }
        });
    }
}


#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::BTreeMap;
    use std::{thread, time};

    use utils::tick;
    use common::helper::make_command_task_with_args;

    #[test]
    fn test_active() {
        // Run the command echo Hello World
        // check that active is correct at all stages

        // Build up info
        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());

        // Build exec and check not active
        let mut exec = ExecuterCmd::new();
        assert_eq!(false, exec.active());

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert_eq!(true, exec.active());

        // Read until active is false
        let mut count: usize = 0;
        while exec.active() {
            if exec.next_packet().is_some() {
                count += 1;
            }

            tick();
        }

        // There should have been only one next_packet with data
        assert_eq!(1, count);

        // Active should be false
        assert_eq!(false, exec.active());
    }

    // TODO: test_command_from_task()
    // TODO: test_command_from_task_attributes
    // TODO: test_command_from_task_variables

    #[test]
    fn test_new() {
        let mut exec = ExecuterCmd::new();

        assert_eq!(false, exec.active());
        assert!(exec.status().is_none());
        assert!(exec.next_packet().is_none());
    }

    #[test]
    fn test_new_packet_multi() {
        // Test that `echo "Hello\nWorld!\nMultiple Packet Test!\n"`
        // causes multiple packets to be sent
        //
        // Buffer is hit when \n or \r is reached so we can use that
        // another option would be to read /dev/random or run $ yes

        // Build up info and exec
        let task = make_command_task_with_args("echo", vec!("Hello\nWorld!\nMultiple Packet Test!\n"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert_eq!(true, exec.active());

        // Read packets into list
        let mut out = vec!();

        while exec.active() {
            if let Some(packet) = exec.next_packet() {
                if let Some(stdout) = packet.stdout {
                    out.push(stdout);
                }
            }
        }

        // Extra checks to debug CI issue that occurs sometimes
        assert_eq!(false, exec.active());
        assert!(exec.next_packet().is_none());
        assert!(exec.status().is_some());
        assert_eq!(0, exec.status().unwrap());

        // Check that there are four packets and data is correct
        assert_eq!(4, out.len());
        assert_eq!("Hello\n", out[0]);
        assert_eq!("World!\n", out[1]);
        assert_eq!("Multiple Packet Test!\n", out[2]);
        assert_eq!("\n", out[3]);
    }

    #[test]
    fn test_new_packet_none_and_sleep() {
        // Test that with `sleep 1` we get none until one second has passed

        let task = make_command_task_with_args("sleep", vec!("1"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());

        // Loop until we are not active counting the time we waited
        let mut counter: usize = 0;

        while exec.active() {  // FIXME: this could hang in active is broken
            assert!(exec.next_packet().is_none());
            counter += 1;

            thread::sleep(time::Duration::from_millis(100));
        }

        // Test that the packets are flushed
        assert!(exec.next_packet().is_none());

        // Sleep for one second and loop at 100ms so expect 9 - 21 iterations
        // minimum of 9 iterations and max of 21 (double to allow slow perf)
        assert!(counter > 9);
        assert!(counter < 21);
    }

    #[test]
    fn test_new_packet_stderr() {
        // Check something that writes to stderr has output
        // TODO:
    }

    #[test]
    fn test_new_packet_stdout() {
        // Test that a simple `echo "Hello World"` produces a stdout packet

        // Build up info and exec
        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert_eq!(true, exec.active());

        // There should be one packet of info and we should still be active
        loop {  // FIXME: could be infinite
            // wait until first packet is found
            if let Some(packet1) = exec.next_packet() {
                let stdout = packet1.stdout;
                assert!(stdout.is_some());
                assert_eq!("Hello World!\n", stdout.unwrap().as_str());
                assert_eq!(true, exec.active());
                break;
            }

            tick();
        }

        // The next packet should be empty and we should become inactive
        let packet2 = exec.next_packet();
        assert!(packet2.is_none());
        assert_eq!(false, exec.active());
    }

    #[test]
    fn test_spawn_command() {
        // Test that we can spawn a echo command

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Spawn command
        assert!(exec.spawn_command(&task, task_info).is_ok());

        // Wait until it finishes otherwise we cause a panic in threads
        while exec.active() {
            exec.next_packet();
        }
    }

    #[test]
    fn test_spawn_command_active_fail() {
        // Test trying to spawn a second command fails

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info1 = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let task_info2 = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Spawn command and check it errors
        assert!(exec.spawn_command(&task, task_info1).is_ok());
        assert!(exec.spawn_command(&task, task_info2).is_err());

        // Wait until it finishes otherwise we cause a panic in threads
        while exec.active() {
            exec.next_packet();
        }
    }

    #[test]
    fn test_spawn_command_err() {
        // Run `invalid` check that it does not run as a command
        // and check exit code is -1

        let task = make_command_task_with_args("invalid", vec!());
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Start command and check it is none
        assert!(exec.spawn_command(&task, task_info).is_err());
        assert!(exec.status().is_some());
        assert_eq!(-1, exec.status().unwrap());
    }

    // TODO: test_spawn_command_environment (echo $ALIASER_TEST_VAR)
    // TODO: test_spawn_command_directory (pwd)

    // TODO: test for multiple spawn_command ? (as Runner will call >1?)

    // TODO: test_start_monitor_thread

    #[test]
    fn test_status() {
        // Run `pwd` check it has exit status of zero

        let task = make_command_task_with_args("pwd", vec!());
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Start command and check it is none
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert!(exec.status().is_none());

        // Read until we are not active
        while exec.active() {
            exec.next_packet();

            tick();
        }

        // Check we have a status of zero
        assert!(exec.status().is_some());
        assert_eq!(0, exec.status().unwrap());
    }

    #[test]
    fn test_status_non_zero() {
        // Run `bash -c "exit 255"` check it has exit status of 255

        let task = make_command_task_with_args("bash", vec!("-c", "exit 255"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterCmd::new();

        // Start command and check it is none
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert!(exec.status().is_none());

        // Read until we are not active
        while exec.active() {
            exec.next_packet();

            tick();
        }

        // Check we have a status of zero
        assert!(exec.status().is_some());
        assert_eq!(255, exec.status().unwrap());
    }

    // TODO: test_stdio_buffered_flush
    // TODO: test_stdio_buffered_watcher
}
