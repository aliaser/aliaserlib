/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
//! The types that run a given [`CommandTask`] to the shell or elsewhere
//!
//! [`CommandTask`]: ../../common/struct.CommandTask.html
use common::{CommandTask, CommandTaskArgs, Packet};
use error::LibError;

mod cmd;
mod mock;

pub use runner::executer::cmd::ExecuterCmd;
pub use runner::executer::mock::ExecuterMock;

/// Non-blocking trait for spawning a [`CommandTask`] and then retrieving data
///
/// [`CommandTask`]: ../common/struct.CommandTask.html
pub trait ExecuterType {
    /// Whether the [`CommandTask`] is still running and there are more
    /// [`Packet`]
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    /// [`Packet`]: ../common/struct.Packet.html
    fn active(&self) -> bool;

    /// Create a new `ExecuterType`
    ///
    /// We have new and spawn_command as the box'd type is given to [`Runner`]
    ///
    /// [`Runner`]: ../struct.Runner.html
    fn new() -> Box<Self> where Self: Sized;

    /// Non-blocking method to retrieve the next [`Packet`] or None
    ///
    /// [`Packet`]: ../common/struct.Packet.html
    fn next_packet(&mut self) -> Option<Packet>;

    /// Spawn the given [`CommandTask`] or error
    ///
    /// Note that this could be called multiple times, so it should protect
    /// against being called when active. Also it should reset any state.
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    fn spawn_command<'e>(&'e mut self, task: &CommandTask, args: CommandTaskArgs) -> Result<&'e mut ExecuterType, LibError>;

    /// The exit code of the [`CommandTask`]  if it has ended
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    fn status(&self) -> Option<i32>;

    // TODO: fn kill(&mut self)
    // Then clients can stop if required
    // maybe Ctrl+C can be linked to this as well

    #[cfg(test)]
    /// So that tests can easily check if the right Executer is being used
    // FIXME: Is this useful at all for client libs ?
    fn name(&self) -> &str;
}

// TODO: ExecuterCmd and ExecuterMock have some common tests
// are we able to run the same test cases here on both as they are a trait ?

