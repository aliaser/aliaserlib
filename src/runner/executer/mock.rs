/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::sync::mpsc;

use common::{CommandTask, CommandTaskArgs, Packet};
use error::LibError;
use runner::executer::ExecuterType;

/// Simply creates a string representing the [`CommandTask`] and
/// [`CommandTaskArgs`].
///
/// Mainly use for testing, dry-run and fallback purposes.
///
/// [`CommandTask`]: ../../common/struct.CommandTask.html
/// [`CommandTaskArgs`]: ../../common/struct.CommandTaskArgs.html
pub struct ExecuterMock {
    active: bool,
    code: Option<i32>,

    mock_code: Option<i32>,
    mock_err: Option<LibError>,

    status_rx: mpsc::Receiver<i32>,
    status_tx: mpsc::Sender<i32>,

    stdout_rx: mpsc::Receiver<Option<String>>,
    stdout_tx: mpsc::Sender<Option<String>>,
}

impl ExecuterType for ExecuterMock {
    /// Whether this ExecuterCmd is still running and there may be more packets
    fn active(&self) -> bool {
        self.active
    }

    /// Create a new `ExecuterMock`
    fn new() -> Box<Self> {
        let (status_tx, status_rx) = mpsc::channel();
        let (stdout_tx, stdout_rx) = mpsc::channel();

        Box::new(
            Self {
                active: false,
                code: None,

                mock_code: None,
                mock_err: None,

                status_rx: status_rx,
                status_tx: status_tx,

                stdout_rx: stdout_rx,
                stdout_tx: stdout_tx,
            }
        )
    }

    /// Non-blocking method to get the next [`Packet`] or None
    ///
    /// [`Packet`]: ../../common/struct.Packet.html
    fn next_packet(&mut self) -> Option<Packet> {
        match self.stdout_rx.try_recv() {
            Ok(data) => {
                Some(Packet {
                    stdout: data,
                    stderr: None,
                })
            },
            Err(_) => {
                // No more packets so stop, set active and code
                self.active = false;

                // If there is a pending status then read it
                // also if there is a mock_code then override with that
                if let Ok(status) = self.status_rx.try_recv() {
                    self.code = match self.mock_code {
                        Some(mock_code) => Some(mock_code),
                        None => Some(status),
                    };
                }

                None
            }
        }
    }

    /// Spawn the given [`CommandTask`] or error
    ///
    /// [`CommandTask`]: ../../common/struct.CommandTask.html
    fn spawn_command<'e>(&'e mut self, task: &CommandTask, args: CommandTaskArgs) -> Result<&'e mut ExecuterType, LibError> {
        // Check we aren't already active
        if self.active() {
            return Err(lib_error!(format!("Cannot spawn command: {} as ExecuterMock is already active", task)));
        }

        if let Some(err) = self.mock_err.take() {
            self.active = false;
            self.code = Some(-1);

            Err(err)
        } else {
            self.active = true;
            self.code = None;

            let data = ExecuterMock::string_from_task(task, args);

            if self.stdout_tx.send(Some(data)).is_err() {
                return Err(lib_error!("Unable to send stdout to receiver"));
            }

            if self.status_tx.send(0).is_err() {
                return Err(lib_error!("Unable to send status to receiver"));
            }

            Ok(self)
        }
    }

    /// The exit code of the [`CommandTask`] if it has ended
    ///
    /// [`CommandTask`]: ../../common/struct.CommandTask.html
    fn status(&self) -> Option<i32> {
        self.code
    }

    #[cfg(test)]
    /// The name of this Executer that tests can refer to
    fn name(&self) -> &str {
        "ExecuterMock"
    }
}

impl ExecuterMock {
    /// Create a string for the `CommandTask`
    fn string_from_task(task: &CommandTask, args: CommandTaskArgs) -> String {
        let mut data = "".to_owned();

        // Load attributes
        if args.attributes().len() > 0 {
            data.push_str("Attributes:\n");

            for (key, value) in args.attributes() {
                data.push_str(&format!("- {} = {:?}\n", key, value));
            }
        }

        // Load env
        if task.env().len() > 0 {
            data.push_str("Environment:\n");

            for (key, value) in task.env() {
                data.push_str(&format!("- {} = {:?}\n", key, value));
            }
        }

        // Load variables
        if args.variables().len() > 0 {
            data.push_str("Variables:\n");

            for (key, value) in args.variables() {
                // Variables are sub commands so put on their own line
                // we also trim the value as debug programs have newline already

                // Indent the values
                let split_value = value.trim().split("\n");
                let indented_values: Vec<String> = split_value.map(|s| format!("    {}", s)).collect();

                data.push_str(&format!("- {} =\n{}\n", key, indented_values.join("\n")));
            }
        }

        // Load directory
        data.push_str(&format!("Directory: {:?}\n", task.directory()));

        // Load program and args
        // Note we should end with \n otherwise console won't have new line at end
        data.push_str(&format!("Program: {:?} Args: {:?}\n", task.program(), task.args()));

        return data;
    }
}

#[cfg(test)]
impl ExecuterMock {
    pub fn mock_error(&mut self, error: Option<LibError>) {
        self.mock_err = error;
    }

    pub fn mock_status(&mut self, code: Option<i32>) {
        self.mock_code = code;
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::collections::BTreeMap;
    use std::env;
    use std::path::PathBuf;

    use common::helper::make_command_task_with_args;

    #[test]
    fn test_active() {
        // Test the active state

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());

        // Build exec and check not active
        let mut exec = ExecuterMock::new();
        assert_eq!(false, exec.active());

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert_eq!(true, exec.active());

        // There should be one packet of info and we should still be active
        assert!(exec.next_packet().is_some());
        assert_eq!(true, exec.active());

        // The next packet should be empty and we should become inactive
        assert!(exec.next_packet().is_none());
        assert_eq!(false, exec.active());
    }

    #[test]
    fn test_new() {
        // Test new starting state

        let mut exec = ExecuterMock::new();

        assert_eq!(false, exec.active());
        assert!(exec.status().is_none());
        assert!(exec.next_packet().is_none());
    }

    #[test]
    fn test_mock_error() {
        // Test mock_error causes spawn_command to error

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterMock::new();
        exec.mock_error(Some(lib_error!("Error!")));

        // Spawn command and check it errors
        assert!(exec.spawn_command(&task, task_info).is_err());
    }

    #[test]
    fn test_mock_status() {
        // Test mock_status causes status to be the code

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterMock::new();
        exec.mock_status(Some(255));

        assert!(exec.status().is_none());

        // Spawn command and check status still none
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert!(exec.status().is_none());

        // Read first packet and status is still none
        assert!(exec.next_packet().is_some());
        assert_eq!(true, exec.active());
        assert!(exec.status().is_none());

        // Hit next packet which causes active to be false and have a status
        assert!(exec.next_packet().is_none());
        assert_eq!(false, exec.active());
        assert!(exec.status().is_some());
        assert_eq!(255, exec.status().unwrap());
    }

    #[test]
    fn test_next_packet() {
        // Test packet stdout is data and sets active to false when complete

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());

        // Build exec and check not active
        let mut exec = ExecuterMock::new();
        assert_eq!(false, exec.active());

        // Spawn command and check it becomes active
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert_eq!(true, exec.active());

        // There should be one packet of info and we should still be active
        let packet = exec.next_packet();
        assert!(packet.is_some());
        assert_eq!(true, exec.active());

        // Check packet was correct
        let stdout = packet.unwrap().stdout;
        assert!(stdout.is_some());
        let mut expected = String::from("");
        expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
        expected.push_str("Program: \"echo\" Args: [\"Hello World!\"]\n");

        // The next packet should be empty and we should become inactive
        assert!(exec.next_packet().is_none());
        assert_eq!(false, exec.active());
    }

    #[test]
    fn test_spawn_command() {
        // Test spawn command works normally, program and args are correct

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterMock::new();

        // Spawn command and check it errors
        assert!(exec.spawn_command(&task, task_info).is_ok());

        let packet = exec.next_packet();
        assert!(packet.is_some());
        let stdout = packet.unwrap().stdout;
        assert!(stdout.is_some());

        let mut expected = String::from("");
        expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
        expected.push_str("Program: \"echo\" Args: [\"Hello World!\"]\n");

        assert_eq!(expected, stdout.unwrap());
    }

    #[test]
    fn test_spawn_command_active_fail() {
        // Test trying to spawn a second command fails

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info1 = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let task_info2 = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterMock::new();

        // Spawn command and check it errors
        assert!(exec.spawn_command(&task, task_info1).is_ok());
        assert!(exec.spawn_command(&task, task_info2).is_err());
    }

    // TODO: test for multiple spawn_command ? (as Runner will call >1)

    #[test]
    fn test_status() {
        // Test status is zero normally

        let task = make_command_task_with_args("echo", vec!("Hello World!"));
        let task_info = CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new());
        let mut exec = ExecuterMock::new();

        // Spawn command
        assert!(exec.spawn_command(&task, task_info).is_ok());
        assert!(exec.status().is_none());

        // Read packets
        assert!(exec.next_packet().is_some());
        assert!(exec.status().is_none());
        assert!(exec.next_packet().is_none());

        // Check status
        assert!(exec.status().is_some());
        assert_eq!(0, exec.status().unwrap());
    }

    #[test]
    fn test_string_from_task_all() {
        // Test string from task with all info
        // attributes, directory, environment, exec, variables
        // test_spawn_command tests the case of only directory and exec

        let arguments = vec!("${ENV_VAR}".to_owned(), "${ALIASER_VAR}".to_owned(), "${ALIASER_ATTR}".to_owned());
        let mut attributes = BTreeMap::new();
        attributes.insert("ALIASER_ATTR".to_owned(), "aliaser_attr_val".to_owned());
        attributes.insert("ALIASER_ATTR_B".to_owned(), "aliaser_attr_val_b".to_owned());
        let directory = PathBuf::from("/tmp");
        let mut environment = BTreeMap::new();
        environment.insert("ENV_VAR".to_owned(), "env_var_val".to_owned());
        environment.insert("ENV_VAR_B".to_owned(), "env_var_val_b".to_owned());
        let program = "echo";
        let mut variables = BTreeMap::new();
        variables.insert("ALIASER_VAR".to_owned(), "aliaser_var_val_a\naliaser_var_val_b".to_owned());
        variables.insert("ALIASER_VAR_B".to_owned(), "aliaser_var_b_val_a\naliaser_var_b_val_b".to_owned());

        let task = CommandTask::new(arguments, directory, environment, PathBuf::from(program));
        let task_info = CommandTaskArgs::new(&attributes, &variables);

        let expected = String::from("Attributes:
- ALIASER_ATTR = \"aliaser_attr_val\"
- ALIASER_ATTR_B = \"aliaser_attr_val_b\"
Environment:
- ENV_VAR = \"env_var_val\"
- ENV_VAR_B = \"env_var_val_b\"
Variables:
- ALIASER_VAR =
    aliaser_var_val_a
    aliaser_var_val_b
- ALIASER_VAR_B =
    aliaser_var_b_val_a
    aliaser_var_b_val_b
Directory: \"/tmp\"
Program: \"echo\" Args: [\"${ENV_VAR}\", \"${ALIASER_VAR}\", \"${ALIASER_ATTR}\"]
");
        assert_eq!(expected, ExecuterMock::string_from_task(&task, task_info));
    }
}
