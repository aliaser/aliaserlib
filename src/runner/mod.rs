/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
//! Contains parts which allow for running given commands and retrieving data
//! from the executers.
use std::collections::BTreeMap;
use std::slice::Iter;

mod chunk;
pub mod executer;
mod outputblocking;

use common::{CommandTask, CommandTaskArgs, Packet};
use error::LibError;
use pipeline::{Pipeline, PipelineChunk};
use runner::chunk::RunnerChunk;
use runner::executer::ExecuterType;

pub use runner::outputblocking::OutputBlocking;


/// A `Runner` is used to execute a [`Pipeline`] and then allow access to
/// [`Packet`]
///
/// [`Packet`]: ../common/struct.Packet.html
/// [`Pipeline`]: ../pipeline/struct.Pipeline.html
pub struct Runner<'p, 'c: 'p, 'r: 'p> {
    exec: Box<ExecuterType>,
    /// The current `RunnerChunk` that is being worked on
    chunk: Option<RunnerChunk<'p>>,
    /// Any inner error that has occurred, eg spawning a process
    inner_error: Option<LibError>,
    iterator: Iter<'p, PipelineChunk>,
    pipeline: &'p Pipeline<'c, 'r>,
    /// The exit status of this `Runner`
    status: Option<i32>,

    // TODO: have
    // chunks: Vec<RunnerChunk>
    // i: usize  // or iterator? but then still immutable vs mutable borrow

    // Change to a list of chunks which always has at least 1 chunk
    // then can do chunk.first() but that is Option anyway

    // Instead have a list of chunks and pop from the front ?
    // first chheck if we need to move to a new chunk
    // then use that chunk to get info
    // should get around double immutable issues if we call direct to .chunk
}

impl<'p, 'c, 'r> Runner<'p, 'c, 'r> {
    /// Whether the `Runner` is active
    pub fn active(&self) -> bool {
        self.chunk.is_some()
    }

    /// Pick the default `ExecuterType`
    pub fn default_executer() -> Box<ExecuterType> {
        executer::ExecuterCmd::new()
    }

    /// Pick the default `ExecuterType` for a specific key
    pub fn default_executer_for_key(key: &str) -> Option<Box<ExecuterType>> {
        match key {
            "auto" => Some(Runner::default_executer()),
            "cmd" => Some(executer::ExecuterCmd::new()),
            "mock" => Some(executer::ExecuterMock::new()),
            _ => None
        }
    }

    /// Create a new `Runner` for a given [`Pipeline`]
    ///
    /// [`Pipeline`]: ../pipeline/struct.Pipeline.html
    pub fn new(pipeline: &'p Pipeline<'c, 'r>, exec: Box<ExecuterType>) -> Self {
        Self {
            chunk: None,
            exec: exec,
            inner_error: None,
            iterator: pipeline.iter(),
            pipeline: pipeline,
            status: None,
        }
    }

    /// If there is a [`Packet`] then return it otherwise None
    ///
    /// Note that a None here does not mean that the `Runner` is finished, it
    /// just means that there is currently no data to return. Instead use
    /// `active` to determine if the `Runner` is still running.
    ///
    /// This method also spawns the sub-commands and stores variable output
    ///
    /// [`Packet`]: ../common/struct.Packet.html
    pub fn next_packet(&mut self) -> Option<Packet> {
        // If there is a `Packet` in the `Executer` then use it
        if let Some(packet) = self.exec.next_packet() {
            if let Some(ref mut chunk) = self.chunk {
                Runner::output_or_variable(chunk, packet)
            } else {
                // TODO: error there is exec but no chunk?!
                None
            }
        } else {
            // If the executer is not active try to move to next `CommandTask`
            if !self.exec.active() {
                // If there is a status that looks like an error then stop
                // TODO: Read expected status from Pipeline::Chunk/CommandTask
                //
                // maybe have Executer::status() -> Result<Option<i32>, Option<i32>>
                // this can read from the CommandTask::valid_status or something
                if let Some(status) = self.exec.status() {
                    if status != 0 {
                        self.chunk = None;
                        self.status = Some(status);

                        // Consume iterator
                        while self.iterator.next().is_some() {

                        }

                        return None;
                    }
                }

                // From the chunk see if there is a next command
                // Get chunk in it's own scope so that we can write to chunk later
                if let Some(task) = match self.chunk {
                    Some(ref mut chunk) => chunk.next_command_task(),
                    None => None  // TODO: warn
                } {
                    // There is another `CommandTask` in this `RunnerChunk`
                    // so try to spawn it
                    self.spawn_command_task(task);
                } else {
                    // There are no more `CommandTask` in this `RunnerChunk`
                    // so move to the next `Chunk` in the `Pipeline`
                    self.move_next_chunk();
                }
            }

            None
        }
    }

    /// The exit status of this `Runner`
    pub fn status(&self) -> Option<i32> {
        self.status
    }

    /// If an error occurred then take it
    pub fn take_error(&mut self) -> Option<LibError> {
        self.inner_error.take()
    }
 }

impl<'p, 'c, 'r> Runner<'p, 'c, 'r> {
    /// Move to the next [`PipelineChunk`] in the [`Pipeline`] iterator
    ///
    /// If we have reached the end then set the status as success
    ///
    /// [`Pipeline`]: ../pipeline/struct.Pipeline.html
    /// [`PipelineChunk`]: ../pipeline/struct.PipelineChunk.html
    fn move_next_chunk(&mut self) {
        self.chunk = match self.iterator.next() {
            Some(new_chunk) => Some(RunnerChunk::new(new_chunk)),
            None => {
                // We have reached end, set successful status
                self.status = Some(0);
                None
            }
        }
    }

    /// If the chunk has a target then store the packet in the variable
    /// otherwise return the packet to be sent to the client
    fn output_or_variable(chunk: &mut RunnerChunk<'p>, mut packet: Packet) -> Option<Packet> {
        // Clone so that we can borrow as mut below, also we would clone anyway
        match chunk.target().cloned() {
            Some(target) => {
                // For now it only takes the stdout
                if let Some(ref data) = packet.stdout {
                    chunk.variables_mut().entry(target.to_owned()).or_insert("".to_owned()).push_str(data);
                }

                None
            },
            None => {
                // If there is no target for the output then check if filters
                // can be run as well
                // TODO: will this be inside the ExecuterType ?
                for filter in chunk.filters() {
                    packet = filter.process_packet(packet);
                }

                Some(packet)
            }
        }
    }

    /// Spawn the given [`CommandTask`] with the `ExecuterType`
    ///
    /// If the current `RunnerChunk` has no target, then attributes and
    /// variables from the `Pipeline` and `RunnerChunk` will be given to the
    /// `ExecuterType`
    ///
    /// [`CommandTask`]: ../common/struct.CommandTask.html
    /// [`ExecuterType`]: ../executer/trait.ExecuterType.html
    fn spawn_command_task(&mut self, task: &'p CommandTask) {
        // If the chunk doesn't have a target, then build the
        // relevant attributes and variables.
        let args = match self.chunk {
            Some(ref chunk) => {
                match chunk.target() {
                    Some(_) => CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new()),
                    None => CommandTaskArgs::new(self.pipeline.request().attributes(), chunk.variables()),
                }
            },
            None => {
                // TODO: warn
                CommandTaskArgs::new(&BTreeMap::new(), &BTreeMap::new())
            }
        };

        // There is so try to spawn the command with the args
        if let Err(err) = self.exec.spawn_command(task, args) {
            // There was an error spawning so stop
            self.inner_error = Some(err);
            self.chunk = None;

            // Consume iterator
            while self.iterator.next().is_some() {

            }

            // If there was a status then copy it
            if let Some(status) = self.exec.status() {
                self.status = Some(status);
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use std::env;

    use common::helper::make_command_task_with_args;
    use config::helper::make_config;
    use pipeline::helper::{make_request_empty, make_pipeline_chunk_from_task};

    #[test]
    fn test_error() {
        // Test single error is exposed

        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut exec = executer::ExecuterMock::new();
        exec.mock_error(Some(lib_error!("Error!")));

        let mut runner = Runner::new(&pipeline, exec);

        // First packet is None as we move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we spawn the chunk
        // Here is where we error
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        // Check that there is an error
        assert!(runner.take_error().is_some());

        // There should be a -1 status code
        assert!(runner.status().is_some());
        assert_eq!(-1, runner.status().unwrap());
    }

    #[test]
    fn test_error_multi_chunk_stop() {
        // Test error stops multiple chunks

        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Task A!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        {
            let task = make_command_task_with_args("echo", vec!("Task B!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut exec = executer::ExecuterMock::new();
        exec.mock_error(Some(lib_error!("Error!")));

        let mut runner = Runner::new(&pipeline, exec);

        // First packet is None as we move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we spawn the chunk
        // Here is where we error
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        // Check that there is an error
        assert!(runner.take_error().is_some());

        // There should be a -1 status code
        assert!(runner.status().is_some());
        assert_eq!(-1, runner.status().unwrap());

        // Ensure that we can't leak to the next chunk and we have stopped
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());
    }

    #[test]
    fn test_default_executer() {
        assert_eq!("ExecuterCmd", Runner::default_executer().name());
    }

    #[test]
    fn test_default_executer_for_key() {
        {
            let exec = Runner::default_executer_for_key("auto");
            assert!(exec.is_some());
            assert_eq!("ExecuterCmd", exec.unwrap().name());
        }

        {
            let exec = Runner::default_executer_for_key("cmd");
            assert!(exec.is_some());
            assert_eq!("ExecuterCmd", exec.unwrap().name());
        }

        {
            let exec = Runner::default_executer_for_key("mock");
            assert!(exec.is_some());
            assert_eq!("ExecuterMock", exec.unwrap().name());
        }

        assert!(Runner::default_executer_for_key("test").is_none());
    }

    #[test]
    fn test_new() {
        let config = make_config();
        let request = make_request_empty();
        let pipeline = Pipeline::from(&config, &request);

        let mut runner = Runner::new(&pipeline, executer::ExecuterMock::new());

        assert_eq!(false, runner.active());
        assert!(runner.take_error().is_none());
        assert!(runner.status().is_none());
    }

    #[test]
    fn test_next_packet() {
        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, executer::ExecuterMock::new());
        assert_eq!(false, runner.active());

        // First packet is None as we are move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we are spawning chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Next packet will have data in ExecuterMock
        let packet = runner.next_packet();
        assert_eq!(true, runner.active());

        assert!(packet.is_some());

        // Check packet was correct
        {
            let stdout = packet.unwrap().stdout;
            assert!(stdout.is_some());

            let mut expected = String::from("");
            expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
            expected.push_str("Program: \"echo\" Args: [\"Hello World!\"]\n");

            assert_eq!(expected, stdout.unwrap());
        }

        // Next packet should be None and we should be become inactive
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());
    }

    #[test]
    fn test_next_packet_multi_chunk() {
        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Task A!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        {
            let task = make_command_task_with_args("echo", vec!("Task B!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, executer::ExecuterMock::new());
        assert_eq!(false, runner.active());

        // First packet is None as we are move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we are spawning chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        {
            // Next packet will have data in ExecuterMock for task A
            let packet = runner.next_packet();
            assert_eq!(true, runner.active());
            assert!(packet.is_some());

            // Check packet was correct
            let stdout = packet.unwrap().stdout;
            assert!(stdout.is_some());

            let mut expected = String::from("");
            expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
            expected.push_str("Program: \"echo\" Args: [\"Task A!\"]\n");

            assert_eq!(expected, stdout.unwrap());
        }

        // Next packet is None as we are moving to next chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Next packet is None as we are spawning chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        {
            // Next packet will have data in ExecuterMock for task A
            let packet = runner.next_packet();
            assert_eq!(true, runner.active());
            assert!(packet.is_some());

            // Check packet was correct
            let stdout = packet.unwrap().stdout;
            assert!(stdout.is_some());

            let mut expected = String::from("");
            expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
            expected.push_str("Program: \"echo\" Args: [\"Task B!\"]\n");

            assert_eq!(expected, stdout.unwrap());
        }

        // Next packet should be None and we should be become inactive
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());
    }

    #[test]
    fn test_new_packet_pre_processor() {
        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let mut chunk = make_pipeline_chunk_from_task(task);

            let variable_a = make_command_task_with_args("/test/variable-a", vec!());
            chunk.add_pre_processor("variable-a", variable_a);

            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, executer::ExecuterMock::new());
        assert_eq!(false, runner.active());

        // First packet is None as we are moving to variable-a chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we are spawning variable-a chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Third packet is None as we are moving to exec chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Fourth packet is None as we are spawning exec chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Next packet will have data in ExecuterMock
        let packet = runner.next_packet();
        assert_eq!(true, runner.active());

        assert!(packet.is_some());

        // Check packet was correct
        {
            let stdout = packet.unwrap().stdout;
            assert!(stdout.is_some());

            let mut expected = String::from("");
            expected.push_str("Variables:\n- variable-a =\n");
            expected.push_str(&format!("    Directory: {:?}\n", env::current_dir().unwrap()));
            expected.push_str("    Program: \"/test/variable-a\" Args: []\n");
            expected.push_str(&format!("Directory: {:?}\n", env::current_dir().unwrap()));
            expected.push_str("Program: \"echo\" Args: [\"Hello World!\"]\n");

            assert_eq!(expected, stdout.unwrap());
        }

        // Next packet should be None and we should be become inactive
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());
    }

    #[test]
    fn test_status() {
        // Test normal single chunk exits with zero

        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut runner = Runner::new(&pipeline, executer::ExecuterMock::new());

        // First packet is None as we move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we spawn the chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Third packet is Some as chunk data appears
        assert!(runner.next_packet().is_some());
        assert_eq!(true, runner.active());

        // Fourth packet is None as chunk has finished
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        assert!(runner.status().is_some());
        assert_eq!(0, runner.status().unwrap());
    }

    #[test]
    fn test_status_non_zero() {
        // Test non-zero status is exposed

        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Hello World!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut exec = executer::ExecuterMock::new();
        exec.mock_status(Some(255));

        let mut runner = Runner::new(&pipeline, exec);

        // First packet is None as we move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we spawn the chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Third packet is Some as chunk data appears
        assert!(runner.next_packet().is_some());
        assert_eq!(true, runner.active());

        // Fourth packet is None as chunk has finished
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        // Check that there is a status
        assert!(runner.status().is_some());
        assert_eq!(255, runner.status().unwrap());
    }

    #[test]
    fn test_status_non_zero_multi_chunk_stop() {
        // Test non-zero stops multiple chunks

        let config = make_config();
        let request = make_request_empty();
        let mut pipeline = Pipeline::from(&config, &request);

        {
            let task = make_command_task_with_args("echo", vec!("Task A!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        {
            let task = make_command_task_with_args("echo", vec!("Task B!"));
            let chunk = make_pipeline_chunk_from_task(task);
            pipeline.append(chunk);
        }

        let mut exec = executer::ExecuterMock::new();
        exec.mock_status(Some(255));

        let mut runner = Runner::new(&pipeline, exec);

        // First packet is None as we move to chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Second packet is None as we spawn the chunk
        assert!(runner.next_packet().is_none());
        assert_eq!(true, runner.active());

        // Third packet is Some as chunk data appears
        assert!(runner.next_packet().is_some());
        assert_eq!(true, runner.active());

        // Fourth packet is None as chunk has finished
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        // Check that there is a status
        assert!(runner.status().is_some());
        assert_eq!(255, runner.status().unwrap());

        // Ensure that we can't leak to the next chunk and we have stopped
        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());

        assert!(runner.next_packet().is_none());
        assert_eq!(false, runner.active());
    }

    // TODO: internal methods ?
}

