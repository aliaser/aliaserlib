/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */

//! Custom logging module that allows for changing the [`LogLevel`]
//!
//! This module allows for the use of logging and changing the [`LogLevel`]
//! during the middle of a program. It does this by the use of a Singleton
//! and channels.
//!
//! Below is an example of setting the logging level, an error being logged
//! and a client receiving the packet. Normally the `try_recv` will be done in a
//! loop on a background thread of the client.
//!
//! ```rust
//! # #[macro_use] extern crate aliaserlib;
//! # use aliaserlib::logging::{enable_logging, LogLevel, try_recv};
//! # fn main() {
//! enable_logging(LogLevel::Warn);
//! error!("My error!");
//!
//! let packet = try_recv();
//! assert!(packet.is_some());
//! let (level, data) = packet.unwrap();
//! assert_eq!(LogLevel::Error, level);
//! assert_eq!("My error!".to_owned(), data);
//! # }
//! ```
//!
//! [`LogLevel`]: enum.LogLevel.html

use std::sync::{Arc, mpsc, Mutex, RwLock};

lazy_static! {
    static ref LOGGER: Logger = Logger::default();
}

/// The level of logging requested by the user
#[derive(Clone, Debug, PartialEq, PartialOrd)]
pub enum LogLevel {
    /// Designates very serious errors.
    Error = 0,
    /// Designates hazardous situations.
    Warn,
    /// Designates useful information.
    Info,
    /// Designates lower priority information.
    Debug,
    /// Designates very low priority, often extremely verbose, information.
    Trace
}

struct Logger {
    rx: Arc<Mutex<mpsc::Receiver<(LogLevel, String)>>>,
    state: RwLock<LogLevel>,
    tx: Arc<Mutex<mpsc::Sender<(LogLevel, String)>>>,
}

impl Default for Logger {
    fn default() -> Self {
        let (tx, rx) = mpsc::channel();

        Self {
            rx: Arc::new(Mutex::new(rx)),
            state: RwLock::new(LogLevel::Warn),
            tx: Arc::new(Mutex::new(tx)),
        }
    }
}

/// Enable logging at the given [`LogLevel`]
///
/// Note that this method can be called multiple times and log requests after
/// any change will respect the new LogLevel.
///
/// [`LogLevel`]: enum.LogLevel.html
pub fn enable_logging(log_level: LogLevel) {
    let mut state = LOGGER.state.write().unwrap();
    *state = log_level;
}

/// Log the given data with the given LogLevel
/// If the current logging level is the same or lower than the given level
/// the data will be sent to the reader
pub fn log(data: String, log_level: LogLevel) {
    let state = LOGGER.state.read().unwrap();

    // If the given LogLevel is less than or equal to the current logging LogLevel
    // then send to the tx so the client receives the message
    if log_level <= *state {
        let tx = LOGGER.tx.lock().unwrap();
        tx.send((log_level, data)).unwrap();
    }
}

/// Return if there is a packet pending in the log channel
pub fn try_recv() -> Option<(LogLevel, String)> {
    let rx = LOGGER.rx.lock().unwrap();
    rx.try_recv().ok()
}

/// Logs a message at the debug level
///
/// ```rust
/// # #[macro_use] extern crate aliaserlib;
/// # fn main() {
/// debug!("Hello {}", "World!");
/// # }
/// ```
#[macro_export]
macro_rules! debug {
    ($( $args:expr ),+) => (
        $crate::logging::log(format!($( $args ),+), $crate::logging::LogLevel::Debug)
    );
}

/// Logs a message at the error level
///
/// ```rust
/// # #[macro_use] extern crate aliaserlib;
/// # fn main() {
/// error!("Hello {}", "World!");
/// # }
/// ```
#[macro_export]
macro_rules! error {
    ($( $args:expr ),+) => (
        $crate::logging::log(format!($( $args ),+), $crate::logging::LogLevel::Error)
    );
}

/// Logs a message at the info level
///
/// ```rust
/// # #[macro_use] extern crate aliaserlib;
/// # fn main() {
/// info!("Hello {}", "World!");
/// # }
/// ```
#[macro_export]
macro_rules! info {
    ($( $args:expr ),+) => (
        $crate::logging::log(format!($( $args ),+), $crate::logging::LogLevel::Info)
    );
}

/// Logs a message at the trace level
///
/// ```rust
/// # #[macro_use] extern crate aliaserlib;
/// # fn main() {
/// trace!("Hello {}", "World!");
/// # }
/// ```
#[macro_export]
macro_rules! trace {
    ($( $args:expr ),+) => (
        $crate::logging::log(format!($( $args ),+), $crate::logging::LogLevel::Trace)
    );
}

/// Logs a message at the warn level
///
/// ```rust
/// # #[macro_use] extern crate aliaserlib;
/// # fn main() {
/// warn!("Hello {}", "World!");
/// # }
/// ```
#[macro_export]
macro_rules! warn {
    ($( $args:expr ),+) => (
        $crate::logging::log(format!($( $args ),+), $crate::logging::LogLevel::Warn)
    );
}

#[cfg(test)]
mod tests {
    use super::*;

    // Each tests depends on LOGGER being in a certain state
    // so to prevent parallel issues, they should lock the mutex

    // TODO: use macro to capture when things error ?
    // https://github.com/rust-lang/rust/issues/43155#issuecomment-315543432
    lazy_static! {
        static ref TEST_MUTEX: Mutex<()> = Mutex::new(());
    }

    #[test]
    fn test_enable_logging() {
        let _guard = TEST_MUTEX.lock().unwrap();

        {
            let state = LOGGER.state.read().unwrap();
            assert_eq!(LogLevel::Warn, *state);
        }

        enable_logging(LogLevel::Error);

        {
            let state = LOGGER.state.read().unwrap();
            assert_eq!(LogLevel::Error, *state);
        }

        enable_logging(LogLevel::Info);

        {
            let state = LOGGER.state.read().unwrap();
            assert_eq!(LogLevel::Info, *state);
        }

        enable_logging(LogLevel::Warn);

        {
            let state = LOGGER.state.read().unwrap();
            assert_eq!(LogLevel::Warn, *state);
        }
    }

    #[test]
    fn test_logger_default() {
        let logger = Logger::default();
        let state = logger.state.read();
        assert!(state.is_ok());
        assert_eq!(LogLevel::Warn, *state.unwrap());
    }

    #[test]
    fn test_log_level_ordering() {
        assert!(LogLevel::Error < LogLevel::Warn);
        assert!(LogLevel::Warn < LogLevel::Info);
        assert!(LogLevel::Info < LogLevel::Debug);
        assert!(LogLevel::Debug < LogLevel::Trace);
        assert!(LogLevel::Trace > LogLevel::Error);
    }

    #[test]
    fn test_log() {
        let _guard = TEST_MUTEX.lock().unwrap();

        assert!(try_recv().is_none());

        log("test".to_owned(), LogLevel::Error);
        assert!(try_recv().is_some());
        assert!(try_recv().is_none());
    }

    #[test]
    fn test_log_dropped() {
        let _guard = TEST_MUTEX.lock().unwrap();

        assert!(try_recv().is_none());

        log("test".to_owned(), LogLevel::Trace);
        assert!(try_recv().is_none());
    }

    #[test]
    fn test_try_recv() {
        let _guard = TEST_MUTEX.lock().unwrap();

        assert!(try_recv().is_none());

        log("test".to_owned(), LogLevel::Error);
        let packet = try_recv();
        assert!(packet.is_some());

        let (level, data) = packet.unwrap();
        assert_eq!(LogLevel::Error, level);
        assert_eq!("test".to_owned(), data);
    }

    #[test]
    fn test_macro_debug() {
        let _guard = TEST_MUTEX.lock().unwrap();

        // Should be dropped
        enable_logging(LogLevel::Error);
        debug!("test");
        assert!(try_recv().is_none());

        // Should go through
        enable_logging(LogLevel::Debug);
        debug!("Hello {}", "World!");
        assert!(try_recv().is_some());

        enable_logging(LogLevel::Warn);
    }

    #[test]
    fn test_macro_error() {
        let _guard = TEST_MUTEX.lock().unwrap();

        // Should go through
        enable_logging(LogLevel::Error);
        error!("Hello {}", "World!");
        assert!(try_recv().is_some());

        enable_logging(LogLevel::Warn);
    }

    #[test]
    fn test_macro_info() {
        let _guard = TEST_MUTEX.lock().unwrap();

        // Should be dropped
        enable_logging(LogLevel::Error);
        info!("test");
        assert!(try_recv().is_none());

        // Should go through
        enable_logging(LogLevel::Info);
        info!("Hello {}", "World!");
        assert!(try_recv().is_some());

        enable_logging(LogLevel::Warn);
    }

    #[test]
    fn test_macro_trace() {
        let _guard = TEST_MUTEX.lock().unwrap();

        // Should be dropped
        enable_logging(LogLevel::Error);
        trace!("test");
        assert!(try_recv().is_none());

        // Should go through
        enable_logging(LogLevel::Trace);
        trace!("Hello {}", "World!");
        assert!(try_recv().is_some());

        enable_logging(LogLevel::Warn);
    }

    #[test]
    fn test_macro_warn() {
        let _guard = TEST_MUTEX.lock().unwrap();

        // Should be dropped
        enable_logging(LogLevel::Error);
        warn!("test");
        assert!(try_recv().is_none());

        // Should go through
        enable_logging(LogLevel::Warn);
        warn!("Hello {}", "World!");
        assert!(try_recv().is_some());
    }
}
