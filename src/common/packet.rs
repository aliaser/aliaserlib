/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */

/// A packet of data from a command
pub struct Packet {
    /// Optional data from the stderr stream
    pub stderr: Option<String>,
    /// Optional data from the stdout stream
    pub stdout: Option<String>,
}

impl Packet {
    /// Create a [`Packet`] with no dat inside
    ///
    /// [`Packet`]: struct.Packet.html
    pub fn new() -> Self {
        Self {
            stderr: None,
            stdout: None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        // Test that stderr and stdout are none at start
        let packet = Packet::new();

        assert!(packet.stderr.is_none());
        assert!(packet.stdout.is_none());
    }

    #[test]
    fn test_stdio_write() {
        // Test that stderr and stdout can be changed to Some(string)
        let mut packet = Packet::new();
        packet.stderr = Some("stderr".to_owned());
        packet.stdout = Some("stdout".to_owned());

        assert!(packet.stderr.is_some());
        assert_eq!("stderr", packet.stderr.unwrap().as_str());

        assert!(packet.stdout.is_some());
        assert_eq!("stdout", packet.stdout.unwrap().as_str());
    }
}
