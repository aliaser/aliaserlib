/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;

/// Extra information for a command, this contains attributes and variables
///
/// This is given to the [`ExecuterType`] by the [`Runner`].
///
/// [`ExecuterType`]: ../executer/trait.ExecuterType.html
/// [`Runner`]: ../runner/struct.Runner.html
pub struct CommandTaskArgs {
    attributes: BTreeMap<String, String>,
    variables: BTreeMap<String, String>,
}

impl CommandTaskArgs {
    /// Attributes of the [`CommandTaskArgs`]
    ///
    /// [`CommandTaskArgs`]: struct.CommandTaskArgs.html
    pub fn attributes(&self) -> &BTreeMap<String, String> {
        &self.attributes
    }

    /// Create a `CommandTaskArgs` from the information
    // TODO: should these be &BTreeMap<&str, &str> ?
    pub fn new(attributes: &BTreeMap<String, String>, variables: &BTreeMap<String, String>) -> Self {
        Self {
            attributes: attributes.clone(),
            variables: variables.clone(),
        }
    }

    /// Variables of the [`CommandTaskArgs`]
    ///
    /// [`CommandTaskArgs`]: struct.CommandTaskArgs.html
    pub fn variables(&self) -> &BTreeMap<String, String> {
        &self.variables
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        let mut attributes = BTreeMap::new();
        attributes.insert("a".to_owned(), "b".to_owned());

        let mut variables = BTreeMap::new();
        variables.insert("c".to_owned(), "d".to_owned());

        let taskargs = CommandTaskArgs::new(&attributes, &variables);

        assert!(taskargs.attributes().contains_key("a"));
        assert_eq!("b", taskargs.attributes()["a"].as_str());

        assert!(taskargs.variables().contains_key("c"));
        assert_eq!("d", taskargs.variables()["c"].as_str());
    }
}
