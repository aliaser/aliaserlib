/*
 * Copyright (C) 2017 Andrew Hayzen <ahayzen@gmail.com>
 *
 * This file is part of aliaserlib.
 *
 * aliaserlib is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation; version 3.
 *
 * aliaserlib is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with aliaserlib.  If not, see <http://www.gnu.org/licenses/>.
 */
use std::collections::BTreeMap;
use std::fmt;
use std::path::{Path, PathBuf};
use std::vec::Vec;

/// This is a raw command that is part of a [`Pipeline`]
///
/// [`Pipeline`]: ../pipeline/struct.Pipeline.html
pub struct CommandTask {
    args: Vec<String>,
    directory: PathBuf,
    env: BTreeMap<String, String>,
    program: PathBuf,
}

impl CommandTask {
    /// List of arguments that will be given to the executable
    pub fn args(&self) -> &Vec<String> {
        &self.args
    }

    /// Directory which the command will be executed
    pub fn directory(&self) -> &Path {
        self.directory.as_path()
    }

    /// Map of environment variables that the command will execute with
    pub fn env(&self) -> &BTreeMap<String, String> {
        &self.env
    }

    /// Retrieve a mutable reference to the environment
    // TODO: should we just have env_insert(key, value) ?
    pub fn env_mut(&mut self) -> &mut BTreeMap<String, String> {
        &mut self.env
    }

    /// Program which this command will run
    pub fn program(&self) -> &Path {
        self.program.as_path()
    }

    /// Replaces the program with the given new program
    ///
    /// This is used by [`PipelineBuilder`] to update `${EXEC}` to a path.
    ///
    /// [`PipelineBuilder`]: ../pipeline/trait.PipelineBuilder.html
    pub fn program_replace(&mut self, program: &Path) {
        self.program = PathBuf::from(program);
    }

    /// Create a new [`CommandTask`] from the given details
    ///
    /// [`CommandTask`]: struct.CommandTask.html
    // FIXME: should this take references ?
    pub fn new(args: Vec<String>, directory: PathBuf, env: BTreeMap<String, String>, program: PathBuf) -> CommandTask {
        CommandTask {
            args: args,
            directory: directory,
            env: env,
            program: program,
        }
    }
}

impl fmt::Display for CommandTask {
    /// Formats the [`CommandTask`] into a human readable format
    ///
    /// This is useful as debug and error lines format the [`CommandTask`]
    ///
    /// [`CommandTask`]: struct.CommandTask.html
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "Program: {:?} Args: {:?}", self.program, self.args)
    }
}

#[cfg(test)]
pub mod helper {
    use super::*;

    use std::env;

    /// Helper for making a simple [`CommandTask`]
    ///
    /// [`CommandTask`]: ../../common/struct.CommandTask.html
    pub fn make_command_task() -> CommandTask {
        let args = vec!("a".to_owned(), "b".to_owned());
        let directory = PathBuf::from("/test/path");
        let mut env = BTreeMap::new();
        env.insert("c".to_owned(), "d".to_owned());
        let program = PathBuf::from("/test/program");

        CommandTask::new(args, directory, env, program)
    }

    /// Helper method for tests which creates CommandTask from program and args
    pub fn make_command_task_with_args(program: &str, args: Vec<&str>) -> CommandTask {
        let mut arguments: Vec<String> = Vec::new();

        for arg in &args {
            // FIXME: Arg is &&str so we need double to_owned() ?
            arguments.push(arg.to_owned().to_owned());
        }

        CommandTask::new(arguments, env::current_dir().unwrap(), BTreeMap::new(), PathBuf::from(program))
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    use common::task::helper::make_command_task;

    #[test]
    fn test_display() {
        let task = make_command_task();
        let display = format!("{}", task);

        assert_eq!("Program: \"/test/program\" Args: [\"a\", \"b\"]", display.as_str());
    }

    #[test]
    fn test_env_mut() {
        let mut task = make_command_task();

        {
            let mut env = task.env_mut();
            env.insert("e".to_owned(), "f".to_owned());
        }

        assert!(task.env().contains_key("c"));
        assert_eq!("d", task.env()["c"].as_str());
        assert!(task.env().contains_key("e"));
        assert_eq!("f", task.env()["e"].as_str());
    }

    #[test]
    fn test_new() {
        // Create a new CommandTask check that values are correct
        let task = make_command_task();

        assert_eq!(2, task.args().len());
        assert_eq!("a", task.args()[0].as_str());
        assert_eq!("b", task.args()[1].as_str());

        assert_eq!("/test/path", task.directory().to_str().unwrap());

        assert!(task.env().contains_key("c"));
        assert_eq!("d", task.env()["c"].as_str());

        assert_eq!("/test/program", task.program().to_str().unwrap());
    }

    #[test]
    fn test_program_replace() {
        let mut task = make_command_task();

        task.program_replace(Path::new("/test/program_b"));

        assert_eq!("/test/program_b", task.program().to_str().unwrap());
    }
}
